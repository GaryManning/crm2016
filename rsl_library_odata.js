function GetRecords (arrIds, params) {

	var def = $.Deferred();
	var records = [];

	function getRecords (URL) {
		return $.ajax({
			type: "GET",
			   contentType: "application/json; charaset=utf-8",
			   datatype: "json",
			   url: URL,
			   beforeSend: function (XMLHttpRequest) {
				   //Specifiying this header ensures that the results will be returned as JSON
				   XMLHttpRequest.setRequestHeader("Accept", "application/json");
			   }
		});
	}

	// The 'select' statement
	//var select = params.select;
	// The 'filter' statement
	//var filter = params.filter;

	// There is a character limit on the length of urls that can be passed to the CRM system
	// (limited by IE to about 2084, 25 records). That places a limit on the number of records
	// that can be retrieved in one go since each retrieval is constructed by constructing a
	// string of all the record ids. By breaking the array into sections smaller strings can be
	// constructed and used for getting the CRM records.
	
	// Split the array into smaller arrays
	var max = 24; // The number of records that can be retrieved in one go
	//var max = 5; // The number of records that can be retrieved in one go
	var shortArrIds = []; // This will be the array of shortened arrays
	for ( var i = 0, len = arrIds.length; i < len; i += max ) {
		if ( (i + max) >= arrIds.length ) {
			shortArrIds.push(arrIds.slice(i));
		} else {
			shortArrIds.push(arrIds.slice(i, i + max));
		}
	}

	// An array that will hold all the AJAX request results, ready to be passed to the deferred object handler ($.when below)
	var resultsArray = [];

	// Loop through each shortened array and build a url for use in the AJAX request
	// The urls should look something like:
	// http://tw-crm02:5555/TIPS/XRMServices/2011/OrganizationData.svc/rsl_penaltySet?$select=rsl_FirstName,rsl_LastName,rsl_penaltystatus,rsl_AppealStatus,rsl_PenaltyStatusLastValue&$filter=rsl_penaltyId%20=%20guid('xxxxx-xxx-xxx-xxx')%20or%20rsl_penaltyId%20=%20guid('xxxx-xxx-xxx-xx') etc. etc. etc (up to 25 filter clauses)
	for ( i = 0, len = shortArrIds.length; i < len; i++ ) {
		var URL = serverXRMServices + entitySet;
		URL = URL + "?$select=" + params.select;
		URL = URL + "&$filter=";
		for (var j = 0; j < shortArrIds[i].length; j++ ) {
			URL = URL + params.filter + "%20eq%20(guid'" + shortArrIds[i][j] + "')%20";
			if ( j < (shortArrIds[i].length - 1) ) {
				URL = URL + "or%20";
			}
		}
		// Call the AJAX request (getRecords) and store the results in the resultsArray
		// The AJAX requests are asynchronous so the script starts them and then carries
		// on with the rest of the script...
		resultsArray.push(getRecords(URL));
	}

	// ... however, at this point it has to wait for all the AJAX calls (started above) to finish
	// This is done by the JQuery 'when' function - it waits for all of its applied parameters to
	// finish (in this case all the AJAX calls stored in the resultsArray array). When they are all
	// finished the 'success' function is executed on the results, or the 'fail' function if there was
	// an issue.
	$.when.apply($, resultsArray).then(
			// Success section
			function () {
				var records = [];
				if ( resultsArray.lenth === 0 ) {
					def.reject("No records found");
					return
				}
				if ( resultsArray.length == 1 ) {
					// If only one AJAX call was made (less than the 'max' number of records were amended) then the 'when' clause passes the AJAX result in 3 parameters to the 'done' function (data, status and resultText)
					// In this case we only need the data argument (arguments[0])
					//Log("Overriding for 1 argument");
					records = arguments[0].d.results;
				} else {
					// If more than one AJAX call was made (more than the 'max' number of records) then the 'when' clause passes the AJAX results in that number of parameters (each an object of {'data': data, 'status': status, 'resultText': resultText})
					// In that case we need to loop through the arguments and get the arg[0] of each argument
					// and rebuild them into one array again
					$.each(arguments, function (index, arg) {
						//Log("Got: "+arg[0].d.results[0].rsl_FirstName);
						records = records.concat(arg[0].d.results);
					});
				}
				def.resolve(records);
			},

			 // Fail section
			 function (error) {
				 if ( error.statusText === "timeout" ) {
					 alert("Timeout error." + XmlHttpRequest.responseText);
				 }
				 if ( error && error.responseText ) {
					 alert("Error - " + error.statusText);
				 }
				 def.reject();
			 }
	);

	return def.promise();
}

function checkStatusGeneric (records, matchField, matchStatus, condition, errorMessage) {

		var count = records.length;
		for (i=0; i < count; i++ ) {
			// Loop through the possible matches
			//if ( records[i][matchField] == matchStatus ) {
			if ( eval(condition) ) {
					alert(errorMessage);
					window.returnValue = "Cancelled";
					window.close();
					return false;
			}
		}
}

function checkStatusMultiGeneric (records, matchField, matchStatus1, matchStatus2, condition, errorMessage) {

		var count = records.length;
		for (i=0; i < count; i++ ) {
			// Loop through the possible matches
			//if ( records[i][matchField] == matchStatus ) {
			if ( eval(condition) ) {
					alert(errorMessage);
					window.returnValue = "Cancelled";
					window.close();
					return false;
			}
		}
}

function checkStatus (records, matchField, matchStatus, errorMessage) {
	// Check for a match against a string value
	checkStatusGeneric(records, matchField, matchStatus, "records[i][matchField] == matchStatus", errorMessage);
}

function checkStatusNE (records, matchField, matchStatus, errorMessage) {
	// Check for a match against a string value
	checkStatusGeneric(records, matchField, matchStatus, "records[i][matchField] != matchStatus", errorMessage);
}

function checkStatusValue (records, matchField, matchStatus, errorMessage) {
	// Check for match against the .Value property of an object
	checkStatusGeneric(records, matchField, matchStatus, "records[i][matchField].Value == matchStatus", errorMessage);
}

function checkStatusNEValue (records, matchField, matchStatus, errorMessage) {
	// Check for no match against the .Value property of an object
	checkStatusGeneric(records, matchField, matchStatus, "records[i][matchField].Value != matchStatus", errorMessage);
}

function checkStatusNEMultiValue (records, matchField, matchStatus1, matchStatus2, errorMessage) {
	// Check for no match against the .Value property of an object
	checkStatusMultiGeneric(records, matchField, matchStatus1, matchStatus2, "records[i][matchField].Value != matchStatus1 && records[i][matchField].Value != matchStatus2", errorMessage);
}

function checkStatusNEName (records, matchField, matchStatus, errorMessage) {
	// Check for no match against the .Name property of an object
	checkStatusGeneric(records, matchField, matchStatus, "records[i][matchField].Name != matchStatus", errorMessage);
}

function updateRecord(entity, entitySet, id, entityObject, status, note) {
	// updateRecord: see http://rajeevpentyala.wordpress.com/2012/02/05/update-record-using-odata-and-jquery-in-crm-2011/
	var jsonEntity = window.JSON.stringify(entityObject);

	$.ajax({
		type: "POST",
		contentType: "application/json; charaset=utf-8",
		datatype: "json",
		data: jsonEntity,
		//url: serverUrl + "(guid'" + id + "')",
        url: serverXRMServices + entitySet + "(guid'" + id + "')",
		beforeSend: function (XMLHttpRequest) {
			//Specifiying this header ensures that the results will be returned as JSON
			XMLHttpRequest.setRequestHeader("Accept", "application/json");
			// Specify the HTTP method MERGE to update just the changes you are submitting
			XMLHttpRequest.setRequestHeader("X-HTTP-METHOD", "MERGE");
		},
		success: function (data, textStatus, XmlHttpRequest) {
			// The update was successful, so now update the 'Notes' entity with the details of what was done
			AddNotes(entity, id, status, note);
		},
		error: function (XmlHttpRequest, textStatus, errorThrown) {
			if (XmlHttpRequest && textStatus ) {
				//alert("Error while updating " + entitySet + "; Error: " + errorThrown);
				alert(JSON.parse(XmlHttpRequest.responseText).error.message.value);
			}
		}
	});
}

function AddNotes(entity, id, status, note) {
	var serverUrl = serverXRMServices;
	var entitySet = "/AnnotationSet";
	var objAnnotation = new Object();
	objAnnotation.NoteText = note;
	objAnnotation.Subject = "Notice Closed - " + status;

	var refAccount = new Object();
	refAccount.LogicalName = entity;
	refAccount.Id = id;

	objAnnotation.ObjectId = refAccount;
	objAnnotation.ObjectTypeCode = refAccount.LogicalName;

	// Parse the entity object into JSON 
	var jsonEntity = window.JSON.stringify(objAnnotation);

	// Asynchronous AJAX function to add a note to the Annotation entity attached to the current guid
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		datatype: "json",
		url: serverUrl + entitySet,
		data: jsonEntity,
		beforeSend: function (XmlHttpRequest) {
			XmlHttpRequest.setRequestHeader("Accept", "application/json");
		},
		success: function (XmlHttpRequest) {
			Log("Note update completed successfuly");
		},
		error: function (XmlHttpRequest, textStatus, errorThrown) {
			if (XmlHttpRequest && XmlHttpRequest.responseText ) {
				//alert("Error while writing note. Error: " + XmlHttpRequest.responseText + ". (url: " + url + ")");
				alert(JSON.parse(XmlHttpRequest.responseText).error.message.value);
			}
		}
	});
}
