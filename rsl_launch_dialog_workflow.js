function launchModalDialog (dialogID, typeName, recordId) {

	//var primaryEntityId = Xrm.Page.data.entity.getId();
	
	// Load Modal
	var serverUrl = Mscrm.CrmUri.create('/cs/dialog/rundialog.aspx');

	if ( window.showModalDialog ) {

		window.showModalDialog(serverUrl + '?DialogId=' + dialogID + '&EntityName=' + typeName + '&ObjectId=' + recordId, null, 'dialogwidth:615px;dialogheight:480px;resizable:1;status:1;scrollbars:1');
		
		// Refreshes page once dialog is finished/closed
		Xrm.Utility.openEntityForm(typeName, recordId);

	} else {

		alert("Run this command from the '... (MORE COMMANDS)'-->'Start Dialog' menu item");

	}
}
