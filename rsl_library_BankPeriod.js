function closeBankPeriod() {
	if ( confirm("You are about to close the current Bank Period and open a new one. Press 'Ok' to continue otherwise press 'Cancel'") == false ) {
		return;
	}

	$.when( getCurrentBankPeriod() ).done( function(currentPeriod) {

		if ( currentPeriod.d.results.length != 1 ) {
			// More (or less) than 1 open bank period was returned
			if ( Xrm.Page.context.getUserName() !== "Simon Hopper" ) {
				alert("Error: " + currentPeriod.d.results.length + " open Bank Periods were found (should be 1).");
				return;
			}
			$.each(currentPeriod.d.results, function(i, v) {
				Xrm.Utility.confirmDialog("Overriding the multiple open bank periods. Close " + currentPeriod.d.results[0].rsl_bankperiod + " by pressing 'Ok' or 'Cancel' to choose another",
									function() {
										//Yes bit
										return false
									},
									function() {
										// No bit
										currentPeriod.d.results.shift();
									});
				});
			alert(currentPeriod.d.results[0].rsl_bankperiod);
		}

	    var currentBankPeriod = new Object();
    	currentBankPeriod.rsl_ClosingDate = new Date();
    	currentBankPeriod.rsl_status = false;
	
    	// Parse the entity object into JSON 
    	var jsonEntity = window.JSON.stringify(currentBankPeriod);

		// Close the current record
		updateRecord(currentPeriod.d.results[0].rsl_bankperiodId, jsonEntity);

		var newName = parseInt(currentPeriod.d.results[0].rsl_bankperiod.match(/\d+/)) + 1;
		var newDate = new Date().format("dd MMM yyyy hh:mm:ss");

		$.when( getCurrentPeriodMap() ).done( function(currentMap) {
			// Create a new one
	    	var newBankPeriod = new Object();
    		newBankPeriod.rsl_bankperiod = "BP " + newName.toString() + " (" + newDate +")";
    		newBankPeriod.rsl_OpeningDate = new Date();
    		newBankPeriod.rsl_status = true;
    		newBankPeriod.rsl_PeriodMapReference = {
					'Id': currentMap.d.results[0].rsl_periodmapId,
					'Name': currentMap.d.results[0].rsl_periodLabel,
					'LogicalName': 'rsl_periodmap'
				};

    		jsonEntity = window.JSON.stringify(newBankPeriod);

			$.when( createRecord(jsonEntity) ).done( function() {
				// Refresh the grid
				setTimeout('document.getElementById("crmGrid").control.refresh();', 500);
			});
		});
	});
}

function createRecord(jsonEntity) {
	var serverUrl = serverXRMServices + "rsl_bankperiodSet";

    $.ajax({
        type: "POST",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: serverUrl,
        beforeSend: function (XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        //success: function (data, textStatus, XmlHttpRequest) {
			//Log("Record created sucessfully");
        //},
        error: function (XmlHttpRequest, textStatus, errorThrown) {
			//Log("Record NOT created sucessfully");
            if (XmlHttpRequest && textStatus ) {
                alert("Error while creating new Bank Period; Error: " + errorThrown + "\n" + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
	    	}
        }
    });
}

function updateRecord(id, jsonEntity) {
	var serverUrl = serverXRMServices + "rsl_bankperiodSet";

    $.ajax({
        type: "POST",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: serverUrl + "(guid'" + id + "')",
        beforeSend: function (XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            // Specify the HTTP method MERGE to update just the changes you are submitting
            XMLHttpRequest.setRequestHeader("X-HTTP-METHOD", "MERGE");
        },
        success: function (data, textStatus, XmlHttpRequest) {
			Log("Record updated sucessfully");
        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
			Log("Record NOT updated sucessfully");
            if (XmlHttpRequest && textStatus ) {
                alert("Error while closing current Bank Period; Error: " + errorThrown + "\n" + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
	    	}
        }
    });
}


//
// Function to add the current Bank Period to a new form
//
function addBankPeriod() {
    // Capture the context of the form.  If 1 then the form is not new and we don't need to do anything
    if ( Xrm.Page.ui.getFormType() != 1) {
		return;
	}

    var bankPeriod = Xrm.Page.getAttribute("rsl_bankperiod");

	$.when( getCurrentBankPeriod() ).done( function (currentPeriod) {

		if ( currentPeriod.d.results.length != 1 ) {
			// More (or less) than 1 open bank period was returned
			alert("Warning: more than 1 open Bank Period was found. Please select a Bank Period manually.");
			Xrm.Page.ui.controls.get("rsl_bankperiod").setDisabled(false);
			return;
		}

    	bankPeriod.setValue([{
			'id': currentPeriod.d.results[0].rsl_bankperiodId,
			'name': currentPeriod.d.results[0].rsl_bankperiod,
			'entityType': 'rsl_bankperiod'
		}]);

	});
}

//
// Function to get the current 'Open' bank period
//
function getCurrentBankPeriod() {

	var currentPeriod;

    return $.ajax({
        type: "GET",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
		url: serverXRMServices + "rsl_bankperiodSet?$select=rsl_bankperiodId,rsl_bankperiod&$filter=rsl_status%20eq%20true",
        beforeSend: function (XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        //success: function (data, textStatus, XmlHttpRequest) {
			//currentPeriod = data;
        //},
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText ) {
                alert("Error while retrieving current bank period: " + XmlHttpRequest.responseText);
	    	}
        }
    });
}

//
// Function to get the current period map
//
function getCurrentPeriodMap() {

	var now = new Date().toISOString(); // Returns a string formatted: '16-12-2014T16:32:25Z'
	now = "datetime'" + now + "'";		// make it useable by the $filter below: datetime'16-12-2014T16:32:25Z'

    return $.ajax({
        type: "GET",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
		url: serverXRMServices + "rsl_periodmapSet?$select=rsl_periodmapId,rsl_periodlabel&$filter=rsl_periodstart%20le%20"+now+"%20and%20rsl_periodfinish%20ge%20"+now,
        beforeSend: function (XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        //success: function (data, textStatus, XmlHttpRequest) {
			//currentPeriod = data;
        //},
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText ) {
                alert("Error while retrieving current period map: " + "\n" + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
	    	}
        }
    });
}

if ( !Date.prototype.toISOString ) {         
    (function() {         
        function pad(number) {
	            var r = String(number);
	            if ( r.length === 1 ) {
	                r = '0' + r;
	            }
	            return r;
	        }      
	        Date.prototype.toISOString = function() {
	            return this.getUTCFullYear()
               + '-' + pad( this.getUTCMonth() + 1 )
               + '-' + pad( this.getUTCDate() )
               + 'T' + pad( this.getUTCHours() )
               + ':' + pad( this.getUTCMinutes() )
               + ':' + pad( this.getUTCSeconds() )
               + '.' + String( (this.getUTCMilliseconds()/1000).toFixed(3) ).slice( 2, 5 )
               + 'Z';
       };       
   }() );
}
