// Court Booking
//
// Concatenate the Court+From+To into FullBooking
function CourtBooking_CreateFullBooking() {
    var myCourt = Xrm.Page.getAttribute("rsl_court").getValue(); // Court Booking Court
    var myFrom = Xrm.Page.getAttribute("rsl_from").getValue(); // Court Booking from
    //    var myTo = Xrm.Page.getAttribute("rsl_to").getValue(); // Court Booking To
    var myMaxCases = Xrm.Page.getAttribute("rsl_maxcases").getValue(); // Court Max Cases
    // Set rsl_name.
    Xrm.Page.getAttribute("rsl_name").setSubmitMode("always"); // This forces the save on a Read-Only field.
    if (myMaxCases != null) {
        //        Xrm.Page.data.entity.attributes.get("rsl_name").setValue(myCourt[0].name + ' ' + myFrom.format("dd-MMM") + ' ' + myFrom.format("HH:mm") + ' - ' + myTo.format("HH:mm") + ' (' + myMaxCases + ')');
        Xrm.Page.data.entity.attributes.get("rsl_name").setValue(myCourt[0].name + ' ' + myFrom.format("dd-MMM") + ' ' + myFrom.format("HH:mm") + ' (' + myMaxCases + ')');
    }
    else {
        //        Xrm.Page.data.entity.attributes.get("rsl_name").setValue(myCourt[0].name + ' ' + myFrom.format("dd-MMM") + ' ' + myFrom.format("HH:mm") + ' - ' + myTo.format("HH:mm"));
        Xrm.Page.data.entity.attributes.get("rsl_name").setValue(myCourt[0].name + ' ' + myFrom.format("dd-MMM") + ' ' + myFrom.format("HH:mm"));
    }

}
// If we have entered a Start Date/Time and we have now Finish Date/Time default it to Start + 1 Hour
//function CourtBooking_StartChanged() {
//    var myFrom = Xrm.Page.getAttribute("rsl_from").getValue(); // Court Booking from
//    var myTo = Xrm.Page.getAttribute("rsl_to").getValue(); // Court Booking To
//    if (myFrom != null && myTo == null) {
//        Xrm.Page.data.entity.attributes.get("rsl_to").setValue(myFrom);
//        //        Xrm.Page.data.entity.attributes.get("rsl_to").setValue(myFrom.setHours(myFrom.getHours() + 1));
//    }
//}

// Save Validation for the form.
function CourtBooking_SaveValidation(executionObj) {
    var myFrom = Xrm.Page.getAttribute("rsl_from").getValue(); // Court Booking from
    var myTo = Xrm.Page.getAttribute("rsl_to").getValue(); // Court Booking To
    var now = new Date() // Date & Time now
    // Ensure the Finish is after the start
    //    if (myFrom >= myTo) {
    //        alert("The 'To' date and time must be after the 'From' date and time");
    //        executionObj.getEventArgs().preventDefault();
    //        return;
    //    }
    // Ensure Start of Inish are not before now()
    if (myFrom < now) {
        alert("The 'From' date cannot be in the past.");
        executionObj.getEventArgs().preventDefault();
        return;
    }

    // Now concatinate the booking into one string.
    CourtBooking_CreateFullBooking();
}

// Functional called when the Court is Changed and we are in NEW record mode.
// Sets the "rsl_asnrequired" from the Parent Court Record
function set_court_details() {
	$ = parent.$;
    var court = Xrm.Page.getAttribute("rsl_court").getValue();
    //
    // If we have a Court set e.g. It has not been cleared
    //
    if (court != null) {
        // Get court details...
        var guid = Xrm.Page.getAttribute("rsl_court").getValue()[0].id;
        //var odataSetName = var_court[0].entityType;
        var odataSetName = "rsl_courtSet"; // var_court[0].entityType returns 'court' rather than 'Court'

        var select = "?$select=rsl_asnrequired,rsl_admin_address1,rsl_admin_address2,rsl_admin_address3,rsl_admin_town,rsl_admin_county,rsl_admin_postcode,rsl_admin_country,rsl_Address1,rsl_Address2,rsl_Address3,rsl_Town,rsl_County,rsl_Postcode,rsl_country,rsl_courtcode,rsl_admin_fulladdress,rsl_FullAddress";
        //Asynchronous AJAX function to Retrieve a CRM record using OData
        $.ajax({
            type: "get",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: serverOdataEndpoint + odataSetName + "(guid'" + guid + "')" + select,
            beforeSend: function(XMLHttpRequest) {
                //Specifying this header ensures that the results will be returned as JSON.
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success: function(data, textStatus, XmlHttpRequest) {
                Xrm.Page.getAttribute("rsl_asnrequired").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_admin_address1").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_admin_address2").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_admin_address3").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_admin_town").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_admin_county").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_admin_postcode").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_admin_country").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_address1").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_address2").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_address3").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_town").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_county").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_postcode").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_country").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_courtcode").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_admin_fulladdress").setSubmitMode("always"); // This forces the save on a Read-Only field.
                Xrm.Page.getAttribute("rsl_fulladdress").setSubmitMode("always"); // This forces the save on a Read-Only field.

                Xrm.Page.getAttribute("rsl_asnrequired").setValue(data.d.rsl_asnrequired.Value);
                Xrm.Page.getAttribute("rsl_admin_address1").setValue(data.d.rsl_admin_address1);
                Xrm.Page.getAttribute("rsl_admin_address2").setValue(data.d.rsl_admin_address2);
                Xrm.Page.getAttribute("rsl_admin_address3").setValue(data.d.rsl_admin_address3);
                Xrm.Page.getAttribute("rsl_admin_town").setValue(data.d.rsl_admin_town);
                Xrm.Page.getAttribute("rsl_admin_county").setValue(data.d.rsl_admin_county);
                Xrm.Page.getAttribute("rsl_admin_postcode").setValue(data.d.rsl_admin_postcode);
                Xrm.Page.getAttribute("rsl_admin_country").setValue(data.d.rsl_admin_country);
                Xrm.Page.getAttribute("rsl_address1").setValue(data.d.rsl_Address1);
                Xrm.Page.getAttribute("rsl_address2").setValue(data.d.rsl_Address2);
                Xrm.Page.getAttribute("rsl_address3").setValue(data.d.rsl_Address3);
                Xrm.Page.getAttribute("rsl_town").setValue(data.d.rsl_Town);
                Xrm.Page.getAttribute("rsl_county").setValue(data.d.rsl_County);
                Xrm.Page.getAttribute("rsl_postcode").setValue(data.d.rsl_Postcode);
                Xrm.Page.getAttribute("rsl_country").setValue(data.d.rsl_country);
                Xrm.Page.getAttribute("rsl_courtcode").setValue(data.d.rsl_courtcode);
                Xrm.Page.getAttribute("rsl_admin_fulladdress").setValue(data.d.rsl_admin_fulladdress);
                Xrm.Page.getAttribute("rsl_fulladdress").setValue(data.d.rsl_FullAddress);

            },
            error: function(XmlHttpRequest, textStatus, errorThrown) {
                if (XmlHttpRequest) {
                    alert("Error: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
                }
            }
        });
    }
    else {
        Xrm.Page.getAttribute("rsl_asnrequired").setValue(null);
    }
}


// Functional called when the Court is Changed and we are in NEW record mode.
// Sets the "rsl_asnrequired" from the Parent Court Record
function set_prosecutor_details() {
    //
    // Bring over Prosecutors Signature
    //
	$ = parent.$;
    var prosecutor = Xrm.Page.getAttribute("rsl_pros_prosecutor").getValue();
    //
    // If we have a Court set e.g. It has not been cleared
    //
    //debugger;
    if (prosecutor != null) {
        // Get Prosecutor details...
        var guid = Xrm.Page.getAttribute("rsl_pros_prosecutor").getValue()[0].id;
        //alert(guid);
        //var odataSetName = var_court[0].entityType;
        var odataSetName = "rsl_prosecutorSet";

        var select = "?$select=rsl_prosecutor_signature";
        //Asynchronous AJAX function to Retrieve a CRM record using OData
        $.ajax({
            type: "get",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: serverOdataEndpoint + odataSetName + "(guid'" + guid + "')" + select,
            beforeSend: function(XMLHttpRequest) {
                //Specifying this header ensures that the results will be returned as JSON.
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success: function(data, textStatus, XmlHttpRequest) {
                Xrm.Page.getAttribute("rsl_prosecutor_signature").setSubmitMode("always"); // This forces the save on a Read-Only field.
                //Xrm.Page.getAttribute("rsl_prosecutor_signature").setValue(data.d.rsl_prosecutor_signature.Value);
                Xrm.Page.getAttribute("rsl_prosecutor_signature").setValue(data.d.rsl_prosecutor_signature);
                //alert(Xrm.Page.getAttribute("rsl_prosecutor_signature").getValue());

            },
            error: function(XmlHttpRequest, textStatus, errorThrown) {
                if (XmlHttpRequest) {
                    alert("Error: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
                }
            }
        });
    }
    else {
        Xrm.Page.getAttribute("rsl_prosecutor_signature").setValue(null);
    }
}
