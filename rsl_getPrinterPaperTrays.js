var getPrinterPaperTrays = function () {

	// Temporary setup
	var printers = [{"name": "RSSL", "papertrays": ["Auto"]}];

	callWrapper({
		dataType: "jsonp",
		url: serverBridge + "getpapertrays",
		success: function (data) {
			printers = data.printers;
			//printers = [{"name": "Printer A", "papertrays": ["Tray 1", "Tray 2"]},{"name": "Printer B", "papertrays": ["Tray 3", "Tray 4"]}];
		},
		error: function () {
			//alert("Failed to get list of available paper trays");
			printers = [];
		}
	});

	return function (name) {
		if ( name === "Printers" ) {
			return printers;
		}

		for (var i = 0; i < printers.length; i++) {
			if ( printers[i].name === name ) {
				return printers[i].papertrays
			}
		}
		// If we get this far then the printer was not in the list so return an empty array
		return [];
	}
}();
