// Load the lifecycles stylesheet
//rsl_load_css_file("../WebResources/rsl_lifecyclestyles");

function defaultLauncher (selectedIds, entity, popupName, grid, pdfprint) {
	/*
	 * This function acts as a central script that is called by the particular ribbon buttons
	 * The parameters are:
	 * selectedIds - the list of guids of the selected records
	 * entity - 'penalty' or 'prosecution'
	 * popupName - the name of the popup to be called once all processing has been completed here
	 * grid - true or false depending on whether the button is on a grid or a form respectively
	 *
	 * Overview: this function reads (and then evals) the appropriate external javascript file and then
	 * calls the embedded function (in the just loaded-in file) with the appropriate parameters
	 *
	 * Set 'Debug.logging = true;' to get logging output the debugging console (or alerts if not started)
	 */

	//Log("defaultLauncher: Selected records count: " + selectedIds.length);
	if ( selectedIds.length == 0 ) {
		alert("No records have been selected.");
		return;
	}

	// Check to see if this is a form...
	if ( Xrm.Page.data ) {
		// ... and if it is then check to see if it is 'dirty'
		if ( Xrm.Page.data.entity.getIsDirty() ) {
			alert("Please save the form before using this button.");
			return;
		}
	}

	//configurePopupWindows();

	var entitySet, selectParams, filterField, statusField;

	// Set up the appropriate variables depending on the calling entity type
	switch (entity) {
		case "penalty":
			selectParams = "?$select=rsl_penaltyId,rsl_FirstName,rsl_LastName,rsl_penaltystatus";
			filterField = "rsl_penaltyId";
			statusField = "rsl_penaltystatus";
			fnameField = "rsl_FirstName";
			lnameField = "rsl_LastName";
			break;

		case "prosecution":
			selectParams = "?$select=rsl_pros_firstname,rsl_pros_lastname,rsl_pros_prosecutionstatus";
			filterField = "rsl_prosecutionId";
			statusField = "rsl_pros_prosecutionstatus";
			fnameField = "rsl_pros_firstname";
			lnameField = "rsl_pros_lastname";
			break;
	}

	entity = "rsl_" + entity;
	entitySet = entity + "Set";

	//Debug.logging = true;
	
	var lifeCyclePrint, adhocPrint, editdocument, adddocument;
	var addDocument;

	function popupWindow (idArray, popupName, data) {

		// If a template is supplied in the parameters then use it, otherwise just use a blank string
		if ( data ) {
			template = data.template;
		} else {
			template = "";
		}

		// The section that reads and evals the appropriate external file
		switch (popupName) {
			case "printpdf":
				// Load in the lifecycleprint javascript file and execute it...
				if ( typeof printDocuments !== 'function' ) {
					if ( lifeCyclePrint == undefined ) {
						lifeCyclePrint = new XMLHttpRequest(); lifeCyclePrint.open("GET", "../WebResources/rsl_lifecycleprint", false); lifeCyclePrint.send();
					}
					eval(lifeCyclePrint.responseText);
				}
				Log("switch at pdfprint: "+idArray);
				printDocuments(selectedIds.length, idArray, entity, template, true);
				break;

			case "lifecycleprint":
				// Load in the lifecycleprint javascript file and execute it...
				if ( typeof printDocuments !== 'function' ) {
					if ( lifeCyclePrint == undefined ) {
						lifeCyclePrint = new XMLHttpRequest(); lifeCyclePrint.open("GET", "../WebResources/rsl_lifecycleprint", false); lifeCyclePrint.send();
					}
					eval(lifeCyclePrint.responseText);
				}
				Log("switch at lifecycleprint: "+idArray);
				printDocuments(selectedIds.length, idArray, entity, template , false);
				break;

			case "adhocprint":

				var winWidth = 500;
				var winHeight = window.outerHeight - 300;

				var params = encodeURIComponent("ids="+selectedIds+"&entity="+entity+"&idArray=null&popup=lifecycleprint&winWidth="+winWidth+"&winHeight="+winHeight);
				var d = $.Deferred();

				Xrm.Page.context.rsl_deferred = d;
				Xrm.Utility.openWebResource("rsl_adhocPrintPage", params, winWidth, winHeight);
				$.when(d).then(function(data) {
					popupWindow(idArray, "lifecycleprint", data);
				});
				break;

			case "editdocument":

				var winWidth = 500;
				var winHeight = window.outerHeight - 300;

				var params = encodeURIComponent("ids="+selectedIds+"&entity="+entity+"&idArray=null&popup=lifecycleprint&winWidth="+winWidth+"&winHeight="+winHeight);
				var d = $.Deferred();
				Xrm.Page.context.rsl_deferred = d;
				Xrm.Utility.openWebResource("rsl_adhocPrintPage", params, winWidth, winHeight);
				$.when(d).then(function(data) {
					popupWindow(idArray, "downloaddocument", data);
				});
				break;

			case "downloaddocument":
				// This line will just call the editdocument function on the 'bridge' and expect a fully
				// formatted document in return, which can be downloaded and saved - it must be 'saved'
				// or 'saved as' - selecting 'open' will not work
				window.location.href = serverBridge + "editdocument?entity=" + entity + template + "&id=" + idArray[0][0];
				break;

			case "adddocument":

				var winWidth = 500;
				//var winHeight = window.outerHeight - 400 - 236;	// minus another 236 so the window doesn't look so odd
				var winHeight = 336;	// minus another 236 so the window doesn't look so odd

				var params = encodeURIComponent("ids="+selectedIds+"&entity="+entity+"&idArray=null&winWidth="+winWidth+"&winHeight="+winHeight);
				var d = $.Deferred();
				Xrm.Page.context.rsl_deferred = d;
				Xrm.Utility.openWebResource("rsl_addDocumentPage", params, winWidth, winHeight);
				$.when(d).then(function(data) {
					addDocument(idArray, entity);
					Xrm.Page.ui.controls.get("PrintedDocsGrid").refresh();
				});
				break;

			default:
				// Set the popup window size parameters
				var top = 200;
				var left = 400;
				var height = 250;
				var width = 520;
				var toolbar = "no";
				var menubar = "no";
				var loc = "no";
				var resizable = "yes";

				var windowParams = "dialogTop:" + top + "px; dialogLeft:" + left + "px; dialogHeight:" + height + "px; dialogWidth:" + width + "px; toolBar: "+ toolbar + "; menubar:" + menubar + "; location:" + loc + "; resizable:" + resizable +";";
				var windowUrl = serverWebResources + popupName;

				//window.showModalDialog(windowUrl, selectedIds, windowParams);
				alert("Opening " + popupName + " window (" + windowUrl + ")");
				break;
		}
	}

	// Define a helper function to check that a record in a specified state
	function checkMatch (record, status) {
		//Log("Checking match...");
		// Make sure that each record is 'in the right state' to be processed
		// If not then the script displays an informative alert and exits
		// Make short-cuts to values in the save records array - just makes the code more readable
		var r = record;
		var p = r[statusField].Value;
		//var a = r[appealField].Value;
		var name = record[fnameField] + " " + record[lnameField];
		if ( p != status ) {
			// Check that the 'status' matches the others, if it doesn't then popup up a message and return 'false'
			Log("Mis-matched status for: "+name+"("+p+" should be "+status+")");
			alert("All records should have the same status when using this button.");
			return false;
		}
	}

	function getRecords (URL) {
		//Log("Getting Records (" + URL + ")...")
		return $.ajax({
			type: "GET",
			contentType: "application/json; charaset=utf-8",
			datatype: "json",
			url: URL,
			beforeSend: function (XMLHttpRequest) {
				//Specifiying this header ensures that the results will be returned as JSON
				XMLHttpRequest.setRequestHeader("Accept", "application/json");
			}
		});
	}


	// The selected IDs are passed to the script in the 'selectedIds' variable
	// We just need to convert it to a string and split it by ','s into an array
	var strIds = selectedIds.toString();
	var arrIds = strIds.split(",");

	// There is a character limit on the length of urls that can be passed to the CRM system (about 2084, 25 records)
	// That places a limit on the number of records that can be retrieved in one go since each retrieval is constructed
	// by constructing a string of all the record ids. By breaking the array into sections smaller strings can be
	// constructed and used for getting the CRM records.
	// Split the array into smaller arrays
	var max = 5;	// The number of records that can be retrieved in one go
	var shortArrIds;	 // This will be the array of shortened arrays
	shortArrIds	= [];
	for ( var i = 0, len = arrIds.length; i < len; i += max ) {
		shortArrIds.push(arrIds.slice(i, i + max));
	}


	// If only one record is selected then we don't need to go through the process of checking that they all match
	//Log("Selected records count: " + selectedIds.length);
	if ( selectedIds.length != 1 ) {

		// An array that will hold all the AJAX request results, ready to be passed to the deferred object handler ($.when below)
		var resultsArray = [];

		// Loop through each shortened array and build a url for use in the AJAX request
		// The urls should look something like: http://tw-crm02:5555/TIPS/XRMServices/2011/OrganizationData.svc/rsl_penaltySet?$select=rsl_FirstName,rsl_LastName,rsl_penaltystatus,rsl_AppealStatus,rsl_PenaltyStatusLastValue&$filter=rsl_penaltyId%20=%20guid('xxxxx-xxx-xxx-xxx')%20or%20rsl_penaltyId%20=%20guid('xxxx-xxx-xxx-xx')  etc. etc. etc (up to 25 filter clauses)
		for ( i = 0, len = shortArrIds.length; i < len; i++ ) {
			var URL = serverXRMServices + entitySet;
			URL = URL + selectParams;
			URL = URL + "&$filter=";
			for (var j = 0; j < shortArrIds[i].length; j++ ) {
				URL = URL + filterField + "%20eq%20(guid'" + shortArrIds[i][j] + "')%20";
				if ( j < (shortArrIds[i].length - 1) ) {
					URL = URL + "or%20";
				}
			}
			// Call the AJAX request (getRecords) and store the results in the resultsArray
			// The AJAX requests are asynchronous so the script starts them and then carries on with the rest of the script...
			resultsArray.push(getRecords(URL));
		}

		// ... however, at this point it has to wait for all the AJAX calls (started above) to finish
		// This is done by the JQuery 'when' function - it waits for all of its applied parameters to finish (in this case all the AJAX calls stored in the resultsArray array)
		// When they are all finished the 'done' function is executed on the results
		$.when.apply($, resultsArray).then( function () {
			var records = [];
			if ( resultsArray.length == 1 ) {
				// If only one AJAX call was made (less than the 'max' number of records were amended) then the 'when' clause passes the AJAX result in 3 parameters to the 'done' function (data, status and resultText)
				// In this case we only need the data argument (arguments[0])
				//Log("Overriding for 1 argument");
				records = arguments[0].d.results;
			} else {
				// If more than one AJAX call was made (more than the 'max' number of records) then the 'when' clause passes the AJAX results in that number of parameters (each an object of {'data': data, 'status': status, 'resultText': resultText})
				// In that case we need to loop through the arguments and get the arg[0] of each argument
				// and rebuild them into one array again
				$.each(arguments, function (index, arg) {
					//Log("Got: "+arg[0].d.results[0].rsl_FirstName);
					records = records.concat(arg[0].d.results);
				});
			}
			if ( popupName  != "adhocprint" ) {
				//Log("Records: "+records.length);
				// Get the penalty status of the first record - all the others must match it
				var count = records.length;
				var status = records[0][statusField].Value;
				for (i=1; i < count; i++ ) {
					// Check to see if the status matches the first selected row
					if ( checkMatch(records[i], status) == false ) {
						return false;
					}
				}
			}

			if ( confirm("You have selected " + selectedIds.length + " records to process. Press 'Ok' to continue otherwise press 'Cancel'") == false ) {
				alert("No records have been processed.");
				return false;
			}

			// If we get here then everything is ok, so we can now pop the window up...
			popupWindow(shortArrIds, popupName);

			return true;
		},
			// Fail section
			function (error) {
				if ( error.statusText === "timeout" ) {
					alert("Timeout error." + XmlHttpRequest.responseText);
				}
				if ( error && error.responseText ) {
					alert("Error - " + error.statusText);
				}
			}
		);
	} else {
		if ( grid ) {
			// If the button is on a grid then present the user with a brief message to confirm that they
			// have picked the right number of records
			if ( confirm("You have selected " + selectedIds.length + " record to process. Press 'Ok' to continue otherwise press 'Cancel'") == false ) {
				alert("No records have been processed.");
				return false;
			}
		}
		popupWindow(shortArrIds, popupName);
	}

}


var configurePopupWindows = function () {
	// Configure all the possible required windows, but don't display them leave that for the sub-functions
	// Use the 'Completed' flag to indicate whether the script neess to be run again or not (it will get
	// set to 'true' once all the elements have been defined
	var Completed = false;
	var text = "Printing letters...";

	var popupHeight = 100;
	var popupWidth  = 300;

	return function () {
		if ( Completed ) { return; }

		jQuery.fn.center = function () {
			this.css("position", "absolute");
			// Original centering function.
			//this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
			//this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
			//New centering function. Needed to have the height/width of the parent window taken into consideration too
			this.css("top", (($(window.parent.document).height() - this.outerHeight()) / 2) - ($(window.parent.document).height() - $(window).height()) + $(window).scrollTop() + "px");
			this.css("left", (($(window.parent.document).width() - this.outerWidth()) / 2) - ($(window.parent.document).width() - $(window).width()) + $(window).scrollLeft() + "px");
			return this;
		}

		/*
		jQuery.fn.centerUp = function (requester) {
			// Position the window 300 pixels higher than the computed centre
			Debug.logging = true;
			Log("centering " + requester);
			Debug.logging = false;
			this.css("position", "absolute");
			this.css("top", 300 - (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
			this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
			return this;
		}
		*/

		jQuery.fn.clear = function () {
			$('#updatingText').text("Intialising...");
			this.hide();
			return this;
		}

		/*
		// Iframe with content to refresh the windows logon (by calling the ping thing on the bridge server)
		var pingIframe = $('<iframe name="hiddenIframe" id="hiddenIframe" style="border-width: 0px; width: 0px; height: 0px;" src="'+serverPing+'"></iframe>');
		$('body').append(pingIframe);
		*/

		// Popup with spinning progress gif
		$('body').append('<div id="updatingDiv"></div>');

		$('#updatingDiv').append('<p id="titleText">' + text + '</p>')
			.append('<p id="updatingText">Initialising...</p>')
			.hide();

		$('#updatingDiv').center();

		// Popup for use at end of mail-merge (for notes etc)
		$('body').append('<div id="completedDiv"></div>');

		$('#completedDiv').append('<p id="completedTitle">Please confirm that the letters have printed and have (or soon will be) posted.</p>')
			.css('background-color', '#cccccc')
			//.css('height', popupHeight+'px')
			.css('width', popupWidth+'px')
			//.center()
			.hide();

		$('#completedTitle').css('text-align', 'center')
		 		.css('margin', '20px')
		 		.css('font', '20px bolder')
		 		.css('font-family', 'Segoe UI, Tahoma, Arial');

		$('#completedDiv').append('<textarea id="completedTextarea"></textarea>');
		$('#completedTextarea').data('defaultText', "Enter some notes...");
		$('#completedTextarea').text($('#completedTextarea').data('defaultText'));

		$('#completedDiv').append($('<div id="completedButtons"></div>')
				// These buttons need centring
				.append('<button id="completedDoneButton">Done</button>')
				.append('<button id="completedCancelButton">Cancel</button>')
				//.css('width', (popupWidth - 20) + 'px')
				//.css('margin', '0px auto')
				);

		//$('#completedDoneButton').css('margin', '20px');
		$('#completedDoneButton').prop('disabled', true);

		//$('#completedCancelButton').css('margin', '20px');
		
		Completed = true;
	};
}();
