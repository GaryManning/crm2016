function init() {
	$('#adddocTextArea').data('defaultText', TEXT_ENTER_NOTES);
	$('#adddocTextArea').val($('#adddocTextArea').data('defaultText'));
	$('#adddocDoneButton').prop('disabled', true);

	$('#adddocForm').prop('action', serverBridge+'uploaddocument');
	$('#adddocFileInput').change( function () {
		if ( $('#adddocFileInput').val() != "" && $('#adddocTextArea').val() != "" ) {
			$('#adddocDoneButton').prop('disabled', false);
		}
	});

	$('#adddocTextArea').click( function () {
		if ( $('#adddocTextArea').val() == TEXT_ENTER_NOTES ) {
			$('#adddocTextArea').val("");
		}
	} ).blur( function () {
		if ( $('#adddocTextArea').val() == "" ) {
			$('#adddocTextArea').val($('#adddocTextArea').data('defaultText'));
			$('#adddocDoneButton').prop('disabled', true);
		} else {
			if ( $('#adddocFileInput').val() != "" ) {
				$('#adddocDoneButton').prop('disabled', false);
			}
		}
	} ).keyup( function () {
		if ( $('#adddocTextArea').val() == "" ) {
			$('#adddocDoneButton').prop('disabled', true);
		} else {
			if ( $('#adddocFileInput').val() != "" ) {
				$('#adddocDoneButton').prop('disabled', false);
			}
		}
	} ).focus( function () {
		if ( $('#adddocTextArea').val() == "" ) {
			$('#adddocDoneButton').prop('disabled', true);
		} else {
			if ( $('#adddocFileInput').val() != "" ) {
				$('#adddocDoneButton').prop('disabled', false);
			}
		}
	});

	$('#adddocCancelButton').on('click', function () {
		window.close();
	});

	var iframe = $('<iframe name="hiddenIframe" id="hiddenIframe" style="border-width: 0px; width: 0px; height: 0px;"><html><head></head><body><div><p>Waiting...</p></div></body></html></iframe>');
	$('#adddocDiv').append(iframe);
}

function addDocument(params) {
	/*
	 * Overview: this function presents a file upload dialog to the user and then uploads the file
	 * the 'bridge' server (which in turn adds it to the selected record)
	 * Parameters:
	 * ids - an array of the selected ids (though it should only ever be one since it can only be called from a form)
	 * entity - 'penalty' or 'prosecution'
	 * set 'Debug.logging = true;' to get loggin to the console
	 */

	var data = decodeURIComponent(params.split(/=/)[1]);
	var id = data.split(/&/)[0].split(/=/)[1];
	var entity = data.split(/&/)[1].split(/=/)[1];
	var winWidth = data.split(/&/)[2].split(/=/)[1];
	var winHeight = data.split(/&/)[3].split(/=/)[1];

	// Show the dialog
	$('#adddocForm').data('count', 0);

	function createUpload(server) {
		//Log("Sending create upload request.");

		var url = server;
			
		//return $.ajax({
		return callWrapper({
			dataType: 'jsonp',
			url:  url,
			success: function (data, textStatus, XmlHttpRequest) {
				// Should get something like '{"response": "12345-abcde"}
				if ( !data.response ) {
					// something failed so alert the user
					alert("Error: wrong data format returned during upload creation phase.");
					hideAddDocDiv();
					// Strictly speaking the XmlHttpRequest has succeeded so we need to manually force an error condition that can be picked up in the next section
					// Maybe I could look at 'throw'ing an error - but maybe I'll do that in the future
					data.response = "ERROR";
					return;
				}
				// Add the upload id to the form before it is submitted...
				$('#adddocUploadIdHidden').prop('value', data.response);
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				if ( textStatus === "parsererror" ) {
					// probably a sign-on error so just ignore it...
					return;
				}
				if (XmlHttpRequest && errorThrown ) {
					alert("Error during the upload creation phase; Error (textStatus): " + textStatus);
				}
				hideAddDocDiv();
			}
		});
	}

	function getUploadProgress(timer, server, id) {
		//Log("progress count is: "+$('#adddocForm').data('count'));
		if ( $('#adddocForm').data('count') == 5 ) {
			//Log("Progress counted out...");
			window.clearInterval(timer);		// looks like we're done so stop the timer
			hideAddDocDiv();
			return;
		} else {
			$('#adddocForm').data('count', $('#adddocForm').data('count') + 1);
		}
		//Log("Setting reference to: "+id);
		var	url = server + "?reference=" + id;
		return $.ajax({
			dataType: 'jsonp',
			url: url,
			success: function (data, textStatus, XmlHttpRequest) {
				//Log("Got: " + data.done);
				if ( data.done == false ) {
					if ( data.error ) {
						// there has been an error so alert the user
						window.clearInterval(timer);		// looks like we're done so stop the timer
						alert("There has been an error: " + data.error + ".\n");
						hideAddDocDiv();
					}
				} else {
					window.clearInterval(timer);		// looks like we're done so stop the timer
					hideAddDocDiv();
					// Refresh the Printed Docs Grid
					Xrm.Page.ui.controls.get("PrintedDocsGrid").refresh();
				}
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				window.clearInterval(timer);			// we're done so stop the timer
				hideAddDocDiv();
				alert("Error during the upload; 'textStatus' is:" + textStatus);
				//if (XmlHttpRequest && errorThrown ) {
					//alert("Error while printing during the print process; Error: " + XmlHttpRequest.responseText);
					//$('#adddocDiv').hide();
				//}
			}
		});
	}

	// Start the upload process by getting a reference from the server
	//var getReference = createUpload(serverBridge + "getuploadreference");
	var getReference;

	function loop(error, errorText, errorObject) {
		if ( errorText === "initial" || errorText === "parsererror" ) { 
			$.when( getReference = createUpload(serverBridge + "getuploadreference") )
			.fail(
				// The createUpload failed, it is likely to be due to a logon timeout
				// (particularly if the errorText is a 'parserError')
				// so make the call again and see what happens this time
				loop
			);
		} else {
			alert("Error encountered while trying to re-new logon. Message: " + errorText);
		}
	}

	loop("", "initial");

	function submitForm(e) {
		// When the 'Done' button is pressed the document is uploaded
		$('#adddocDoneButton').prop('disabled', true);
		//$('#adddocDoneButton').unbind("click");
		//Log("Submitting the form");
		$.when( getReference ).done( function (createUploadResult) {
			var uploadId = createUploadResult.response;
			if ( uploadId == "ERROR" ) {
				//Log("Forced error encountered.");
				return;
			}
			//Log("Upload ref: "+uploadId);

			$.when( $('#adddocForm').submit() ).then( function () {
				//Log("starting the upload timer");
				var timer;
				timer = window.setInterval(function () { getUploadProgress(timer, serverBridge + "getuploadstatus", uploadId); }, 3000); 
				getUploadProgress(timer, serverBridge + "getuploadstatus", uploadId); 
				return true;
			},
			// Fail section
			function (error) {
				//Log("Progress timer NOT started.");
				if ( error.statusText === "timeout" ) {
					alert("Timeout error." + XmlHttpRequest.responseText);
				}
				if ( error && error.responseText ) {
					alert("Error - " + error.statusText);
				}
				hideAddDocDiv();
			}
			);

		});
	}

	function hideAddDocDiv() {
		/*
		$('#adddocEntityHidden').remove();
		$('#adddocGuidHidden').remove();
		$('#adddocDoneButton').unbind();
		//$('#adddocTextArea').unbind();
		$('#adddocCancelButton').unbind();
		$('#adddocDiv').hide();
		*/
		window.close();
	}


	if ( !$('#adddocEntityHidden').length ) {
		// Only run this section if it has not already been run
		//Log("Adding missing bits");
		$('#adddocDoneButton').bind("click", submitForm);

		$('#adddocForm')
			// These hidden element need to be added to the form at this point since we did not know
			// their values previously
			.append('<input id="adddocEntityHidden" type="hidden" name="entity" value="'+entity+'" />')
			.append('<input id="adddocGuidHidden" type="hidden" name="id" value="'+id+'" />');

		$('#adddocCancelButton').click( function () {
			hideAddDocDiv();
		});
	}

}
