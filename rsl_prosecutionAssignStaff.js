rsl_load_css_file("../WebResources/rsl_assignstaffstyles");

jQuery.fn.center = function () {
	// A jQuery function to centre the popup
	this.css("position", "absolute");
	this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
	this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
	return this;
}

var ConfigureAjaxUpdating = function () {
	// Popup a window let the user know that something is happening
	var Completed = false;
	var text = "Updating Assignments...";
	return function () {
		if ( Completed ) { return; }
		$('body').append('<div id="updatingDiv"></div>');
		$('#updatingDiv').append('<p id="updatingText">' + text + '</p>')
	 	 	.css('background', 'url(/_imgs/AdvFind/progress.gif) no-repeat center')
			.css('background-color', '#CCCCCC')
			.css('height', '150px')
			.css('width', '150px')
			.center()
			.hide()
			.ajaxStop( function () {
				if ( $(this).is(":visible") ) {
					$(this).hide();
					try {
						//Log("Trying to refresh the grid...");
						//window.parent.opener.document.getElementById("crmGrid").control.refresh();
						document.getElementById("crmGrid").control.refresh();
					} catch (e) {
						try {
							//Log("Trying to refresh the form...");
							setTimeout( function () { parent.location.reload(true); }, 250);
							} catch (f) {
								Log("Even the form refresh failed...");
						}
					}
				} else {
				}
			});

		$('#selectStaffDiv').data('listBuilt', false);

		//$('#staffDiv').center();
		$('#staffDoneButton').prop('disabled', true);

		Completed = true;
	};
}();

function prosecutionAssignStaff(ids) {

	selectedIds = ids;

	ConfigureAjaxUpdating();

	// Get a list of the Backoffice Staff and present it in a drop-down list.
	if ( ! $('#selectStaffDiv').data('listBuilt') ) {
		$.when( getStaff() ).then( function (staffData) {
			//Log("Got staff data");
			$('#loadingMessage').hide();
			$('#staffDiv').show();
		});
	} else {
		$('#staffDoneButton').unbind('click'); 
		$('#staffDoneButton').bind('click', doneButtonHandler);
		$('#loadingMessage').hide();
		$('#staffDiv').show();
	}

	// Set up the variables
	var entitySet = "rsl_prosecutionSet";
	var URL = serverXRMServices + entitySet;

	function updateRecord(guid, id, name) {
		// updateRecord: see http://rajeevpentyala.wordpress.com/2012/02/05/update-record-using-odata-and-jquery-in-crm-2011/

		// Create and build an object that can be used to transport the new data to the database. This object will be the same for all the prosecutions to be processed
		var entityObject = new Object;

		ownerId = new Object;
		ownerId.Id = id;
		ownerId.LogicalName = "rsl_backofficestaff";
		ownerId.Name = name;

		entityObject.rsl_backofficeownerId = ownerId;

		var jsonEntity = window.JSON.stringify(entityObject);

		$.ajax({
			type: "POST",
			contentType: "application/json; charaset=utf-8",
			datatype: "json",
			data: jsonEntity,
			url: serverXRMServices + "rsl_prosecutionSet" + "(guid'" + guid + "')",
			beforeSend: function (XMLHttpRequest) {
				//Specifiying this header ensures that the results will be returned as JSON
				XMLHttpRequest.setRequestHeader("Accept", "application/json");
				// Specify the HTTP method MERGE to update just the changes you are submitting
				XMLHttpRequest.setRequestHeader("X-HTTP-METHOD", "MERGE");
			},
			success: function (data, textStatus, XmlHttpRequest) {
				// 29Jan14 - Decided notes not needed ownership change as the changes are recorded in the audit trail
				//AddNotes(id, status, note);
				window.close();
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				if (XmlHttpRequest && XmlHttpRequest.responseText ) {
					alert("Error while updating " + entitySet + "; Error: " + XmlHttpRequest.responseText.error.message.value);
				}
			}
		});
	}

	function doneButtonHandler(e) {

		var staffId = $(e.target).data('selectionId');
		var fName = $(e.target).data('selectionFirstName');
		var lName = $(e.target).data('selectionLastName');

		if ( confirm("You have chosen " + fName + " " + lName + " to be the owner of the selected Prosecution records. Press 'Ok' to continue otherwise press 'Cancel'") == false ) {
			return false;
		}

		$('#staffDiv').hide();
		$('#updatingDiv').show();

		$.each(selectedIds, function (index, value) {
			updateRecord(value, staffId, lName);
		});
		return true;
	}

	function getStaff () {
		// set some (function) global variables
		var cssNotSelected = {"background": "#ffffff", "color": "#000000"}
		var cssSelected = {"background": "#000000", "color": "#ffffff"}

		var entitySet = "rsl_backofficestaff"+"Set";
			
		var URL = serverXRMServices + entitySet;
		URL = URL + "?$select=rsl_backofficestaffId,rsl_bo_forename,rsl_bo_surname,rsl_bo_fullname";

		$.ajax({
			type: "GET",
			contentType: "application/json; charaset=utf-8",
			datatype: "json",
			url: URL,
			beforeSend: function (XMLHttpRequest) {
				//Specifiying this header ensures that the results will be returned as JSON
				XMLHttpRequest.setRequestHeader("Accept", "application/json");
			},
			success: function (data, textStatus, XmlHttpRequest) {
				if ( data.d.results.length != 0 ) {
					// Build list of documents with the returned data
					var staffList = $('<ul id="staffListUL"></ul>');
					$(data.d.results).each( function (index, value) {
						staffList.append(
							$('<li>'+value.rsl_bo_forename+' '+value.rsl_bo_surname+'</li>').bind('click',
								function () {
									$('#staffListUL > li').css(cssNotSelected);
									$('#staffDoneButton').prop('disabled', false);
									$('#staffDoneButton').data('selectionId', value.rsl_backofficestaffId);
									$('#staffDoneButton').data('selectionFirstName', value.rsl_bo_forename);
									$('#staffDoneButton').data('selectionLastName', value.rsl_bo_surname);
									$(this).css(cssSelected);
								})
						);
					});
					$('#selectStaffDiv').append(staffList);
					$('#selectStaffDiv').data('listBuilt', true);
					$('#staffDoneButton').bind('click', doneButtonHandler);
					$('#staffCancelButton').click( function () {
							$('#staffListUL > li').css(cssNotSelected);
							$('#staffDoneButton').prop('disabled', true);
							$('#staffDiv').hide();
							window.close();
					});
					//$('#staffDiv').show();
					//$('#loadingMessage').hide();
				} else {
					alert("No data found.");
				}
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				if (XmlHttpRequest && XmlHttpRequest.responseText ) {
					alert("Error while retrieving " + entitySet + "; Error: " + XmlHttpRequest.responseText);
				}
			}
		});
	}
}
