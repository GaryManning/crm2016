// jQuery additions
jQuery.fn.center = function () {
	// A jQuery function to centre the popup
	this.css("position", "absolute");
	this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
	this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
	return this;
}

jQuery.fn.HCenter = function () {
	// A jQuery function to Horizontally centre the element
	this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
	return this;
}

function messageSetUp(entryPoint) {
	// Set up the messaging system between the windows
	$(window).on('message', function(event) {
		event = event.originalEvent;

		if ( event.origin !== Xrm.Page.context.getClientUrl() ) {
			alert("Invalid origin found in window.");
			return;
		}

		winSource = event.source;
		winOrigin = event.origin;

		if ( event.data === "Ready" ) {
			if ( ids ) {
				return;
			}
			event.source.postMessage("ids", event.origin);
			return;
		}

		data = JSON.parse(event.data);
		if ( data.ids ) {
			ids = data.ids;
			entryPoint(ids);
			return;
		}

		/*
		if ( data.size ) {
			popupWinWidth = data.size.width;
			popupWinHeight = data.size.height;
			return;
		}

		if ( data.lookup ) {
			// We've got the looked-up item here
			// Need to do something with it...
			VM.selectedCourt(data.lookup.name);
			VM.selectedCourtId = data.lookup.id;

			VM.getCourtSlots(VM.selectedCourtId, ids.length, jurisdictionCourt);
			document.getElementById('selectedCourtField').value = VM.selectedCourt();
			lookupWindow.close();
		}
		*/

	});
	// window.opener.postMessage does not seem to work
	//window.opener.postMessage("Ready", Xrm.Page.context.getClientUrl());
}

var ConfigureAjaxUpdating = function () {
	// Popup a window let the user know that something is happening
	var Completed = false;
	return function () {
		if ( Completed ) { return; }

		$('body').append('<div id="updatingDiv"></div>');
		$('#updatingDiv').append('<p id="updatingText"></p>')
			.css('background', 'url(/_imgs/AdvFind/progress.gif) no-repeat center')
			.css('background-color', '#CCCCCC')
			.css('height', '150px')
			.css('width', '150px')
			.center()
			.hide();

		$(document).ajaxStop( function () {
			if ( $('#updatingDiv').is(":visible") ) {
				$('#updatigDiv').hide();
				try {
					// Try to refresh the view (grid)...
					window.parent.opener.document.getElementById("crmGrid").control.refresh();
					window.close();
				} catch (e) {
					try {
						// ... if that fails then we are probably on a form so try refreshing that...
						setTimeout( function () { window.parent.opener.parent.location.reload(true);window.close(); }, 250);
					} catch (f) {
						// ... if that didn't work then get the user to refresh the page themself
						alert("The update completed successfully, but the page refresh failed. You may need to reload the page to see the updated details.");
					}
				}
			}
		});

		$('#updatingText').css('text-align', 'center')
			.css('width', '100%')
			.css('position', 'absolute')
			.css('margin', '0px')
			.css('bottom', '5px');

		$('#buttonBox')
			.css('position', 'absolute')
			.css('bottom', '15px')
			.HCenter();

		Completed = true;
	};
}();
