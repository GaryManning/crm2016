function prosecutionClosure (selectedIds) {
	// Set the popup window size parameters
	try {
		if ( Xrm.Page.data.entity.getIsDirty() ) {
			// If this is a form then check to see if any fields have been changed before continuing
			alert("Please save the form before closing the Penalty");
			return;
		}
	} catch (e) {
		// Do nothing - we must be on a grid rather than a from
	}

	var params = "selectedIds=" + selectedIds;

	Xrm.Utility.openWebResource("rsl_prosecutionWizard", params, 450, 700);
}

function rsl_refresh() {
	try {
		document.getElementById('crmGrid').control.Refresh();
	} catch (e) {
		try {

			//Reload Form
			// SAH 29-Jul-16  CRM2016 Fix
			//window.location.reload(true);
			window.parent.location.reload(true);
		} catch (f) {
			alert("Error while refreshing the form. Close and re-open the record or refresh it manually by pressing F5.");
		}
	}
}

