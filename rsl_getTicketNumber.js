// getTicketNumber.js
// This script will run when the form saves and will detect whether
// there is a ticket number for it. If not then it will request one from the server
// 

function getPrefix (toc) {
	//var toc = Xrm.Page.getAttribute('rsl_toc').getValue()[0].id.slice(1,-1);
    var url = serverXRMServices + "rsl_tocSet?$select=rsl_tocId,rsl_toc_name,rsl_toc_pfnprefix,rsl_toc_mg11prefix&$filter=rsl_tocId eq (guid'" + toc + "')";

    return $.ajax({
        type: "GET",
        contentType: "application/json; charaset=utf-8",
		url: url,
        datatype: "json",
        beforeSend: function(XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function(data, textStatus, XmlHttpRequest) {
			// Save the results to a variable
        },
        error: function(XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText) {
                alert("Error while retrieving TOC prefixes; Error: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
            }
        }
    });
}

function getNewNumber() {
    var formType = Xrm.Page.ui.getFormType();
    if (formType != 1) {
		// If this is not a new form then just return
		return;
	}

	var params =  { count: 1, showtext: true };		// parameters
	var serverPath = "/getpfnnumbers";				// path to ticket number server
	var url = serverBridge + serverPath;
	var retrieved = 0;

	return $.getJSON(url + "?callback=?", params, function (data) {
			retrieved = data.text[0];
        });
}

function setNewNumber (tocId, variant) {
	var dataField = { Penalty: "rsl_ticketnumber", Prosecution: "rsl_name" };

	if ( Xrm.Page.getAttribute('rsl_toc').getValue() === null ) {
		Xrm.Page.getAttribute(dataField[variant]).setValue("");
		return;
	}

	$.when( getPrefix(tocId), getNewNumber() ).done( function (prefix, ticketNumber) {
			var prefix_name = { Penalty: "rsl_toc_pfnprefix", Prosecution: "rsl_toc_mg11prefix" };
			var newNumber = prefix[0].d.results[0][prefix_name[variant]] + ticketNumber[0].text[0];
			Xrm.Page.getAttribute(dataField[variant]).setValue(newNumber);
	});
}
