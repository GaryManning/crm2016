/*
 * globalVariables.js
 *
 * Centralised storage file for various global variables
 *
 */
var server;

// I was hoping to be able to automate this bit using Xrm.Page.context.getClientUrl()
// but unfortunately the Xrm variable is not always in scope to the script.
// Perhaps one day I will figure out how to load the context stuff, but until then you
// need to select just ONE of the following 'server' lines - all the other variables
// should get set accordingly.
//
// If a new server is to be added then a new server line (and appropriate comment)
// needs to be added below here, plus a new 'case' section within the switch block
// lower down
//

// Raspberry Development Server
//server = "https://raspberry.mycrmhosted.net";

// GAG Server
//server = "https://tips.mycrmhosted.net";

// TPE Server
server = "https://fg-tips.mycrmhosted.net";

var serverHost 			= server;
var serverWebResources  = "/WebResources/";
var serverOdataEndpoint = "/XRMServices/2011/OrganizationData.svc/";
var serverBridge;

// Court Booking object type code (used in court booking wizard for look-ups)
var objectTypeCode;

switch (server) {
	// Raspberry Development Server
	case "https://raspberry.mycrmhosted.net":
		serverBridge	= "https://rsl-serv03.raspberry.local/tips/secure/";
		objectTypeCode	= "10005";
		break;

	// GAG Server
 	case "https://tips.mycrmhosted.net":
		serverBridge	= "https://apps1.raspberrysoftware.com/secure/2016_stage/tips/";
		objectTypeCode	= "10007";
		break;

	// TPE Server
	case "https://fg-tips.mycrmhosted.net":
		serverBridge			= "https://apps1.raspberrysoftware.com/secure/tpe/tips/";
		CourtBookingTypeCode	= "10009";
		BankPeriodTypeCode		= "10007";
		break;
}

var serverWebResources = serverHost + serverWebResources;
var serverXRMServices  = serverHost + serverOdataEndpoint;
var serverPing         = serverBridge + "ping";

function callWrapper(call, callAgain) {

	// Link jQuery to the system jQuery (in the parent widow)
	$ = parent.$;

	// Check to see if showModal exists. It is deprecated so at some point will disappear from all browsers
	if ( typeof window.showModalDialog === "undefined" ) {
		return $.ajax( call ).then( function (data ) { return data; });
	}

	/*
	 * A script to check the return status of all calls to the bridge server and serve a login
	 * page if requred - ie the bridge connection has timed out.
	 * It works by intercepting a 'parsererror' from the server and popping up a login screen
	 * if found. Then the call is retried and if successful then just returns the data.
	 * If it fails a second time then there is something else wrong so don't try the call
	 * again - an infinite loop will ensue.
	 */
	return $.ajax( call )
		.then(
			function( data ) {
				return data; //$.Deferred(function(d) { d.resolve(call); });
			},
			function(error, errorText, errorObject) {
				if (errorText === "parsererror") {
					// if 'callAgain' is set then drop out of the function as something has gone wrong and we dont want to carry on looping ad infinitum
					if ( !callAgain ) {
						var ping = serverBridge + "ping?close=pleasedoit";
						window.showModalDialog(ping, null, "center: yes; dialogHeight: 300px; dialogWidth: 500px; resizable: yes");
						//return callWrapper(call, true);
					} else {
						alert("Error encountered while trying to re-new logon. Message: " + errorText);
					}
				} else {
					return $.Deferred(function(d) {
						d.reject(error);
					});
				}
			}
			)
}


// Create a (useful) Debug function (use: call 'Debug.logging = true' in the script, then 'Log(<message>)' thereafter
var Debug = { logging: false, print: function (message) { if ( Debug.logging ) { if ( window.console ) { console.log(message); } else { alert(message); } return; } } }; var Log = Debug.print;

// A useful function to load style sheets
function rsl_load_css_file(filename) { 
	var fileref = document.createElement("link") 
	fileref.setAttribute("rel", "stylesheet") 
	fileref.setAttribute("type", "text/css") 
	fileref.setAttribute("href", filename) 
	document.getElementsByTagName("head")[0].appendChild(fileref) 
}

// Function to change the URL of the 'logon' Iframe attached to some forms
function changeIframeURL() {
	//var server = serverBridge + "ping";
	var iframeName = "IFRAME_server_connect";
	var iframe;
	try {
		iframe = Xrm.Page.ui.controls.get(iframeName);
		iframe.setSrc(serverPing);
	} catch (e) {
		alert("Error: " + e.message + " - during setSrc().");
	}
}

// Text strings
var TEXT_ENTER_NOTES = "Enter some notes...";
