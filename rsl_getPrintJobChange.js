function jobStatusChangeOnLoad () {
	// Function to loop round and get the current status of a print job

	Xrm.Page.getAttribute('rsl_printjobstatus').setSubmitMode("never");
	// Get the initial status
	var status = Xrm.Page.getAttribute('rsl_printjobstatus').getValue();

	//if ( status !== "866810000" ) {
	if ( status === "866810001" || status === "866810002" ) {
		// (NB 866810000 is 'Initialising', 866810001 is 'Completed - Success' and 86680002 is 'Completed - Failure')
		// Nothing more to do so just tidy up and return
		tidyUp(false)
		return;
	}

	// Make the inital call to the loop
	jobStatusChangeLoop(status);
}

function tidyUp(inLoop) {
	if ( Xrm.Page.getAttribute('rsl_completionnote').getValue() === null && inLoop ) {
		setTimeout( function() {
			alert("Batch print finished. You may now enter details in the 'Verification Note' field.");
    		Xrm.Page.getControl("Printed_Documents").refresh();
		}, 1000);
	}

	// Enable the note field
	Xrm.Page.ui.controls.get('rsl_completionnote').setDisabled(false);
	// Enable the Reprint button
	$(Xrm.Page.ui.controls.get('WebResource_btnReprintPlaceHolder').getObject()).parent().next().attr('disabled', false);
	// One last refresh of the form before exiting
    Xrm.Page.getControl("Printed_Documents").refresh();
}

function jobStatusChangeLoop (status) {
	// Call the server to get the latest status
	$.when( getprintjobchange(status) ).then( function (data) {
			// When the server returns with the current status update the form field
			Xrm.Page.getAttribute('rsl_printjobstatus').setValue(data.status);
			
			if ( data.status !== 866810000 ) {
				// If the status is not initialising then it must be complete so just tidy up and return
				tidyUp(true);
				return;
			} else {
				// If the print has still not finished then refresh the grid and call the loop again this time with the current status to indicate that it was not called by the form onLoad event
    			Xrm.Page.getControl("Printed_Documents").refresh();
				setTimeout(function () { jobStatusChangeLoop(status); }, 100);
			}
		});
}

function getprintjobchange () {
	var status = Xrm.Page.getAttribute('rsl_printjobstatus').getValue();
	var id = Xrm.Page.data.entity.getId();

	var server = serverBridge + "getprintjobchange";
	var	url = server + "?print=" + id + "&status=" + status;
	return $.ajax({ dataType: 'jsonp', url: url,
			// No need for a 'success' method as it just returns with the data
			error: function(XmlHttpRequest, textStatus, errorThrown) {
						if (XmlHttpRequest && XmlHttpRequest.responseText) {
							alert("Error while retrieving print status; Message: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
						}
			}
		});
}
