// addDocumentButtons.js
// This script will run when the form loads

function addDocumentButtons () {
	// This is the function called by the onLoad event of the form.
	//
	// 03March2014
	// Download document button no longer needed as it has moved to the ribbon
	//

	// Set debugging
	Debug.logging = false;

	// The form field that holds the document Id
	var dataField = "rsl_documentid";

	// Get the id
	var docId = Xrm.Page.getAttribute(dataField).getValue();

	if ( docId == undefined ) {
		// No id found, so inform the user
		//alert("No document is found");
		// Display an error message in place of the PDF preview
		var iFrame = "IFRAME_pdf_document";
		//Xrm.Page.getControl(iFrame).setVisible(true);
		Xrm.Page.getControl(iFrame).setSrc(serverWebResources + "rsl_previewDocumentError");
		return;
	}

	function addLinksToButton(fieldname, url, inline, title) {
		// A function to add 'click' functionality to the View/Download icons
		crmForm.all[fieldname].style.cursor="hand";
		crmForm.all[fieldname].attachEvent("onclick",function () { return clickFunction(url, inline); } );
		crmForm.all[fieldname].title=title;
	}

	function clickFunction (url, inline) {
		// Function to actually action the button click
		//Log("Button clicked with: "+url);
		if ( inline == true ) {
			window.open(url, "_blank", null, false);
		} else {
			window.location.href = url;
		}
	}

	// Build the url to the server...
	var URL = serverBridge + "getdocument";
	
	// Add 'click' to the 'View' button
	//addLinksToButton("WebResource_btnViewPlaceHolder", URL+"?inline=true&id="+docId, true, "View Document");
	// Add 'click' to the 'Download' button
	//addLinksToButton("WebResource_btnDownloadPlaceHolder", URL+"?id="+docId, false, "Download Document");

	// Display the PDF on the form (in an iFrame)
	var iFrame = "IFRAME_pdf_document";
	//Xrm.Page.getControl(iFrame).setVisible(true);
	Xrm.Page.getControl(iFrame).setSrc(URL+"?inline=true&id="+docId);
}

function downloadDocument () {
	// The form field that holds the document Id
	var dataField = "rsl_documentid";
	// Get the id
	var docId = Xrm.Page.getAttribute(dataField).getValue();
	var URL = serverBridge + "getdocument" + "?id=" + docId;
	window.location.href = URL;
}
