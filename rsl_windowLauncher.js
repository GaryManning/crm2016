function rsl_windowLauncher(wr, ids, params) {
	
	switch (wr) {
		case "rsl_assignCourtWizard":
			if ( ids.length === 0 ) {
				alert("No records selected to assign.");
				return;
			}
			winWidth = 560;
			winHeight = 670;
			break;
		case "rsl_adjourncase":
			if ( ids.length === 0 ) {
				alert("No records selected to adjourn.");
				return;
			}
			winWidth = 560;
			winHeight = 670;
			break;
		case "rsl_assignStaffPage":
			if ( ids.length === 0 ) {
				alert("No records selected to assign.");
				return;
			}
			winWidth = 450;
			winHeight = 200;
			break;
		case "rsl_prosecutionWizard":
			if ( ids.length === 0 ) {
				alert("No records selected to close.");
				return;
			}
			winWidth = 450;
			winHeight = 535;
			break;
		case "rsl_reopenProsecution":
			if ( ids.length === 0 ) {
				alert("No records selected to re-open.");
				return;
			}
			winWidth = 560;
			winHeight = 670;
			break;
		case "rsl_penaltyClosureWizard":
			if ( ids.length === 0 ) {
				alert("No records selected to close.");
				return;
			}
			winWidth = 560;
			winHeight = 317;
			break;
		default:
			winWidth = 450;
			winHeight = 700;
			break;
	}

	//selectedIds = ids;
	var idsObject = {'ids': ids};
	var sizeObject = {'size': {'width': winWidth, 'height': winHeight}};

	var popupReady = false;

	if ( !params ) {
		params = null;
	}
	
	//Xrm.Utility.openWebResource(script, "selectedIds=" + selectedIds, winWidth, winHeight);
	win = Xrm.Utility.openWebResource(wr, params, winWidth, winHeight);

	$(window).off('message');
	$(window).on('message', function(event) {
		event = event.originalEvent;

		if ( event.origin !== Xrm.Page.context.getClientUrl() ) {
			alert("Invalid origin when opening '" + wr + "'.");
			return;
		}

		if ( event.data === "ids" || event.data === "Ready" ) {
			// Send the selected ids to the window
			event.source.postMessage(JSON.stringify(idsObject), event.origin);
			popupReady = true;
		}

		if ( event.data === "size" ) {
			// Send the intial size of the window (useful for resizing back to the original size)
			event.source.postMessage(JSON.stringify(sizeObject), event.origin);
		}
	});

	// After a delay, post a 'Ready' message to the popup
	// (in the popup, window.opener.postMessage does not work for some reason
	// so we post a message to it which it can use to get the 'source' from)
	function checkPopupReady(count) {
		//console.log("checkPopup count: " + count);
		if ( popupReady ) {
			return;
		}

		var delay = 500;
		if ( count > 3 ) {
			delay = 1000;
		}
		if ( count > 7 ) {
			delay = 2000;
		}
		if ( count > 10 ) {
			return;
		}
		win.postMessage("Ready", Xrm.Page.context.getClientUrl());
		setTimeout(function() { checkPopupReady(count + 1); }, delay);
	}

	checkPopupReady(1);
}

function rsl_refresh() {
	// Function to refresh the grid or the form - called by the web resource opened above
	try {
		document.getElementById('crmGrid').control.Refresh();
	} catch (e) {
		try {

			//Reload Form
			// SAH 29-Jul-16  CRM2016 Fix
			//window.location.reload(true);
			window.parent.location.reload(true);
		} catch (f) {
			alert("Error while refreshing the form. Close and re-open the record or refresh it manually by pressing F5.");
		}
	}
}
