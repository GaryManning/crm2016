function assignCourtWizard(selectedIds) {
	// Load the globalVariable library
	//var globalVariables = new ActiveXObject("Microsoft.XMLHTTP");
	//globalVariables.open("GET", "../WebResources/rsl_globalVariables", false);
	//globalVariables.send();
	//eval(globalVariables.responseText);

	//loadScript("../WebResources/rsl_globalVariables");

	// Set the popup window size parameters
	var top = 200;
	var left = 400;
	var height = 280;
	var width = 490;
	var toolbar = "no";
	var menubar = "no";
	var loc = "no";
	var resizable = "yes";

	try {
		if ( Xrm.Page.data.entity.getIsDirty() ) {
			// If this is a form then check to see if any fields have been changed before continuing
			alert("Please save the form before closing the Penalty");
			return;
		}
	} catch (e) {
		// Do nothing - we must be on a grid rather than a from
	}

	var windowParams = "dialogTop:" + top + "px; dialogLeft:" + left + "px; dialogHeight:" + height + "px; dialogWidth:" + width + "px; toolBar: "+ toolbar + "; menubar:" + menubar + "; location:" + loc + "; resizable:" + resizable +";";
	var windowUrl = serverWebResources + "rsl_assignCourtWizard";

	var params = "selectedIds=" + selectedIds;

	Xrm.Utility.openWebResource("rsl_assignCourtWizard", params, 450, 700);
}

function rsl_refresh() {
	// Function to refresh the grid or the form - called by the web resource opened above
	try {
		document.getElementById('crmGrid').control.Refresh();
	} catch (e) {
		try {

			//Reload Form
			// SAH 29-Jul-16  CRM2016 Fix
			//window.location.reload(true);
			window.parent.location.reload(true);
		} catch (f) {
			alert("Error while refreshing the form. Close and re-open the record or refresh it manually by pressing F5.");
		}
	}
}
