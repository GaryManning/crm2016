function getASNs () {

	var recordsFound = 0;
	var records = [];
	var bookedCourt = Xrm.Page.data.entity.getId();

	function getProsecutions (nextURL) {
		var entitySet = "rsl_prosecution"+"Set";
			
		var URL;
		if ( nextURL ) {
			URL = nextURL;
		} else {
			URL = serverXRMServices + entitySet;
			URL = URL + "?$select=rsl_prosecutionId&$filter=rsl_pros_courtappearance/Id eq (guid'" + bookedCourt + "') and rsl_pros_prosecutionstatus/Value eq 866810015";
		}

		$.ajax({
			type: "GET",
			contentType: "application/json; charaset=utf-8",
			datatype: "json",
			url: URL,
			beforeSend: function (XMLHttpRequest) {
				//Specifiying this header ensures that the results will be returned as JSON
				XMLHttpRequest.setRequestHeader("Accept", "application/json");
			},
			success: function (data, textStatus, XmlHttpRequest) {

				recordsFound += data.d.results.length;
				for (var i = 0, len = recordsFound; i < len; i++ ) {
					records.push([data.d.results[i].rsl_prosecutionId]);
				}
				//records = records.concat(guids);

				if ( data.d.__next ) {
					// Only 50 records can be retrieved at a time, so check
					// if there is a '__next' property and use it to call the getProsecutions function again
					// Hopefully this should just loop round and round until all the records have been retrieved
					getProsecutions(data.d.__next);
				} else {
					// This must be last one...
					if ( recordsFound === 0 ) {
						alert("There are no records that require an ASN sending.");
					} else { 
						if ( confirm("There are " + recordsFound + " records to process. Press 'Ok' to continue or 'Cancel' to cancel.") === false ) {
							return;
						}
						printDocuments(recordsFound, records, "rsl_prosecution", "", true);
					}
				}
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				if (XmlHttpRequest && XmlHttpRequest.responseText ) {
					alert("Error while retrieving " + entitySet + "; Error: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);

				}
			}
		});
	}

	getProsecutions();
}
