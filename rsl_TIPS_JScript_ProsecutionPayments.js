// Prosecution Payment 
//
// Remove the Payment Method Reference field that do not apply to the current Payment reference.
// Scenario:
// User choose Payment Method=Card Payment and enters card reference fields
// Changes Payment Method=CHeques and enter Chq reference field
// Saving at this time would have saved the Card reference fields which is not desired.
// we call this as we save to throw away the uncessary details at the last moment in case the user wanted to refer back to them.
prospayment_emptyunusedreferencefields = function() {
    var myPaymentMethod = Xrm.Page.getAttribute("rsl_pros_paymenttype").getValue();
    // if Method = "Card" (866810001)
    if (myPaymentMethod == 866810001) {
        Xrm.Page.getAttribute("rsl_prospayment_chq_nameoncheque").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_bank").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_number").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_accountnumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_sortcode").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_po_postoffice").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_po_ponumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_tvnumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_station").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_issuedate").setValue(null);
    }
    // Method = "Cheque" (866810002)
    else if (myPaymentMethod == 866810002) {
        Xrm.Page.getAttribute("rsl_prospayment_card_reference").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_po_postoffice").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_po_ponumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_tvnumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_station").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_issuedate").setValue(null);
    }
    // Method = "Postal Order" (866810007)
    else if (myPaymentMethod == 866810007) {
        Xrm.Page.getAttribute("rsl_prospayment_card_reference").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_nameoncheque").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_bank").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_number").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_accountnumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_sortcode").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_tvnumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_station").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_issuedate").setValue(null);
    }
    // Method = "Transfer Voucher" (866810005)
    else if (myPaymentMethod == 866810005) {
        Xrm.Page.getAttribute("rsl_prospayment_card_reference").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_nameoncheque").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_bank").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_number").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_accountnumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_sortcode").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_po_postoffice").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_po_ponumber").setValue(null);
    }
    // Clear all payment references
    else {
        Xrm.Page.getAttribute("rsl_prospayment_card_reference").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_nameoncheque").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_bank").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_number").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_accountnumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_chq_sortcode").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_po_postoffice").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_po_ponumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_tvnumber").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_station").setValue(null);
        Xrm.Page.getAttribute("rsl_prospayment_tv_issuedate").setValue(null);
    }
}

function checkCorrectionPayer() {
    var myPaymentPayer = Xrm.Page.getAttribute("rsl_prospayment_payer").getValue();
	if ( myPaymentPayer == 866810002 ) {
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("card").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("cheque").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("postal_order").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("transfer_voucher").setVisible(false);
	}
	prospayment_hideshow_paymentreference();
}

// *************** OnChange Payment Method ***************
// Show/Hide payment reference details based on the Method Chosen
prospayment_hideshow_paymentreference = function() {
    var myPaymentPayer = Xrm.Page.getAttribute("rsl_prospayment_payer").getValue();
	if ( myPaymentPayer == 866810002 ) {
		return;
	}
    var myPaymentMethod = Xrm.Page.getAttribute("rsl_pros_paymenttype").getValue();
    // if Method = "Card" (866810001)
    if (myPaymentMethod == 866810001) {
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("card").setVisible(true);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("cheque").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("postal_order").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("transfer_voucher").setVisible(false);
    }
    // Method = "Cheque" (866810002)
    else if (myPaymentMethod == 866810002) {
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("card").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("cheque").setVisible(true);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("postal_order").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("transfer_voucher").setVisible(false);
    }
    // Method = "Postal Order" (866810007)
    else if (myPaymentMethod == 866810007) {
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("card").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("cheque").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("postal_order").setVisible(true);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("transfer_voucher").setVisible(false);
    }
    // Method = "Transfer Voucher" (866810005)
    else if (myPaymentMethod == 866810005) {
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("card").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("cheque").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("postal_order").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("transfer_voucher").setVisible(true);
    }
    // Hide all
    else {
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("card").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("cheque").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("postal_order").setVisible(false);
        Xrm.Page.ui.tabs.get("prosecution_payment").sections.get("transfer_voucher").setVisible(false);
    }
}

Prosecution_CalcTotalPaid = function() {
    // Get the current Id - if this is a payment edit then we need to discount it from the overall total
    // (remove the first and last characters ('{' and '}') and convert it lower case so that it can be matched
    // with the query response which is returned as a lower case string with no braces)
    var paymentId = "";
    var formType = Xrm.Page.ui.getFormType(); // Capture the context of the form.  If 1 the NEW
    if (formType != 1) {
        paymentId = Xrm.Page.data.entity.getId().slice(1, -1).toLowerCase();
    }
    var id = Xrm.Page.getAttribute("rsl_proseuctionnumberid").getValue()[0].id;
    var totalCosts = 0;
    var totalCompensation = 0;

    $.ajax({
        async: false,
        type: "GET",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        url: serverXRMServices + "rsl_prosecutionpaymentSet?$select=rsl_prosecutionpaymentcostsreceived,rsl_rsl_prosecutionpaymentcompreceived&$filter=rsl_proseuctionnumberId/Id%20eq%20(guid'" + id + "')",
        beforeSend: function(XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function(data, textStatus, XmlHttpRequest) {
            for (i = 0; i < data.d.results.length; i++) {
                if (data.d.results[i].rsl_prosecutionpaymentId == paymentId) {
                    continue;
                }
                var costs = parseFloat(data.d.results[i].rsl_prosecutionpaymentcostsreceived);
                var compensation = parseFloat(data.d.results[i].rsl_rsl_prosecutionpaymentcompreceived);
                totalCosts += costs;
                totalCompensation += compensation;
            }

            //Xrm.Page.getAttribute("rsl_pros_money_costsreceived").setValue(totalCosts);
            //Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").setValue(totalCompensation);

            // Update the database
            var updateObject = new Object();
            updateObject.rsl_pros_money_costsreceived = totalCosts.toFixed(2);
            updateObject.rsl_pros_money_compensationreceived = totalCompensation.toFixed(2);
            updateProsecutionRecord(id, updateObject);
        },
        error: function(XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && errorThrown) {
                alert("Error while retrieving payment details; Message: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
            }
        }
    });
}

function updateProsecutionRecord(id, updateObject) {
    // updateProsecutionRecord: see http://rajeevpentyala.wordpress.com/2012/02/05/update-record-using-odata-and-jquery-in-crm-2011/
    var jsonEntity = window.JSON.stringify(updateObject);

    $.ajax({
        aysnc: false,
        type: "POST",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: serverXRMServices + "rsl_prosecutionSet(guid'" + id + "')",
        beforeSend: function(XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            // Specify the HTTP method MERGE to update just the changes you are submitting
            XMLHttpRequest.setRequestHeader("X-HTTP-METHOD", "MERGE");
        },
        success: function(data, textStatus, XmlHttpRequest) {
            // The update was successful, so now update the 'Notes' entity with the details of what was done
        },
        error: function(XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && errorThrown) {
                alert("Error while updating rsl_prosecutionpaymentSet; Message: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
            }
        }
    });
}


// ******************* ON SAVE EVENTS ****************************************
prospayment_savevalidation = function(executionObj) {

    // ****** Capture Form Values.
    var myBankPeriod = Xrm.Page.getAttribute("rsl_bankperiod").getValue();
    var myPaymentType = Xrm.Page.getAttribute("rsl_pros_paymenttype").getValue();
    var myPaymentTo = Xrm.Page.getAttribute("rsl_pros_paymentto").getValue();
    var myProsPaymentPaymentDate = Xrm.Page.getAttribute("rsl_prosecutionpaymentdatereceived").getValue();
    var mycostsreceived = Xrm.Page.getAttribute("rsl_prosecutionpaymentcostsreceived").getValue();
    var mycompreceived = Xrm.Page.getAttribute("rsl_rsl_prosecutionpaymentcompreceived").getValue();
    var myProsPaymentPayer = Xrm.Page.getAttribute("rsl_prospayment_payer").getValue();

    //
    //
    // Check we have an Open Bank Period
    if (myBankPeriod == null) {
        alert("There currently is no Bank Period created.  Payment/Credits cannot be created without an Open Bank Period.  Use 'Bank Periods' under 'Configuration' to open a Bank Period.");
        executionObj.getEventArgs().preventDefault();
        return;
    }

    //
    //
    // Check we have a payment date
    if (myProsPaymentPaymentDate == null) {
        alert("A payment date and time must be specified.");
        Xrm.Page.getControl("rsl_prosecutionpaymentdatereceived").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }


    //
    // Payment Type cannot be 'System Credit' 04 'Travel Warrant' 03 'Transfer Voucher' 05
    if (myPaymentType == 866810004 || myPaymentType == 866810003 || myPaymentType == 866810005) {
        alert("Record cannot be saved.  Payment Type cannot be 'System Credit', 'Travel Warrant' or 'Transfer Voucher' for a prosecution payment.");
        Xrm.Page.getControl("rsl_pros_paymenttype").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }

    //
    // Payment To cannot be 'Paid on spot (Insp)'
    if (myPaymentTo == 866810000) {
        alert("Record cannot be saved.  Payment To cannot be 'Paid on the Spot' for a prosecution payment.");
        Xrm.Page.getControl("rsl_pros_paymentto").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }

    //
    // Payment To cannot be 'Paid at Ticket Office'
    if (myPaymentTo == 866810004) {
        alert("Record cannot be saved.  Payment To cannot be 'Paid at Ticket Office' for a prosecution payment.");
        Xrm.Page.getControl("rsl_pros_paymentto").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }


    //
    // **** Ensure Mandaory Fields are filled in for Payment Method
    //
    // Card
    if (myPaymentType == 866810001) {
        var mycardref = Xrm.Page.getAttribute("rsl_prospayment_card_reference").getValue();
        if (mycardref == null) {
            alert("Record cannot be saved.  'Card Reference' must be specified.");
            Xrm.Page.getControl("rsl_prospayment_card_reference").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    // Cheque
    else if (myPaymentType == 866810002) {
        var mychqname = Xrm.Page.getAttribute("rsl_prospayment_chq_nameoncheque").getValue();
        var mychqbank = Xrm.Page.getAttribute("rsl_prospayment_chq_bank").getValue();
        var mychqnumber = Xrm.Page.getAttribute("rsl_prospayment_chq_number").getValue();
        var mychqaccnumber = Xrm.Page.getAttribute("rsl_prospayment_chq_accountnumber").getValue();
        var mychqsortcode = Xrm.Page.getAttribute("rsl_prospayment_chq_sortcode").getValue();
        if (mychqname == null || mychqbank == null || mychqnumber == null || mychqaccnumber == null || mychqsortcode == null) {
            alert("Record cannot be saved.  Cheque references 'Name on Chq', 'Bank', 'Number', 'Account Number' and 'Sort Code' must be specified.");
            Xrm.Page.getControl("rsl_prospayment_chq_nameoncheque").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    // Postal Order
    else if (myPaymentType == 866810007) {
        var mypopostoffice = Xrm.Page.getAttribute("rsl_prospayment_po_postoffice").getValue();
        var myponumber = Xrm.Page.getAttribute("rsl_prospayment_po_ponumber").getValue();
        if (mypopostoffice == null || myponumber == null) {
            alert("Record cannot be saved.  Postal Order references 'Post Office' and 'Number' must be specified.");
            Xrm.Page.getControl("rsl_prospayment_po_postoffice").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    // Transfer Voucher
    else if (myPaymentType == 866810005) {
        var mytvnumber = Xrm.Page.getAttribute("rsl_prospayment_tv_tvnumber").getValue();
        var mystation = Xrm.Page.getAttribute("rsl_prospayment_tv_station").getValue();
        var mytvissuedate = Xrm.Page.getAttribute("rsl_prospayment_tv_issuedate").getValue();
        if (mytvnumber == null || mystation == null || mytvissuedate == null) {
            alert("Record cannot be saved.  Transfer Voucher references 'Number', 'Station' and 'Issue Date' must be specified.");
            Xrm.Page.getControl("rsl_prospayment_tv_tvnumber").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }


    //
    // ***** ENSURE THE FIELD ARE NOT IN CONFLICT *****
    //
    // Payment amount must be specified
    //if ((mycostsreceived <= 0 && mycompreceived <= 0) || (mycostsreceived == null || mycompreceived == null)) {
    if ( mycostsreceived == null || mycompreceived == null ) {
        alert("Record cannot be saved.  An monetary amount must be specified for costs and/or compensation recieved. Set these values to the payment amount or to '0'.");
        executionObj.getEventArgs().preventDefault();
        return false;
    }

	
	// Check for 'Correction' payment
	//if ( myProsPaymentPayer === 866810002 || myPaymentTo === 866810005 || myPaymentType === 866810009 ) {
	if ( myProsPaymentPayer === 866810002 ) {

			// Check to make sure the 'reason' field is set
			if ( Xrm.Page.getAttribute("rsl_pros_payment_reason").getValue() !== null ) {
				// All checks passed - just fall through and carry on 
					
			} else {

				// There is no 'reason' set
				alert("A reason must be entered in the 'Reason' field when making corrective payments.");
				executionObj.getEventArgs().preventDefault();
				return;
			}
	}

    prospayment_emptyunusedreferencefields();
}

function setMonetaryFields () {
    if ( Xrm.Page.ui.getFormType() === 1 ) {

		Xrm.Page.getAttribute('rsl_prosecutionpaymentcostsreceived').setValue(0);
		Xrm.Page.getAttribute('rsl_rsl_prosecutionpaymentcompreceived').setValue(0);	// rsl_rsl_ !!!
	}
}
