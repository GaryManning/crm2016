// 'Penalty' Form validation Javascript

//
// Set all controls to be editable if the form is running in CREATE mode
Penalty_SetControlsCreateMode = function() {
    var formType = Xrm.Page.ui.getFormType();
    var myPenaltyStatusFormValue = Xrm.Page.getAttribute("rsl_penaltystatus").getValue(); // Capture Penalty Status On form

    // Closed Prosecuting or Imported
    if (myPenaltyStatusFormValue == 866810010 || myPenaltyStatusFormValue == 866810022) {
        //
        // Disable Notice Number
        Xrm.Page.ui.controls.get("rsl_ticketnumber").setDisabled(true);
        // Disable Status
        Xrm.Page.ui.controls.get("rsl_penaltystatus").setDisabled(true);
        // Disable Date of Offence
        Xrm.Page.ui.controls.get("rsl_dateofissue").setDisabled(true);
        // Disable Reason Code
        Xrm.Page.ui.controls.get("rsl_reasoncode").setDisabled(true);
        // Disable Date Closed
        Xrm.Page.ui.controls.get("rsl_dateclosed").setDisabled(true);
        // Disable Travel Class
        Xrm.Page.ui.controls.get("rsl_travelclass").setDisabled(true);
        // Disable Void Reason
        Xrm.Page.ui.controls.get("rsl_voidreason").setDisabled(true);
        // Disable Backoffice Owner
        Xrm.Page.ui.controls.get("rsl_backofficeownerid").setDisabled(true);

        // Appeal Arbibrator
        Xrm.Page.ui.controls.get("rsl_appealarbitrator").setDisabled(true);
        // Appeal Status
        Xrm.Page.ui.controls.get("rsl_appealstatus").setDisabled(true);
        // Appeal Received Date
        Xrm.Page.ui.controls.get("rsl_appealdate").setDisabled(true);
        // Appeal Closed Date
        Xrm.Page.ui.controls.get("rsl_appealcloseddate").setDisabled(true);

        // Disable Inspector
        Xrm.Page.ui.controls.get("rsl_inspector").setDisabled(true);
        // Disable Penalty Type
        Xrm.Page.ui.controls.get("rsl_penaltytype").setDisabled(true);
        // Disable Ticket Type
        Xrm.Page.ui.controls.get("rsl_tickettype").setDisabled(true);
        // Disable Issued On
        Xrm.Page.ui.controls.get("rsl_issuedon").setDisabled(true);
        // Disable Excuse Type
        Xrm.Page.ui.controls.get("rsl_excusetype").setDisabled(true);
        // Disable Passengers Excuse
        Xrm.Page.ui.controls.get("rsl_passengersexcuse").setDisabled(true);
        // Disable Brief Events
        Xrm.Page.ui.controls.get("rsl_briefevents").setDisabled(true);
        // Disable Person
        Xrm.Page.ui.controls.get("rsl_person").setDisabled(true);
        // Disable From/To/At Stations
        Xrm.Page.ui.controls.get("rsl_fromstation").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_ticketissuedat").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_tostation").setDisabled(true);
        // Penalty Fare Fine
        Xrm.Page.ui.controls.get("rsl_penaltyfarefine").setDisabled(true);
        // Total Admin Fee
        Xrm.Page.ui.controls.get("rsl_totaladminfee").setDisabled(true);
        // Single Fare Price
        Xrm.Page.ui.controls.get("rsl_singlefareprice").setDisabled(true);

    }
    else if (formType == 2) {
        //
        // Disable Notice Number
        Xrm.Page.ui.controls.get("rsl_ticketnumber").setDisabled(true);
        // Disable Status
        Xrm.Page.ui.controls.get("rsl_penaltystatus").setDisabled(true);
        // Disable Date of Offence
        Xrm.Page.ui.controls.get("rsl_dateofissue").setDisabled(true);
        // Disable Date Closed
        Xrm.Page.ui.controls.get("rsl_dateclosed").setDisabled(true);
        // Disable Inspector
        Xrm.Page.ui.controls.get("rsl_inspector").setDisabled(true);

        // Don't let the form to submit any changes to these fields, they will be handled automatically when the fileds are updated
        Xrm.Page.getAttribute("rsl_totalpaid").setSubmitMode("never");
        Xrm.Page.getAttribute("rsl_penaltyfarefine").setSubmitMode("never");
        Xrm.Page.getAttribute("rsl_totaladminfee").setSubmitMode("never");
        Xrm.Page.getAttribute("rsl_balanceremaining").setSubmitMode("never");
    }
    //
    // New Record or BULK edit
    else if (formType == 1 || formType == 6) {
        // Enable/Disable
        //
        // Enable Date of Offence
        Xrm.Page.ui.controls.get("rsl_dateofissue").setDisabled(false);
        // Enable Inspector
        Xrm.Page.ui.controls.get("rsl_inspector").setDisabled(false);
        // Hide Time to Issue ticket
        Xrm.Page.ui.controls.get("rsl_timetoissueticket").setVisible(false);
        //
        // Visibility
        //
        // Hide Penalty Photo Tab
        Xrm.Page.ui.tabs.get("Penalty_Photos").setVisible(false);
        // Hide Printed Docs Tab
        Xrm.Page.ui.tabs.get("Printed_Documents").setVisible(false);
        // Hide Timeline Tab
        Xrm.Page.ui.tabs.get("Timeline").setVisible(false);
        // Hide Admin Tab
        Xrm.Page.ui.tabs.get("Admin").setVisible(false);
        // Address on Offence
        Xrm.Page.ui.controls.get("rsl_addressonoffence").setVisible(false); ;
        // Hide Notice Number
        Xrm.Page.ui.controls.get("rsl_ticketnumber").setVisible(false);
        // Hide Date Closed
        Xrm.Page.ui.controls.get("rsl_dateclosed").setVisible(false);
        // Hide Chosen Not To Prosecute
        Xrm.Page.ui.controls.get("rsl_chosennottoprosecute").setVisible(false);
        // Hide Chosen Not To Prosecute
        Xrm.Page.ui.controls.get("rsl_noprosecutereason").setVisible(false);
        //
        // Set Sensible Default Values
        //
        Xrm.Page.getAttribute("rsl_dateofissue").setSubmitMode("always");
        Xrm.Page.getAttribute("rsl_dateofissue").setValue(new Date());

        //Xrm.Page.getAttribute("rsl_firstname").setSubmitMode("always");
        //Xrm.Page.getAttribute("rsl_firstname").setValue("Calculating...");

        //Xrm.Page.getAttribute("rsl_lastname").setSubmitMode("always");
        //Xrm.Page.getAttribute("rsl_lastname").setValue("Calculating...");

        //Xrm.Page.getAttribute("rsl_totaladminfee").setSubmitMode("never"); 	// Don't let the form to submit any changes, they will be handled automitcally with the penaltyFeeChange() event handler
        Xrm.Page.getAttribute("rsl_totaladminfee").setValue(0);

        Xrm.Page.getAttribute("rsl_backofficecreated").setSubmitMode("always");
        Xrm.Page.getAttribute("rsl_backofficecreated").setValue(866810001);
        //
        // Make sure these fields are updated in the database when the form closes
        Xrm.Page.getAttribute("rsl_totalpaid").setSubmitMode("always");
        Xrm.Page.getAttribute("rsl_penaltyfarefine").setSubmitMode("always");
        Xrm.Page.getAttribute("rsl_totaladminfee").setSubmitMode("always");
        Xrm.Page.getAttribute("rsl_balanceremaining").setSubmitMode("always");
    }
}



//Hide/Show Appeal Tab based on Penalty Status and data in the appeals fields
function Penalty_HideShowAppealTab() {
    // Capture Penalty Status
    var myPenaltyStatus = Xrm.Page.data.entity.attributes.get("rsl_penaltystatus");
    var myPenaltyStatusValue = myPenaltyStatus.getValue();
    // Capture Appeal Arbitrator
    var myAppealArbitrator = Xrm.Page.getAttribute("rsl_appealarbitrator");
    var myAppealArbitratorValue = myAppealArbitrator.getValue();
    // Capture Appeal Status
    var myAppealStatus = Xrm.Page.getAttribute("rsl_appealstatus");
    var myAppealStatusValue = myAppealStatus.getValue();
    // Appeal Reason not required for Go-Ahead as already recrded in appeals process.
    // Capture Appeal Reason
    //var myAppealReason = Xrm.Page.data.entity.attributes.get("rsl_appealreason");
    //var myAppealReasonValue = myAppealReason.getValue();
    // Capture Appeal Date
    var myAppealDateValue = Xrm.Page.getAttribute("rsl_appealdate").getValue();
    // Capture Appeal Close Date
    var myAppealClosedDateValue = Xrm.Page.getAttribute("rsl_appealcloseddate").getValue();
    // If the Penalty Status is not associated with Appeal AND the Appeal fields are all empty Hide Tab
    //                         Under Apeal   Closed Appeal Upheld
    // Appeal reason Removed
    //if (myPenaltyStatusValue == 866810011 || myPenaltyStatusValue == 866810015 || myAppealArbitratorValue != null || myAppealStatusValue != null || myAppealReasonValue != null || myAppealDateValue != null || myAppealClosedDateValue != null) {
    if (myPenaltyStatusValue == 866810011 || myPenaltyStatusValue == 866810015 || myAppealArbitratorValue != null || myAppealStatusValue != null || myAppealDateValue != null || myAppealClosedDateValue != null) {
        Xrm.Page.getControl("rsl_appealarbitrator").setVisible(true);
        Xrm.Page.getControl("rsl_appealstatus").setVisible(true);
        //Xrm.Page.getControl("rsl_appealreason").setVisible(true);
        Xrm.Page.getControl("rsl_appealdate").setVisible(true);
        Xrm.Page.getControl("rsl_appealcloseddate").setVisible(true);
    }
    else {
        Xrm.Page.getControl("rsl_appealarbitrator").setVisible(false);
        Xrm.Page.getControl("rsl_appealstatus").setVisible(false);
        //Xrm.Page.getControl("rsl_appealreason").setVisible(false);
        Xrm.Page.getControl("rsl_appealdate").setVisible(false);
        Xrm.Page.getControl("rsl_appealcloseddate").setVisible(false);
    }
}

//Hide/Show Penalty Status if Penalty Status = "Closed - Prosecuting" 866810010
function Penalty_DisablePenaltyStatusOnProsecute() {
    // Capture Penalty Status
    var myPenaltyStatus = Xrm.Page.data.entity.attributes.get("rsl_penaltystatus");
    var myPenaltyStatusValue = myPenaltyStatus.getValue();
    if (myPenaltyStatusValue == 866810010) {
        Xrm.Page.getAttribute("rsl_penaltystatus").setSubmitMode("always");
        Xrm.Page.getControl("rsl_penaltystatus").setDisabled(true);
    }
}

//Set Date Closed based on OnChange of Penalty Status
function Penalty_SetDateClosed() {
    // Capture Penalty Status
    var myPenaltyStatus = Xrm.Page.data.entity.attributes.get("rsl_penaltystatus");
    var myPenaltyStatusValue = myPenaltyStatus.getValue();
    var myPenaltyClosedDate = Xrm.Page.data.entity.attributes.get("rsl_dateclosed");
    var myPenaltyClosedDateValue = myPenaltyClosedDate.getValue();
    var myAppealClosedDate = Xrm.Page.data.entity.attributes.get("rsl_appealcloseddate");
    var myAppealClosedDateValue = myAppealClosedDate.getValue();
    Xrm.Page.getAttribute("rsl_dateclosed").setSubmitMode("always");
    // If the Penalty Status is a Closed State AND DateClosed is not set
    if ((myPenaltyClosedDateValue == null) && (myPenaltyStatusValue == 866810005 || myPenaltyStatusValue == 866810006 || myPenaltyStatusValue == 866810007 || myPenaltyStatusValue == 866810008 || myPenaltyStatusValue == 866810009 || myPenaltyStatusValue == 866810015 || myPenaltyStatusValue == 866810012 || myPenaltyStatusValue == 866810016 || myPenaltyStatusValue == 866810018 || myPenaltyStatusValue == 866810010 || myPenaltyStatusValue == 866810021 || myPenaltyStatusValue == 866810022)) {
        // If we are Closed Appeal Upheld and we have an Appeal Closed Date set the Penalty closed date to that.
        if (myAppealClosedDateValue != null && myPenaltyStatusValue == 866810015) {
            Xrm.Page.data.entity.attributes.get("rsl_dateclosed").setValue(myAppealClosedDateValue);
        }
        else {
            Xrm.Page.data.entity.attributes.get("rsl_dateclosed").setValue(new Date());
        }
    }
    // We have alert closed Date and the status isFinite an open on
    else if (myPenaltyClosedDateValue != null && myPenaltyStatusValue != 866810005 && myPenaltyStatusValue != 866810006 && myPenaltyStatusValue != 866810007 && myPenaltyStatusValue != 866810008 && myPenaltyStatusValue != 866810009 && myPenaltyStatusValue != 866810015 && myPenaltyStatusValue != 866810012 && myPenaltyStatusValue != 866810016 && myPenaltyStatusValue != 866810018 && myPenaltyStatusValue != 866810010 && myPenaltyStatusValue != 866810021 && myPenaltyStatusValue != 866810022) {
        Xrm.Page.data.entity.attributes.get("rsl_dateclosed").setValue(null);
    }
}

//Save the Last Penalty Status before it was changed.  Used when coming out of appeal status to set Status back as it was.
//function Penalty_SaveLastPenaltyStatus() {
//    var myPenaltyStatusFormValue = Xrm.Page.getAttribute("rsl_penaltystatus").getValue(); // Capture Penalty Status On form
//    // If we are Setting to Under Appeal ensure we save the last value
//    if (myPenaltyStatusFormValue == 866810011) {
//        // SAH 14-Aug-13
//        // Set the last value Penalty Status but only if the Penalty Status is not already "Under Appeal" or set to a Closed Status.  We were encountering an error where if the
//        // appeal was edited the Last Penalty Status value was being to set to "Under Appeal" which is not what we want here.  We want
//        // to preserve the Penalty Status before the appeal so it can be set back if the appeal is denied.  Additionally we do not want to set the Penalty Status back to
//        // a closed status. Added an extra condition before we set.
//        if (crmForm.all.rsl_penaltystatus.DataValue != 866810011 && crmForm.all.rsl_penaltystatus.DataValue != 866810005 && crmForm.all.rsl_penaltystatus.DataValue != 866810006
//         && crmForm.all.rsl_penaltystatus.DataValue != 866810007 && crmForm.all.rsl_penaltystatus.DataValue != 866810019 && crmForm.all.rsl_penaltystatus.DataValue != 866810021
//         && crmForm.all.rsl_penaltystatus.DataValue != 866810008 && crmForm.all.rsl_penaltystatus.DataValue != 866810018 && crmForm.all.rsl_penaltystatus.DataValue != 866810009
//         && crmForm.all.rsl_penaltystatus.DataValue != 866810015 && crmForm.all.rsl_penaltystatus.DataValue != 866810012 && crmForm.all.rsl_penaltystatus.DataValue != 866810016
//         && crmForm.all.rsl_penaltystatus.DataValue != 866810010 && crmForm.all.rsl_penaltystatus.DataValue != 866810022) {
//            Xrm.Page.data.entity.attributes.get("rsl_penaltystatuslastvalue").setValue(crmForm.all.rsl_penaltystatus.DataValue);
//        }
//    }
//}

//Hide/Show Deferred Payment Plan.  Called based on OnChange of rsl-deferredpaymentplan
function Penalty_HideShowDeferredPaymentPlan() {
    // Capture Deferred Payment Plan Yes/No
    var myPenaltyStatusFormValue = Xrm.Page.getAttribute("rsl_penaltystatus").getValue(); // Capture Penalty Status On form
    // hide the hole section if not Deferred Payment of Payment Schedule.
    if (myPenaltyStatusFormValue != 866810014 && myPenaltyStatusFormValue != 866810013) {
        setVisibleTabSection("Tab_PaymentPlan", null, false)
    }
    else {
        // If a Payment Plan Status
        if (myPenaltyStatusFormValue == 866810014 || myPenaltyStatusFormValue == 866810013) {
            Xrm.Page.getControl("rsl_datepaymentalert").setVisible(true);
        }
        else {
            Xrm.Page.getControl("rsl_datepaymentalert").setVisible(false);
        }
        // If a Deferred Payment Plan Status
        if (myPenaltyStatusFormValue == 866810013) {
            Xrm.Page.getControl("rsl_agreeddeferredpaymentplan").setVisible(true);
        }
        else {
            Xrm.Page.getControl("rsl_agreeddeferredpaymentplan").setVisible(false);
        }
        setVisibleTabSection("Tab_PaymentPlan", null, true)
    }
}


function setVisibleTabSection(tabname, sectionname, show) {
    var tab = Xrm.Page.ui.tabs.get(tabname);
    if (tab != null) {
        if (sectionname == null)
            tab.setVisible(show);
        else {
            var section = tab.sections.get(sectionname);
            if (section != null) {
                section.setVisible(show);
                if (show)
                    tab.setVisible(show);
            }
        }
    }
}

// Function to set the focus to the top tab.  Was necessary to put this in as many of the fields are read-only and the form 
// auto scrolls to the first editable field which was payment details
function penalty_setFocusToStatusTab() {
    Xrm.Page.ui.tabs.get("Status").setFocus(true);
}


/*
// Because Sub Grids load async we need to build in a timeout to allow the SubGrid to build before we Cal the Total Paid
function Penalty_subGridOnload() {
    //    var grid = Xrm.Page.ui.control.get("PenaltyPayments");
    var grid = document.getElementById("PenaltyPayments");

    //    if (grid.readyState != "complete") {
    if (grid == null || ((grid != null) && grid.readyState != "complete")) {
        // delay one second and try again.  
        setTimeout('Penalty_subGridOnload();', 1000);
        return;
    }
    //grid.attachEvent("onrefresh", penalty_CalcTotalPaid);
    //grid.control.add_onRefresh(penalty_CalcTotalPaid);
    //grid.control.add_onRefresh(function(){setTimeout(function(){ penalty_CalcTotalPaid();}, 1000);});
    // The 'penalty_CalcTotalPaid' function was superfluous so just moved the call to 'penalty_CalcRemainingBalance()' to 'calculateTotalPaid()' and called that instead - GSM 201114
    grid.control.add_onRefresh(calculateTotalPaid);
}
*/

function calculateTotalPaid() {

    var id = Xrm.Page.data.entity.getId();

	if ( id === "" ) {
		// a new record, so nothing to calculate so just return
		return;
	}

    $.ajax({
        type: "GET",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        url: serverXRMServices + "rsl_penaltypaymentSet?$select=rsl_PenaltyPaymentAmount,rsl_penaltypaymentamountadmin,rsl_penaltypaymentmode,rsl_penaltypaymentId&$filter=rsl_PenaltyTicketNumberId/Id%20eq%20(guid'" + id + "')",
        beforeSend: function(XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function(data, textStatus, XmlHttpRequest) {
            // Bear in mind that a maximum of 50 payment records can be retrieved - if there are more than 50 payments for a PFN then you are stuffed
            // At the time of writing it was decided that it was unlikely that more than 50 payments would be paid towards a single PFN so it was
            // decided not to put any effort into retrieving more than the 50 limit.
            var paidAmount = 0;
            for (i = 0; i < data.d.results.length; i++) {
                var val = parseFloat(data.d.results[i].rsl_PenaltyPaymentAmount);
                var adminval = parseFloat(data.d.results[i].rsl_penaltypaymentamountadmin);
                var mode = data.d.results[i].rsl_penaltypaymentmode.Value;
                if (mode != 866810000 && mode != null && mode != 866810003) {
                    // if this is not a payment (ie it's a refund or bounced cheque) then reverse the payment amount for this record
                    // added check for 'Correction' (866810003) - 3Jun15 GSM
                    val = -val;
                    adminval = -adminval;
                }
                if (!isNaN(val)) {
                    paidAmount += val;
                }
                if (!isNaN(adminval)) {
                    paidAmount += adminval;
                }
            }

            Xrm.Page.getAttribute("rsl_totalpaid").setValue(parseFloat(paidAmount.toFixed(2)));
            //Xrm.Page.getAttribute("rsl_totalpaid").setSubmitMode("never"); 	// Don't let the form to submit any changes, they will be handled automitcally with the penaltyFeeChange() event handler

            var updateObject = new Object();
            updateObject.rsl_TotalPaid = paidAmount.toFixed(2);
            //alert("About to update total with: " + updateObject.rsl_TotalPaid);
            updatePenaltyRecord(updateObject, "rsl_TotalPaid");

            // Move here from the 'penalty_CalcTotalPaid' function
            penalty_CalcRemainingBalance();
        },
        error: function(XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText) {
                alert("Error while retrieving payment details; Error: " + XmlHttpRequest.responseText);
            }
        }
    });
}

function addPenaltyPaymentsGridHandler () {
	Xrm.Page.getControl("PenaltyPayments").addOnLoad(calculateTotalPaid);
}

//  Update the 'Total Paid' field by getting the value from the database
penalty_CalcTotalPaid = function() {

    calculateTotalPaid();
    // Get the value of Total Paid
    var id = Xrm.Page.data.entity.getId();

    $.ajax({
        type: "GET",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        url: serverXRMServices + "rsl_penaltySet?$select=rsl_TotalPaid&$filter=rsl_penaltyId%20eq%20(guid'" + id + "')",
        beforeSend: function(XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function(data, textStatus, XmlHttpRequest) {
            //Xrm.Page.getAttribute("rsl_totalpaid").setValue(parseFloat(data.d.results[0].rsl_TotalPaid));
            penalty_CalcRemainingBalance();
        },
        error: function(XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText) {
                alert("Error while retrieving rsl_TotalPaid; Error: " + XmlHttpRequest.responseText.error.code.message);
            }
        }
    });

    // This (the 'setFocus()' call below) was put here as an attempt to work-around an issue discovered when a payment is edited.
    // (A new payment is fine - everything works as expected.)
    // Upon return from the edit, it is not possible to exit the form without the 'stay' or 'leave'
    // message popping up, despite there being no reason for it to happen. During testing it was discovered
    // that by simply clicking away from the grid (so that it no longer has focus) the form could be
    // exited as expected. This was an attempt to programatically move the focus to another field.
    // Unfortunately, focussing on the field also selected the text which made it prone to erroneous changing,
    // so it was decided to leave things as they were and just see if it causes any problems.
    //Xrm.Page.getControl("rsl_totaladminfee").setFocus(true);
}

function updatePenaltyRecord(updateObject, schemaName) {
    // updatePenaltyRecord: see http://rajeevpentyala.wordpress.com/2012/02/05/update-record-using-odata-and-jquery-in-crm-2011/
    // If this is a new record then there will not be a record in the database to update
    var formType = Xrm.Page.ui.getFormType();
    if (formType === 1) {
        return;
    }
    var id = Xrm.Page.data.entity.getId();
    var jsonEntity = window.JSON.stringify(updateObject);

    $.ajax({
        type: "POST",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: serverXRMServices + "rsl_penaltySet(guid'" + id + "')",
        beforeSend: function(XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            // Specify the HTTP method MERGE to update just the changes you are submitting
            XMLHttpRequest.setRequestHeader("X-HTTP-METHOD", "MERGE");
        },
        success: function(data, textStatus, XmlHttpRequest) {
            // The update was successful, so now update the 'Notes' entity with the details of what was done
        },
        error: function(XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && textStatus) {
                alert("Error while updating " + schemaName + "; Error: " + errorThrown);
            }
        }
    });
}

// Calculate remaining balance.  used if any of the money amounts are updated.
penalty_CalcRemainingBalance = function() {
    //alert("Recalculating the remaining balance");
    // Get the value of Penalty Fare Fine, Total Paid and Admin Fee
    var mypenaltyfarefinevalue = Xrm.Page.getAttribute("rsl_penaltyfarefine").getValue();
    var mytotaladminfeevalue = Xrm.Page.getAttribute("rsl_totaladminfee").getValue();
    var mytotalpaidvalue = Xrm.Page.getAttribute("rsl_totalpaid").getValue();
    var mypenaltystatus = Xrm.Page.getAttribute("rsl_penaltystatus").getValue();

    // Ensure if the values are null set them to zero
    if (mypenaltyfarefinevalue == null) { mypenaltyfarefinevalue = 0 }
    if (mytotalpaidvalue == null) { mytotalpaidvalue = 0 }
    if (mytotaladminfeevalue == null) { mytotaladminfeevalue = 0 }

    // Calc Balance Remaining
    var balanceRemaining = mypenaltyfarefinevalue + mytotaladminfeevalue - mytotalpaidvalue;
    //Xrm.Page.getAttribute("rsl_balanceremaining").setSubmitMode("always");
    //Xrm.Page.getAttribute("rsl_balanceremaining").setSubmitMode("never");
    Xrm.Page.getAttribute("rsl_balanceremaining").setValue(balanceRemaining);

    var updateObject = new Object();
    updateObject.rsl_BalanceRemaining = balanceRemaining.toFixed(2);
    updatePenaltyRecord(updateObject, "rsl_BalanceRemaining");

    if (balanceRemaining < 0) {
        // Alert the user to the fact the remaining balance is negative
        alert("Remaining balance is negative!  Are you sure your payments are correct?");
    }
    else if (balanceRemaining == 0) {
        // Alert the user to the fact the remaining balance is zero and request to set the PFN to Closed Fully Paid/Part Paid/Closed Prosecuting./Dropped/Debt Collection/Appeal Upheld/Paid at Issue
        if (mypenaltystatus != 866810006 && mypenaltystatus != 866810007 && mypenaltystatus != 866810010 && mypenaltystatus != 866810012 && mypenaltystatus != 866810016 && mypenaltystatus != 866810015 && mypenaltystatus != 866810019) {
            if (confirm("Remaining balance is zero.  Do you wish to close the Penalty Fare as 'Closed - Paid in Full'?")) {
                Xrm.Page.getAttribute("rsl_penaltystatus").setSubmitMode("always");
                Xrm.Page.data.entity.attributes.get("rsl_penaltystatus").setValue(866810006);
                Xrm.Page.getAttribute("rsl_dateclosed").setSubmitMode("always");
                Xrm.Page.data.entity.attributes.get("rsl_dateclosed").setValue(new Date);
                Xrm.Page.getAttribute("rsl_datestatusupdate").setSubmitMode("always");
                Xrm.Page.data.entity.attributes.get("rsl_datestatusupdate").setValue(null);
                Xrm.Page.data.entity.save();
            }
        }
    }
    // Balance remaining >0
    else {
        // Alert the user to the fact the remaining balance is >0 and PFN set to Closed Fully Paid.  Ask to Reopen PFN
        if (mypenaltystatus == 866810006) {
            if (confirm("The remaining balance to be paid is greater than zero however the Notice is set to 'Closed - Paid in Full'.  Do you wish to reopen the Penalty Fare?")) {
                Xrm.Page.getAttribute("rsl_penaltystatus").setSubmitMode("always");
                Xrm.Page.data.entity.attributes.get("rsl_penaltystatus").setValue(866810000);
                Xrm.Page.getAttribute("rsl_datestatusupdate").setSubmitMode("always");
                Xrm.Page.data.entity.attributes.get("rsl_datestatusupdate").setValue(null);
                Xrm.Page.getAttribute("rsl_dateclosed").setSubmitMode("always");
                Xrm.Page.data.entity.attributes.get("rsl_dateclosed").setValue(null);
                Xrm.Page.data.entity.save();
            }
        }

    }
}

function penaltyFeeChanged(executionContext, schemaName) {
    // Generalised function to update the record in the database. The schema name is passed in just to save all the hassle of going away to find out what it should for the given field name :-(
    // Create a new object to be used as a data carrier and add the required data to it
    var updateObject = new Object();
    try {
        updateObject[schemaName] = executionContext.getEventSource().getValue().toFixed(2);
    } catch (e) {
        //alert("Empty field (" + schemaName + ") being set to 0.00");
        executionContext.getEventSource().setValue(0);
        updateObject[schemaName] = executionContext.getEventSource().getValue().toFixed(2);
    }

    // Call the update function (pass the schema name too just for convenience, just in case it is needed in error handling)
    updatePenaltyRecord(updateObject, schemaName);

    // Recalculate the remaining balance
    penalty_CalcRemainingBalance();

    // Let the form know that it does not need to save this field as it has already been done
    //executionContext.getEventSource().setSubmitMode("never");
}

// Smartly set 'Penalty Status' based on the 'Appeal Status'
// Appeal Closed Denied  -  Penalty Status = Last Saved Status
// Appeal Closed Upheld  -  Penalty Status = Closed - Appeal Upheld
penalty_SetPenaltyStatusBasedOnAppealStatus = function() {
    // Capture Penalty Status On Form
    //var myPenaltyStatusFormValue = Xrm.Page.getAttribute("rsl_penaltystatus").getValue(); // Capture Penalty Status On form
    //var mypenaltystatusopenclosedFormValue = Xrm.Page.getAttribute("rsl_penaltystatusopenclosed").getValue(); // Capture Penalty Status Open/Closed
    //    var myPenaltyStatusLastFormValue = Xrm.Page.getAttribute("rsl_penaltystatuslastvalue").getValue(); // Capture Last Penalty Status On form
    // Capture Appeal Status On Form
    //var myAppealStatusForm = Xrm.Page.getAttribute("rsl_appealstatus");
    //var myAppealStatusFormValue = myAppealStatusForm.getValue();
    //Xrm.Page.getAttribute("rsl_penaltystatus").setSubmitMode("always");
    // SAH 5-Nov-13 - Penalty Status will never be in Under Appeal to the following no longer necessary.
    //    // Penalty Status='Under Appeal' 
    //    if (myPenaltyStatusFormValue == 866810011) {
    //        // Appeal Status='Closed Appeal Denied'
    //        if (myAppealStatusFormValue == 866810006) {
    //            Xrm.Page.getAttribute("rsl_penaltystatus").setValue(myPenaltyStatusLastFormValue);
    //            //            Xrm.Page.data.entity.attributes.get("rsl_penaltystatus").setValue(myPenaltyStatusLastFormValue);
    //            // SAH 25-Oct-13
    //            // If Appeal raised as 1streminderreqprint than set back Raised.
    //            if (myPenaltyStatusLastFormValue == 866810001) {
    //                Xrm.Page.data.entity.attributes.get("rsl_penaltystatus").setValue(866810000);
    //            }
    //            // SAH 25-Oct-13
    //            // If Appeal raised as 2ndreminderreqprint than set back Wait...1stRemPrinted.
    //            else if (myPenaltyStatusLastFormValue == 866810003) {
    //                Xrm.Page.data.entity.attributes.get("rsl_penaltystatus").setValue(866810002);
    //            }
    //            else {
    //                Xrm.Page.data.entity.attributes.get("rsl_penaltystatus").setValue(myPenaltyStatusLastFormValue);
    //            }
    //        }
    // Appeal Status='Closed Appeal Upheld' and the PFN has not been Closed already
    //if (myAppealStatusFormValue == 866810007 && mypenaltystatusopenclosedFormValue != "Closed") {
    //    Xrm.Page.data.entity.attributes.get("rsl_penaltystatus").setValue(866810015);
    //}
    //    }
}

// *************** OnChange Ticket Issued At ***************
// Set 'To Station' to 'Ticket Issued At' if 'Ticket Issued At' changed and 'To Station' Null
//penalty_Onchange_TicketIssuedAt = function() {
//    // Capture Ticket Issued At
//    var myTicketIssuedAtForm = Xrm.Page.getAttribute("rsl_ticketissuedat");
//    var myTicketIssuedAtFormValue = myTicketIssuedAtForm.getValue();
//    // Capture To Station
//    var myToStationForm = Xrm.Page.getAttribute("rsl_tostation");
//    var myToStationFormValue = myToStationForm.getValue();
//    // If the 'Ticket Issued At' has a value AND the 'To Station' is null preset the To Station with Ticket Issued at.
//    if (myTicketIssuedAtFormValue != null && myToStationFormValue == null) {
//        Xrm.Page.getAttribute("rsl_tostation").setValue(myTicketIssuedAtFormValue);
//    }
//    if (myTicketIssuedAtFormValue == null && myToStationFormValue != null) {
//        Xrm.Page.getAttribute("rsl_ticketissuedat").setValue(myToStationFormValue);
//    }
//}

// *************** OnChange Appeal Status ***************
// Smartly set dates of Appeal.
penalty_Onchange_AppealStatus = function() {
    // Get my current values
    var myPenaltyStatusFormValue = Xrm.Page.getAttribute("rsl_penaltystatus").getValue(); // Capture Penalty Status On Form
    //    var myPenaltyStatusLastFormValue = Xrm.Page.getAttribute("rsl_penaltystatuslastvalue").getValue(); // Capture Penalty Status On form
    var myAppealArbitratorFormValue = Xrm.Page.getAttribute("rsl_appealarbitrator").getValue(); // Capture Arbitrator On Form
    // Appeal Reason removed for Go-Ahead
    //    var myAppealReasonFormValue = Xrm.Page.getAttribute("rsl_appealreason").getValue(); // Capture Reason On Form
    var myAppealStatusFormValue = Xrm.Page.getAttribute("rsl_appealstatus").getValue(); // Capture Appeal Status On Form
    var myAppealDateFormValue = Xrm.Page.getAttribute("rsl_appealdate").getValue(); // Capture Appeal Date
    var myAppealClosedDateFormValue = Xrm.Page.getAttribute("rsl_appealcloseddate").getValue(); // Capture Appeal Close Date
    // Appeal Status Open or Decision AND the Open Date null set to todays date
    if (myAppealDateFormValue == null && (myAppealStatusFormValue == 866810000 || myAppealStatusFormValue == 866810001)) {
        Xrm.Page.getAttribute("rsl_appealdate").setValue(new Date());
    }
    // Appeal Status Closed AND the Close Date null set to todays date
    if (myAppealClosedDateFormValue == null && (myAppealStatusFormValue == 866810006 || myAppealStatusFormValue == 866810007)) {
        Xrm.Page.getAttribute("rsl_appealcloseddate").setValue(new Date());
    }
    // SAH 16-Aug-13 - Issue reported by Mandy where the Appeal is already Closed as one state (e.g. Denied) and needs to be changed to the other
    // Closed state (e.g. Upheld).  This previously was prevented by saying the Appeal Status did not match the Pentalty Status
    //
    // Ensure we have a full compliment of Appeal fields.  e.g. It is complete and closed.
    Xrm.Page.getAttribute("rsl_penaltystatus").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_dateclosed").setSubmitMode("always");
    if (myAppealArbitratorFormValue != null && myAppealStatusFormValue != null && myAppealDateFormValue != null && myAppealClosedDateFormValue != null) {
        // SAH 9-May-14 - We do not set into "Under Appeal" so no need to set to Last Saved Penalty Status.
        // If the Penalty Status = 'Closed - Appeal Upheld' and the Appeal Status = 'Appeal Denied' set the Penalty Status to LastPenaltyStatus
        //                              Appeal Upheld                           Appeal Denied
        //        if (myPenaltyStatusFormValue == 866810015 && myAppealStatusFormValue == 866810006) {
        //            Xrm.Page.getAttribute("rsl_penaltystatus").setValue(myPenaltyStatusLastFormValue);
        //        }
        // SAH 9-May-14 - Do not set the Penalty Status to 'Closed - Appeal Upheld' as they may still want to chase the original fare.
        // Operator has to therefore choose to manually closed the PFN as we can make not assumption here.
        //                                  Appeal Upheld
        //        else if (myAppealStatusFormValue == 866810007) {
        //            Xrm.Page.getAttribute("rsl_penaltystatus").setValue(866810015);
        //        }
        // The Appeal is set to "Open" and we have all other Appeal fields (with close as optional)
        if (myAppealArbitratorFormValue != null && myAppealStatusFormValue == 866810000 && myAppealDateFormValue != null) {
            //            Xrm.Page.getAttribute("rsl_penaltystatus").setValue(866810011);
            Xrm.Page.getAttribute("rsl_appealcloseddate").setValue(null);
        }
    }
}

// Functional called when the Offender is Changed and we are in NEW record mode.
// Sets the "At time Of Offence (ATOO) details.
function setattimeofoffencedetails() {
    // Are we in NEW record mode ?
    var formType = Xrm.Page.ui.getFormType();
    var person = Xrm.Page.getAttribute("rsl_person").getValue();
    // Form is in CREATE
    if (formType == 1) {
        //
        // If we have a Person set e.g. It has not been cleared
        //
        if (person != null) {
            // Get person details...
            var guid = Xrm.Page.getAttribute("rsl_person").getValue()[0].id;
            //var odataSetName = var_person[0].entityType;
            var odataSetName = "LeadSet"; // var_person[0].entityType returns 'lead' rather than 'Lead'

            // Get Server URL
            var serverUrl = Xrm.Page.context.getClientUrl();
            //The OData end-point
            var ODATA_ENDPOINT = "/XRMServices/2011/OrganizationData.svc";
            var select = "?$select=rsl_Title,FirstName,MiddleName,LastName,rsl_fulladdress,rsl_Gender,rsl_Occupation,Address1_Telephone1,rsl_DOB,rsl_Age,rsl_GuardiansName,rsl_PassengerType,Address1_Line1,Address1_Line2,Address1_Line3,Address1_City,Address1_County,Address1_PostalCode,Address1_Country,rsl_App_EthnicAppearance,rsl_App_Build,rsl_App_EyeColour,rsl_App_HairColour,rsl_App_HairType,rsl_App_FacialHair,rsl_Height,rsl_OtherAppearanceFeatures, rsl_tips, rsl_electoralroll, rsl_drivinglic, rsl_policecheck, rsl_studentid, rsl_utilitybill,rsl_PersonPhoto1,rsl_PersonPhoto2,rsl_PersonPhoto3, rsl_passport, rsl_postoffice,rsl_HeShe,rsl_hesheuppercasefirstletter,rsl_hisher,rsl_hisheruppercasefirstletter";
            //Asynchronous AJAX function to Retrieve a CRM record using OData
            $.ajax({
                type: "get",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                url: serverUrl + ODATA_ENDPOINT + "/" + odataSetName + "(guid'" + guid + "')" + select,
                beforeSend: function(XMLHttpRequest) {
                    //Specifying this header ensures that the results will be returned as JSON.
                    XMLHttpRequest.setRequestHeader("Accept", "application/json");
                },
                success: function(data, textStatus, XmlHttpRequest) {
                    Xrm.Page.getAttribute("rsl_atoo_title").setValue(data.d.rsl_Title.Value);
                    Xrm.Page.getAttribute("rsl_atoo_firstname").setValue(data.d.FirstName);
                    Xrm.Page.getAttribute("rsl_atoo_middlename").setValue(data.d.MiddleName);
                    Xrm.Page.getAttribute("rsl_atoo_lastname").setValue(data.d.LastName);
                    Xrm.Page.getAttribute("rsl_addressonoffence").setValue(data.d.rsl_fulladdress);
                    Xrm.Page.getAttribute("rsl_atoo_gender").setValue(data.d.rsl_Gender.Value);
                    Xrm.Page.getAttribute("rsl_atoo_occupation").setValue(data.d.rsl_Occupation);
                    Xrm.Page.getAttribute("rsl_atoo_address1_telephone1").setValue(data.d.Address1_Telephone1);
                    Xrm.Page.getAttribute("rsl_atoo_dob").setValue(new Date(parseInt(data.d.rsl_DOB.replace("/Date(", "").replace(")/", ""), 10)));
                    Xrm.Page.getAttribute("rsl_atoo_age").setValue(data.d.rsl_Age);
                    Xrm.Page.getAttribute("rsl_atoo_passengertype").setValue(data.d.rsl_PassengerType);
                    Xrm.Page.getAttribute("rsl_atoo_guardiansname").setValue(data.d.rsl_GuardiansName);

                    Xrm.Page.getAttribute("rsl_atoo_address1_line1").setValue(data.d.Address1_Line1);
                    Xrm.Page.getAttribute("rsl_atoo_address1_line2").setValue(data.d.Address1_Line2);
                    Xrm.Page.getAttribute("rsl_atoo_address1_line3").setValue(data.d.Address1_Line3);
                    Xrm.Page.getAttribute("rsl_atoo_address1_city").setValue(data.d.Address1_City);
                    Xrm.Page.getAttribute("rsl_atoo_address1_county").setValue(data.d.Address1_County);
                    Xrm.Page.getAttribute("rsl_atoo_address1_postcode").setValue(data.d.Address1_PostalCode);
                    Xrm.Page.getAttribute("rsl_atoo_address1_country").setValue(data.d.Address1_Country);

                    Xrm.Page.getAttribute("rsl_atoo_app_ethnicappearance").setValue(data.d.rsl_App_EthnicAppearance.Value);
                    Xrm.Page.getAttribute("rsl_atoo_app_build").setValue(data.d.rsl_App_Build.Value);
                    Xrm.Page.getAttribute("rsl_atoo_app_eyecolour").setValue(data.d.rsl_App_EyeColour.Value);
                    Xrm.Page.getAttribute("rsl_atoo_app_haircolour").setValue(data.d.rsl_App_HairColour.Value);
                    Xrm.Page.getAttribute("rsl_atoo_app_hairtype").setValue(data.d.rsl_App_HairType.Value);
                    Xrm.Page.getAttribute("rsl_atoo_app_facialhair").setValue(data.d.rsl_App_FacialHair.Value);
                    Xrm.Page.getAttribute("rsl_atoo_height").setValue(data.d.rsl_Height);
                    Xrm.Page.getAttribute("rsl_atoo_otherappearancefeatures").setValue(data.d.rsl_OtherAppearanceFeatures);

                    Xrm.Page.getAttribute("rsl_atoo_tips").setValue(data.d.rsl_tips.Value);
                    Xrm.Page.getAttribute("rsl_atoo_electoralroll").setValue(data.d.rsl_electoralroll.Value);
                    Xrm.Page.getAttribute("rsl_atoo_drivinglic").setValue(data.d.rsl_drivinglic.Value);
                    Xrm.Page.getAttribute("rsl_atoo_passport").setValue(data.d.rsl_passport.Value);
                    Xrm.Page.getAttribute("rsl_atoo_postoffice").setValue(data.d.rsl_postoffice.Value);
                    Xrm.Page.getAttribute("rsl_atoo_policecheck").setValue(data.d.rsl_policecheck.Value);
                    Xrm.Page.getAttribute("rsl_atoo_studentid").setValue(data.d.rsl_studentid.Value);
                    Xrm.Page.getAttribute("rsl_atoo_utilitybill").setValue(data.d.rsl_utilitybill.Value);

                    Xrm.Page.getAttribute("rsl_atoo_passengerphoto1").setValue(data.d.rsl_PersonPhoto1);
                    Xrm.Page.getAttribute("rsl_atoo_passengerphoto2").setValue(data.d.rsl_PersonPhoto2);
                    Xrm.Page.getAttribute("rsl_atoo_passengerphoto3").setValue(data.d.rsl_PersonPhoto3);

                    Xrm.Page.getAttribute("rsl_atoo_heshe").setValue(data.d.rsl_HeShe);
                    Xrm.Page.getAttribute("rsl_atoo_hesheuppercasefirstletter").setValue(data.d.rsl_hesheuppercasefirstletter);
                    Xrm.Page.getAttribute("rsl_atoo_hisher").setValue(data.d.rsl_hisher);
                    Xrm.Page.getAttribute("rsl_atoo_hisheruppercasefirstletter").setValue(data.d.rsl_hisheruppercasefirstletter);


                },
                error: function(XmlHttpRequest, textStatus, errorThrown) {
                    alert("Error " + errorThrown)
                }
            });
        }
        else {
            Xrm.Page.getAttribute("rsl_atoo_title").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_firstname").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_middlename").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_lastname").setValue(null);
            Xrm.Page.getAttribute("rsl_addressonoffence").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_gender").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_occupation").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_address1_telephone1").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_dob").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_age").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_passengertype").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_guardiansname").setValue(null);

            Xrm.Page.getAttribute("rsl_atoo_address1_line1").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_address1_line2").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_address1_line3").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_address1_city").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_address1_county").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_address1_postcode").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_address1_country").setValue(null);

            Xrm.Page.getAttribute("rsl_atoo_app_ethnicappearance").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_app_build").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_app_eyecolour").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_app_haircolour").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_app_hairtype").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_app_facialhair").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_height").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_otherappearancefeatures").setValue(null);

            Xrm.Page.getAttribute("rsl_atoo_tips").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_electoralroll").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_drivinglic").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_passport").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_postoffice").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_policecheck").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_studentid").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_utilitybill").setValue(null);

            Xrm.Page.getAttribute("rsl_atoo_passengerphoto1").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_passengerphoto2").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_passengerphoto3").setValue(null);

            Xrm.Page.getAttribute("rsl_atoo_heshe").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_hesheuppercasefirstletter").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_hisher").setValue(null);
            Xrm.Page.getAttribute("rsl_atoo_hisheruppercasefirstletter").setValue(null);
        }
    }
}

// Functional called when the Offender is Changed.
// Sets the Correspondence Name & Address details.
function setcorrespondencedetails() {
    var person = Xrm.Page.getAttribute("rsl_person").getValue();
    //
    // If we have a Person set e.g. It has not been cleared
    //
    if (person != null) {
        // Get person details...
        var guid = Xrm.Page.getAttribute("rsl_person").getValue()[0].id;
        //var odataSetName = var_person[0].entityType;
        var odataSetName = "LeadSet"; // var_person[0].entityType returns 'lead' rather than 'Lead'

        // Get Server URL
        var serverUrl = Xrm.Page.context.getClientUrl();
        //The OData end-point
        var ODATA_ENDPOINT = "/XRMServices/2011/OrganizationData.svc";
        var select = "?$select=rsl_Title,FirstName,MiddleName,LastName,rsl_fulladdress,Address1_Line1,Address1_Line2,Address1_Line3,Address1_City,Address1_County,Address1_PostalCode,Address1_Country,rsl_postoffice";
        //Asynchronous AJAX function to Retrieve a CRM record using OData
        $.ajax({
            type: "get",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: serverUrl + ODATA_ENDPOINT + "/" + odataSetName + "(guid'" + guid + "')" + select,
            beforeSend: function(XMLHttpRequest) {
                //Specifying this header ensures that the results will be returned as JSON.
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success: function(data, textStatus, XmlHttpRequest) {
                Xrm.Page.getAttribute("rsl_corr_title").setValue(data.d.rsl_Title.Value);
                Xrm.Page.getAttribute("rsl_corr_firstname").setValue(data.d.FirstName);
                Xrm.Page.getAttribute("rsl_corr_middlename").setValue(data.d.MiddleName);
                Xrm.Page.getAttribute("rsl_corr_lastname").setValue(data.d.LastName);
                Xrm.Page.getAttribute("rsl_corr_addressonoffence").setValue(data.d.rsl_fulladdress);

                Xrm.Page.getAttribute("rsl_corr_address1_line1").setValue(data.d.Address1_Line1);
                Xrm.Page.getAttribute("rsl_corr_address1_line2").setValue(data.d.Address1_Line2);
                Xrm.Page.getAttribute("rsl_corr_address1_line3").setValue(data.d.Address1_Line3);
                Xrm.Page.getAttribute("rsl_corr_address1_city").setValue(data.d.Address1_City);
                Xrm.Page.getAttribute("rsl_corr_address1_county").setValue(data.d.Address1_County);
                Xrm.Page.getAttribute("rsl_corr_address1_postcode").setValue(data.d.Address1_PostalCode);
                Xrm.Page.getAttribute("rsl_corr_address1_country").setValue(data.d.Address1_Country);

                Xrm.Page.getAttribute("rsl_corr_postoffice").setValue(data.d.rsl_postoffice.Value);

            },
            error: function(XmlHttpRequest, textStatus, errorThrown) {
                alert("Error " + errorThrown)
            }
        });
    }
    else {
        Xrm.Page.getAttribute("rsl_corr_title").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_firstname").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_middlename").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_lastname").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_addressonoffence").setValue(null);

        Xrm.Page.getAttribute("rsl_corr_address1_line1").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_address1_line2").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_address1_line3").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_address1_city").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_address1_county").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_address1_postcode").setValue(null);
        Xrm.Page.getAttribute("rsl_corr_address1_country").setValue(null);

        Xrm.Page.getAttribute("rsl_corr_postoffice").setValue(null);
    }
}

//Set the Corr. Full Address Field
set_corr_fulladdress = function() {
    var addr1 = Xrm.Page.getAttribute("rsl_corr_address1_line1").getValue();
    var addr2 = Xrm.Page.getAttribute("rsl_corr_address1_line2").getValue();
    var addr3 = Xrm.Page.getAttribute("rsl_corr_address1_line3").getValue();
    var town = Xrm.Page.getAttribute("rsl_corr_address1_city").getValue();
    var county = Xrm.Page.getAttribute("rsl_corr_address1_county").getValue();
    var postcode = Xrm.Page.getAttribute("rsl_corr_address1_postcode").getValue();
    var country = Xrm.Page.getAttribute("rsl_corr_address1_country").getValue();
    var fulladdress = addr1 + ", " + addr2 + ", " + addr3 + ", " + town + ", " + county + ", " + postcode + ", " + country
    fulladdress = fulladdress.replace(/, null/g, "");
    fulladdress = fulladdress.replace(/null, /g, "");
    Xrm.Page.getAttribute("rsl_corr_addressonoffence").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_corr_addressonoffence").setValue(fulladdress);
}

// ******************* ON SAVE EVENTS ****************************************
// Ensure that If any one Appeal field is set (Status, Reason or Date) then they all need a value.
penalty_savevalidation = function(executionObj) {
    // ****** Capture Form Values.  Used to verify against old values *****
    var myPenaltyStatusFormValue = Xrm.Page.getAttribute("rsl_penaltystatus").getValue(); // Capture Penalty Status On form
    var myAppealArbitratorFormValue = Xrm.Page.getAttribute("rsl_appealarbitrator").getValue(); // Capture Appeal Arbitrator On Form
    var myAppealStatusFormValue = Xrm.Page.getAttribute("rsl_appealstatus").getValue(); // Capture Appeal Status On Form
    // Appeal Reason Removed
    //    var myAppealReasonFormValue = Xrm.Page.getAttribute("rsl_appealreason").getValue(); // Capture Appeal Reason On Form
    var myAppealDateFormValue = Xrm.Page.getAttribute("rsl_appealdate").getValue(); // Capture Appeal Date
    var myAppealClosedDateFormValue = Xrm.Page.getAttribute("rsl_appealcloseddate").getValue(); // Capture Appeal Close Date
    var mydatepaymentalertFormValue = Xrm.Page.getAttribute("rsl_datepaymentalert").getValue(); // Capture Payment Alert
    var myagreeddeferredpaymentplanFormValue = Xrm.Page.getAttribute("rsl_agreeddeferredpaymentplan").getValue(); // Capture Payment Alert
    //
    // ***** DO WE HAVE MINIMAL SET OF FIELDS SET *****
    //
    // TOC
    var var_toc = Xrm.Page.getAttribute("rsl_toc").getValue();
    if (var_toc == null && myPenaltyStatusFormValue != 866810018) {
        alert("Record cannot be saved.  'TOC' must be specified.");
        Xrm.Page.getControl("rsl_toc").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Penalty Status
    if (myPenaltyStatusFormValue == null) {
        alert("Record cannot be saved.  'Penalty Status' must be specified.");
        Xrm.Page.getControl("rsl_penaltystatus").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Date of Issue
    var var_dateofissue = Xrm.Page.getAttribute("rsl_dateofissue").getValue();
    if (var_dateofissue == null && myPenaltyStatusFormValue != 866810018) {
        alert("Record cannot be saved.  'Date of Issue' must be specified.");
        Xrm.Page.getControl("rsl_dateofissue").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Inspector
    var var_inspector = Xrm.Page.getAttribute("rsl_inspector").getValue();
    if (var_inspector == null && myPenaltyStatusFormValue != 866810018) {
        alert("Record cannot be saved.  'Inspector' must be specified.");
        Xrm.Page.getControl("rsl_inspector").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Person
    var var_person = Xrm.Page.getAttribute("rsl_person").getValue();
    if (var_person == null && myPenaltyStatusFormValue != 866810018) {
        alert("Record cannot be saved.  'Person' must be specified.");
        Xrm.Page.getControl("rsl_person").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Excusetype
    var var_inspector = Xrm.Page.getAttribute("rsl_excusetype").getValue();
    if (var_inspector == null && myPenaltyStatusFormValue != 866810018) {
        alert("Record cannot be saved.  'Excuse Type' must be specified.");
        Xrm.Page.getControl("rsl_excusetype").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // /From Station/Issued At Station/To Station
    var var_ticketissuedat = Xrm.Page.getAttribute("rsl_ticketissuedat").getValue();
    var var_fromstation = Xrm.Page.getAttribute("rsl_fromstation").getValue();
    var var_tostation = Xrm.Page.getAttribute("rsl_tostation").getValue();
    if ((var_ticketissuedat == null || var_fromstation == null || var_tostation == null) && myPenaltyStatusFormValue != 866810018) {
        alert("Record cannot be saved.  'From Station', 'Issued At Station' and 'To Station' must be specified.");
        if (var_fromstation == null) {
            Xrm.Page.getControl("rsl_fromstation").setFocus(true);
        }
        else if (var_ticketissuedat == null) {
            Xrm.Page.getControl("rsl_ticketissuedat").setFocus(true);
        }
        else {
            Xrm.Page.getControl("rsl_tostation").setFocus(true);
        }
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Reason Code
    var var_reason = Xrm.Page.getAttribute("rsl_reasoncode").getValue();
    if (var_reason == null && myPenaltyStatusFormValue != 866810018) {
        alert("Record cannot be saved.  'Penalty Reason' must be specified.");
        Xrm.Page.getControl("rsl_reasoncode").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }


    // ***** APPEAL VALIDATION *****
    //
    // ********************** Check if the PENALTY STATUS value is not in conflict with the Appeal ***********
    // SAH 5-Nov-13 - Commentsed out as PFN Status never 'Under Apeal' as continues in its lifecycle.
    //    // IF the Penalty Status = ******* UNDER APPEAL *****
    //    if (myPenaltyStatusFormValue == 866810011) {
    // Throw an error, either or all the values we not unset OR they were all not Set.
    if (myAppealArbitratorFormValue != null || myAppealStatusFormValue != null || myAppealDateFormValue != null) {
        if (myAppealArbitratorFormValue == null || myAppealStatusFormValue == null || myAppealDateFormValue == null) {
            //alert("Record cannot be saved.  'Appeal Arbitrator', 'Appeal Status', 'Appeal Reason' and 'Appeal Date' must either be all be specifed or all be removed.");
            alert("Record cannot be saved.  'Appeal Arbitrator', 'Appeal Status' and 'Appeal Date' must either be all be specifed or all be removed.");
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    // Appeal Status Cannot be in a 'Closed' state if Penalty Status 'Under Appeal'
    //   }
    // IF the Penalty Status = ******  CLOSED - APPEAL UPHELD  *********
    if (myPenaltyStatusFormValue == 866810015) {
        // Throw an error, either or all the values we not unset OR they were all not Set.
        if (myAppealArbitratorFormValue == null || myAppealStatusFormValue == null || myAppealDateFormValue == null || myAppealClosedDateFormValue == null) {
            //alert("Record cannot be saved.  'Appeal Arbitrator', 'Appeal Status', 'Appeal Reason' and 'Appeal Date' and 'Appeal Closed Date' must be set if Penalty Status is 'Closed - Appeal Upheld'.");
            alert("Record cannot be saved.  'Appeal Arbitrator', 'Appeal Status' and 'Appeal Date' and 'Appeal Closed Date' must be set if Penalty Status is 'Closed - Appeal Upheld'.");
            executionObj.getEventArgs().preventDefault();
            return;
        }
        // Appeal Status must be set to 'Closed Upheld' if Penalty Status  'Closed - Appeal Upheld'
        if (myAppealStatusFormValue != 866810007) {
            alert("Record cannot be saved.  'Appeal Status' must be set to 'Closed - Appeal Upheld' if the 'Penalty Status' is set to 'Closed - Appeal Upheld'.");
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    // IF the Penalty Status ****** ANY OTHER VALUE *******
    else {
        // Only check this is the the record has any signs of being in Appeal
        if (myAppealArbitratorFormValue != null && myAppealStatusFormValue != null && myAppealDateFormValue != null && myAppealClosedDateFormValue != null) {
            if (myAppealStatusFormValue != 866810006 && myPenaltyStatusFormValue == 866810015) {
                alert("Record cannot be saved.  Penalty Status cannot be 'Closed - Appeal Upheld' if Appeal Status is set to 'Closed - Appeal Denied'.");
                executionObj.getEventArgs().preventDefault();
                return;
            }
            else if ((myAppealArbitratorFormValue == null || myAppealStatusFormValue == null || myAppealDateFormValue == null || myAppealClosedDateFormValue == null) && (myPenaltyStatusFormValue != 866810011 && myPenaltyStatusFormValue != 866810015)) {
                //alert("Record cannot be saved.  'Appeal Arbitrator', 'Appeal Status', 'Appeal Reason', 'Appeal Date' & 'Appeal Closed Date' must all be set if Penalty Fare no longer in Appeal.");
                alert("Record cannot be saved.  'Appeal Arbitrator', 'Appeal Status', 'Appeal Date' & 'Appeal Closed Date' must all be set if Penalty Fare no longer in Appeal.");
                executionObj.getEventArgs().preventDefault();
                return;
            }
        }
        //        // If the Appeal fields set then the Penalty Status must be an Appeal
        //      if (myAppealStatusFormValue != null || myAppealReasonFormValue != null || myAppealDateFormValue != null || myAppealClosedDateFormValue != null) {
        //      }
        // SAH 5-Nov-13 - Commentsed out as PFN Status never 'Under Apeal' as continues in its lifecycle.
        //        // If the Appeal fields set then the Penalty Status must be an Appeal
        //      if (myAppealArbitratorFormValue != null || myAppealStatusFormValue != null || myAppealReasonFormValue != null || myAppealDateFormValue != null || myAppealClosedDateFormValue != null) {
        //            // Appeal Status not a Closed status
        //            if (myAppealStatusFormValue != 866810006 && myAppealStatusFormValue != 866810007 && myPenaltyStatusFormValue != 866810011) {
        //                alert("Record cannot be saved.  'Penalty Status' must be set to 'Under Appeal' if an Appeal is open.");
        //                executionObj.getEventArgs().preventDefault();
        //                return;
        //            }
        //        }
    }
    //
    // ********************** Check if the APPEAL fields are in conflict with each other. ***********
    // If the Appeal Status Closed we must have a Appeal Closed Date
    if ((myAppealStatusFormValue == 866810006 || myAppealStatusFormValue == 866810007) && myAppealClosedDateFormValue == null) {
        alert("Record cannot be saved.  'Appeal Closed Date' must be set if the Appeal is Closed.");
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Cannot set an Appeal Closed Date unless the Appeal Status is a Closed status
    if ((myAppealStatusFormValue != 866810006 && myAppealStatusFormValue != 866810007) && myAppealClosedDateFormValue != null) {
        alert("Record cannot be saved.  'Appeal Closed Date' cannot be set unless the Appeal Status is set to a Closed status.");
        executionObj.getEventArgs().preventDefault();
        return;
    }

    // SAH 5-Nov-13 - Deferred Payments removed from system so commenting out validation
    //    //
    //    // ********************** Validate Deferred Payment and Payment Plan Details ******************
    //    // Deferred Payment
    //    if (myPenaltyStatusFormValue == 866810014 && mydatepaymentalertFormValue == null) {
    //        alert("Record cannot be saved.  'If Penalty Status is set to a Deferred Payment Plan then the 'Payment Alert Date' must be set.");
    //        Xrm.Page.getControl("rsl_datepaymentalert").setFocus(true);
    //        executionObj.getEventArgs().preventDefault();
    //        return;
    //    }
    //    // Payment Plan
    //    if (myPenaltyStatusFormValue == 866810013 && (mydatepaymentalertFormValue == null || myagreeddeferredpaymentplanFormValue == null)) {
    //        alert("Record cannot be saved.  'If Penalty Status is set to a Payment Plan then the 'Payment Alert Date' and 'Payment Plan Details' must be set.");
    //        Xrm.Page.getControl("rsl_datepaymentalert").setFocus(true);
    //        executionObj.getEventArgs().preventDefault();
    //        return;
    //    }
    //
    // ********************** If Penalty Status has been manually set to '1st Reminder Req. Printing'866810001 OR '2nd Reminder Req. Printing'866810003 Set rsl_date1streminderrequiresprint/rsl_date2ndreminderrequiresprint   ******************
    //
    if (myPenaltyStatusFormValue == 866810001) {
        Xrm.Page.getAttribute("rsl_date1streminderrequiresprint").setValue(new Date());
    }
    else if (myPenaltyStatusFormValue == 866810003) {
        Xrm.Page.getAttribute("rsl_date2ndreminderrequiresprint").setValue(new Date());
    }
    // If all the Save validations pass set the Penalty Status
    penalty_SetPenaltyStatusBasedOnAppealStatus();
    // SAH 3-Sep-13.  Add to set the Closed date based in Penalty status
    Penalty_SetDateClosed();

}
