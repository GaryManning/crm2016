function printDocuments (totalIds, ids, entity, templateId, pdfprint) {
	debugger;
	/*
	 * Overview: this function will print required documents for the selected ids
	 * There is a limit to the number of penalties that can be submitted to the server
	 * owing to the url length limit imposed by IE. The selected ids are broken down into batches
	 * such that their combined string length will not excced the imposed limit.
	 * So, firstly a reference must be obtained from the server so that the batches of prints
	 * can all be 'linked' to each other to provide one big print batch print at the server.
	 * Then a number of calls are made to the addmailmerge function on the server referencing
	 * the obtained reference number.
	 * Once all records have been submitted a timer is started which loops round checking the
	 * status of the current batch print. Once the batch is finished a confirmation dialog is
	 * displayed to the user and the server is updated with their input.
	 *
	 * totalIds - the number of selected ids
	 * ids - an array of the ids
	 * entity - 'penalty' or 'prosecution'
	 * templateId - if supplied, the template to use
	 * pdfprint - if supplied and true, create a pdf
	 */

	//Debug.logging = true;		// uncomment if logging is needed

	function createMailMerge(bridge, type, count, template, pdfprint) {
		// Start the print process by requesting an print reference from the server
		//Log("createMailMerge request sent (" + count + " records).");
		var server;
		var pdfPrintString;
		var idString;
		if ( pdfprint ) {
			server = bridge + "createmailmerge";
			pdfPrintString = "&ispdf=true";
			idString = "&id=";
		} else {
			server = bridge + "createmailmerge";
			pdfPrintString = "";
			idString = "";
		}

		$('#updatingDiv').show();
		//var server = server + "createmailmerge"
		var url = server + "?entity=" + type + "&count=" + count + template + idString + pdfPrintString;

		return callWrapper({
			dataType: 'jsonp',
			url:  url,
			success: function (data, textStatus, XmlHttpRequest) {
				// Should get something like '{"print": "12345-abcde"}
				if ( !data.print ) {
					// something failed so alert the user
					alert("Error: No print parameter returned.");
					// Strictly speaking the XmlHttpRequest has succeeded so we need to manually force an error condition that can be picked up in the next section
					data.print = "ERROR";
					data.forcedError = true;
					return;
				}
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				if ( textStatus === "parsererror" ) {
					return;	// It's likely to be a signin error so we'll tak a chance and ignore it
				}
				if (XmlHttpRequest && errorThrown ) {
					//alert("Error thrown during the print creation phase: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
					alert("Error thrown during the print creation phase: " + errorThrown);
				} else {
					alert("Error during the print creation phase; Error (textStatus): " + textStatus);
				}
				return $.Deferred(function(d) {
						d.reject(XmlHttpRequest);
				});
			},
			complete: function (XmlHttpRequest, textStatus) {
				//$('#updatingDiv').hide();
			}
		});
	}

	function addMailMerge(server, type, printId, guids) {
		// Add each batch of records to the print queue
		//Log("addMailMerge sending request: " + printId + " with: " + guids);
		//Log("Sending addMailMerge request.");

		server = server + "addmailmerge";
		printId = encodeURI(printId);
		guids = encodeURI(guids);

		var	url = server + "?entity=" + type + "&print=" + printId + "&id=" + guids;
		return $.ajax({
			dataType: 'jsonp',
			url: url,
			success: function (data, textStatus, XmlHttpRequest) {
				// Check that there is no error from the server - so long as the request to the server
				// completes ok then this section will be executed, which makes it difficult to detemermine
				// if there have been any other sort of errors (eg wrong reference number etc) so we have to
				// check to see if the 'error' property of the returned object exists, and if it does then
				// force a error by setting a flag
				// Note: this was forced on us by ie refusing to acknowlege any ajax requests with a return
				// status of anything but '200' :-(
				//Log("Got this from the server: "+data);
				if ( data.error ) {
					alert("Error while building the print request: " + data.error);
					data.forcedError = true;
					return false;
				}
				return true;
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				if (XmlHttpRequest && errorThrown ) {
					alert("Error thrown while building the print request: " + errorThrown);
				} else {
					alert("Error while building the print request list; Error (textStatus) " + textStatus);
				}
				return false;
			}
		});
	}

function getPrintProgress(timer, server, id) {
	// Once the print has started printing we will loop round at this point, checking for the current status
	var server = server + "getprintprogress";
	var	url = server + "?print=" + id;
	var processing;
	return $.ajax({
		dataType: 'jsonp',
		url: url,
		success: function (data, textStatus, XmlHttpRequest) {
			//Log("getPrintProgress received: " + data.done);
			if ( data.done == false ) {
				// print the number of documents printed (ie 'printed 34 of 250 documents')
				// update: add 1 to the number of documents printed and display it as the number currently being processed 
				processing = data.printed + 1;
				$('#updatingText').text("Printing " + processing + " of " + data.total);
			} else {
				window.clearInterval(timer);		// looks like we're done so stop the timer
				if ( data.error ) {
					// there has been an error so alert the user
					alert("There has been an error: " + data.error + " " + data.printed + " of " + data.total + " completed");
					// Do the completed print stuff here. There is a chance that we might have to go thorugh the adding notes section as well
					// in which case it will need to be added here
					// (or maybe just not put the following section in an 'else', so that it will get executed anyway)
					sendPrintedComplete(serverBridge, id, "false", "");
					$('#updatingDiv').hide();
					return false;
				} else {
					$('#updatingDiv').hide();
					if ( pdfprint ) {
						sendPDFPrintedComplete(serverBridge, id, "ASN");
					} else {
						$('#completedDiv').show();
						$('#completedDiv').center();
						$('#completedDoneButton').click( function () {
								$('#completedDoneButton').prop('disabled', true);
								sendPrintedComplete(serverBridge, id, "true", $('#completedTextarea').text());
								});
						$('#completedCancelButton').click( function () {
								var cancelMessage;
								if ( template == "" ) {
									cancelMessage = "You have chosen to cancel the 'Lifecycle Print'. In doing so you are confirming the printed material was not produced correctly. The system will not advance any of the selected records status and in addition the printed documents will not be attached to the associated records. The Lifecycle Print will need to be repeated for these records. To avoid sending duplicate letters please destroy any letters printed as part of this batch run. Press 'OK' to acknowledge this action or 'Cancel' to return to the previous screen.";
								} else {
									// A different message is needed if this is an adhoc print (ie a templateId was passed in)
									cancelMessage = "You have chosen to cancel the 'Ad Hoc Print'. In doing so you are confirming the printed material was not produced correctly. The system will not attach the printed documents to the associated records. The Ad Hoc Print will need to be repeated for these records. To avoid sending duplicate letters please destroy any letters printed as part of this batch run. Press 'OK' to acknowledge this action or 'Cancel' to return to the previous screen.";
								}
								if ( confirm(cancelMessage) ) {
									$('#completedTextarea').text($('#completedTextarea').data('defaultText'));
									$('#completedDoneButton').prop('disabled', true);
									$('#completedDiv div').children().unbind();
									$('#completedDiv').hide();
									sendPrintedComplete(serverBridge, id, "false", "");
								}
								});
						$('#completedTextarea').click( function () {
									if ( $('#completedTextarea').val() == $('#completedTextarea').data('defaultText') ) {
										$('#completedTextarea').text("");
									}
								} ).blur( function () {
									if ( $('#completedTextarea').val() == "" ) {
										$('#completedTextarea').text($('#completedTextarea').data('defaultText'));
										$('#completedDoneButton').prop('disabled', true);
									} else {
										$('#completedDoneButton').prop('disabled', false);
									}
								} ).keyup( function () {
									if ( $('#completedTextarea').val() == "" ) {
										$('#completedDoneButton').prop('disabled', true);
									} else {
										$('#completedDoneButton').prop('disabled', false);
									}
								});
					}
				}
				return true;
			}
		},
		error: function (XmlHttpRequest, textStatus, errorThrown) {
			window.clearInterval(timer);			// we're done so stop the timer
			if (XmlHttpRequest && errorThrown ) {
				alert("Error thrown during the print progress phase: " + errorThrown);
			} else {
				alert("Error during the print progress phase; Error: (textStatus) " + textStatus);
			}
			$('#updatingDiv').hide();
		}
	});
}

/*
function sendPrintedComplete(server, id, pass, notes) {
	Log("sendPrintedCompleted sending status");
	var server = server + "completeprint"
	var url = server + "?print=" + id + "&pass=" + pass + "&notes=" + notes;
	return $.ajax({
		dataType: 'jsonp',
		url: url,
		success: function (data, textStatus, XmlHttpRequest) {
			// Check that there is no error from the server
			if ( data.error ) {
				alert("Error while completing the print request: " + data.error);
				return false;
			}
		},
		error: function (XmlHttpRequest, textStatus, errorThrown) {
			if (XmlHttpRequest && errorThrown ) {
				alert("Error thrown during the print completion phase: " + errorThrown);
			} else {
				alert("Error during the print completion phase; Error: (textStatus) " + textStatus);
			}
		},
		complete: function (XmlHttpRequest, textStatus) {
			$('#completedTextarea').text($('#completedTextarea').data('defaultText'));
			$('#completedDoneButton').prop('disabled', true);
			$('#completedDiv div').children().unbind();
			$('#completedDiv').clear();
			try {
				// Try refreshing the grid...
				document.getElementById("crmGrid").control.refresh();
			} catch (e) {
				// ... if it fails then we might be on a form so try reloading that...
				try {
					window.location.reload(true);
				} catch (f) {
					// else do nothing, just prevent an error being generated
				}
			}
		}
	});
}
*/

/*
function sendPDFPrintedComplete(server, id, pass, notes) {
	Log("sendPDFPrintedCompleted sending status");
	var server = server + "completeprint"
	var url = server + "?print=" + id + "&pass=" + pass + "&notes=" + notes;
	window.location.href = url;
}
*/

function sendPDFPrintedComplete(server, id, notes) {
	//Log("sendPDFPrintedCompleted sending status");
	var server = server + "completepdf";
	var url = server + "?print=" + id + "&notes=" + notes;
	window.location.href = url;
}

function init(error, errorText, errorObject) {
	if ( errorText === "initial" || errorText === "parsererror" ) { 
		$.when( createMailMerge(serverBridge, entity, totalIds, templateId, pdfprint) ).then(
			mailMergeCreated
		).fail(
			// The create MailMerge failed, it is likely to be due to a logon timeout
			// (particularly if the errorText is a 'parserError')
			// so make the call again and see what happens this time
			init
		);
	} else {
		alert("Error encountered while trying to re-new logon. Message: " + errorText);
	}
}

init("", "initial");

function mailMergeCreated (createMailMergeResult) {
	if ( createMailMergeResult.forcedError ) {
		//Log("Forced error encountered.");
		return;
	}
	// printId is the reference we got back from the server
	var printId = createMailMergeResult.print;

	$('#updatingText').text("Building mail merge list...");

	var resultsArray = [];
	for (var i = 0, len = ids.length, guids = ""; i < len; i++, guids = "" ) {
		for (var j = 0; j < ids[i].length; j++ ) {
			guids = guids + ids[i][j];
			if ( j < (ids[i].length - 1) ) {
				guids = guids + ",";
			}
		}
		// The AJAX requests are asynchronous so the script starts them and then carries on with the rest of the script...
		resultsArray.push(addMailMerge(serverBridge, entity, printId, guids));
	}

	$.when.apply($, resultsArray).then( function (resultsArrayResult) {
			// ...until it gets here, at which point it waits until all the calls return
			// If we get here then everything is ok, so we can start checking for the printer status
			// There will be a json object with a parameter/value pair of 'error:<message>' we just need to check to see if 'forcedError' exists
			//Log("resultsArray size: " + resultsArray.length + ", resultsArrayResult size: " + resultsArrayResult.length);
			//debugger;
			//Log("All mail merge additions completed");
			if ( resultsArrayResult.forcedError ) {
				// The user has already been informed so we just need to tidy up and finish
				//Log("Error found while building the mail merge list, so stopping here...");
				$('#updatingDiv').hide();
				return false;
			}

			if ( pdfprint === true ) {
				//Log("Starting the progress timer");
				//$('#updatingDiv').show();
				//var timer;
				//timer = window.setInterval(function () { getPrintProgress(timer, serverBridge, printId); }, 3000); 
				//getPrintProgress(timer, serverBridge, printId); 
				//sendPDFPrintedComplete(serverBridge, printId, "ASN");
			}

			$('#updatingDiv').hide();
			Xrm.Utility.openEntityForm("rsl_printjob", printId);
			/*
			try {
				// Try refreshing the grid...
				document.getElementById("crmGrid").control.refresh();
			} catch (e) {
				// ... if it fails then we might be on a form so try reloading that...
				try {
                                      //Reload Form
                                      // SAH 29-Jul-16  CRM2016 Fix
                                      //window.location.reload(true);
                                      window.parent.location.reload(true);
				} catch (f) {
					// else do nothing, just prevent an error being generated
				}
			}
			*/
			return true;
		},
			// Fail section
			function (error) {
				//Log("Progress timer NOT started.");
				if ( error.statusText === "timeout" ) {
					alert("Timeout error." + XmlHttpRequest.responseText);
				}

				if ( error && error.responseText ) {
					alert("Error - " + error.statusText);
				}
				$('#updatingDiv').hide();
			}
		);
	}
/*
	.fail(
		function(error, errorText, errorObject) {
			if ( errorText === "parsererror" ) {
				// just ignore it...
			} else { 
				alert("Got here due to a logon failure...");
			}
			$('#updatingDiv').hide();
		});
		*/
	//});
}
