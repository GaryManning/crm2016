/*
 * Some details about the script should ideally go here
 */
//rsl_load_css_file("../WebResources/rsl_signaturestyles");


function addPrintButtons () {
	// This function is called by the onLoad event of the form.

	var oldButton = $(Xrm.Page.ui.controls.get('WebResource_btnReprintPlaceHolder').getObject()).parent();
	var newButton = $('<button id="PJButton"></button>');


	oldButton.parent().append(newButton);
	oldButton.hide();
	newButton.parent().parent().css('text-align', 'left');
	newButton.css('width', '88px');
	//newButton.attr("disabled", true);

	var isPDF = Xrm.Page.getAttribute('rsl_ispdf').getValue();
	if ( isPDF ) {
		newButton.text("Download PDF");
		newButton.bind('click', clickDownload);
	} else {
		newButton.text("Reprint");
		newButton.bind('click', clickReprint);
	}

	function clickDownload (e) {
		e.preventDefault();
		$(e.target).attr("disabled", true);
		var id = Xrm.Page.data.entity.getId().slice(1,-1).toLowerCase();
		var notes = "PDF DOwnloaded";
		var server = serverBridge + "completepdf";
		//var url = server + "?print=" + id + "&notes=" + notes;
		var url = server + "?print=" + id;
		window.location.href = url;
		$(e.target).attr("disabled", false);
	}

	function clickReprint (e) {
		e.preventDefault();
		$(e.target).attr("disabled", true);
		// Function to actually action the button click
		// Get the uids of any selected prints in the subgrid
		var id = Xrm.Page.data.entity.getId().slice(1,-1).toLowerCase();
		//var selectedIds = document.getElementById("Printed_Documents").control.get_selectedIds();
		var selectedIds = GetSelectedRecords();

		if ( selectedIds.length === 0 ) {
			//alert("No prints selected for re-printing");
			$(e.target).attr("disabled", false);
			return;
		}
		var count = 0;
		$.each(selectedIds, function(k, v) { count = count + v.length; });

		$.when( reprint(id, count) ).then( function (data) {
			// perhaps check to see if 'data' contains an error
			// add ids to mailmerge
			//$('#updatingText').text("Building reprint mail merge list...");
			Xrm.Page.getAttribute('rsl_printjobstatus').setValue(866810000);
			$('#updatingDiv').show();

			var resultsArray = [];
			for (var i = 0, len = selectedIds.length, guids = ""; i < len; i++, guids = "" ) {
				for (var j = 0; j < selectedIds[i].length; j++ ) {
					guids = guids + selectedIds[i][j];
					if ( j < (selectedIds[i].length - 1) ) {
						guids = guids + ",";
					}
				}
				// The AJAX requests are asynchronous so the script starts them and then carries on with the rest of the script...
				resultsArray.push(addMailMerge(id, guids));
			}

			//jobStatusChangeLoop(866810000, false);
			$.when.apply($, resultsArray ).then( function () {
				$('#updatingDiv').hide();
				jobStatusChangeLoop("866810000");
			});
		});
	}


	function GetSelectedRecords() {
		//selectedRecods = document.getElementById("Printed_Documents").control.get_selectedIds();
		//selectedRecord = Xrm.Page.getControl("Printed_Documents").getGrid().getSelectedRows().getAll()[0].getKey().replace(/[{}]/g, "");
		try {
			selectedRecord = Xrm.Page.getControl("Printed_Documents").getGrid().getSelectedRows().get(0).getData().getEntity().getId().slice(1,-1).toLowerCase();
		} catch (e) {
			alert("No prints selected for re-printing");
			return [];
		}

		// We can only select 1 record in the grid (in CRM2016) so we can just return
		return [[selectedRecord]];

		/*
		var max = 5;    // The number of records that can be retrieved in one go
		var shortArrIds;     // This will be the array of shortened arrays
		shortArrIds = [];
		for ( var i = 0, len = selectedRecods.length; i < len; i += max ) {
			shortArrIds.push(selectedRecods.slice(i, i + max));
		}
		return shortArrIds;
		*/
		/*
		// http://dynamicslollipops.blogspot.co.uk/2013/01/dynamics-crm-2011-javascript-getting.html
		// This is required since items selected in a sub-grid are 'lost' when the sub-grid loses focus.
		// This function gives the sub-grid focus and then determines the selected records (or should that be 'Recods'! ;-)
		//var stringOfSelectedIds = "No records were selected";
		if (Xrm.Page.getControl("Printed_Documents") != null) {
			//document.getElementById("Printed_Documents").getElementsByTagName('a')[0].focus();
			$(Xrm.Page.getControl("Printed_Documents")).getElementsByTagName('a')[0].focus();
		}
		//document.getElementById("Printed_Documents").control.get_selectedIds()
		if (document.getElementById("Printed_Documents") != null && document.getElementById("Printed_Documents").control != null) {
			selectedRecods = document.getElementById("Printed_Documents").control.get_selectedIds();

			// There is a character limit on the length of urls that can be passed to the CRM system (about 2084, 25 records)
			// That places a limit on the number of records that can be retrieved in one go since each retrieval is constructed
			// by constructing a string of all the record ids. By breaking the array into sections smaller strings can be
			// constructed and used for getting the CRM records.
			// Split the array into smaller arrays
			var max = 5;    // The number of records that can be retrieved in one go
			var shortArrIds;     // This will be the array of shortened arrays
			shortArrIds = [];
			for ( var i = 0, len = selectedRecods.length; i < len; i += max ) {
				shortArrIds.push(selectedRecods.slice(i, i + max));
			}
			return shortArrIds;
		}
		*/
	}


	function addMailMerge(printId, guids) {
		// Add each batch of records to the print queue
		//Log("addMailMerge sending request: " + printId + " with: " + guids);
		var url = serverBridge + "addmailmerge";

		printId = encodeURI(printId);
		guids = encodeURI(guids);

		var	url = url + "?print=" + printId + "&id=" + guids;
		return $.ajax({
			dataType: 'jsonp',
			   url: url,
			   success: function (data, textStatus, XmlHttpRequest) {
				   // Check that there is no error from the server - so long as the request to the server
				   // completes ok then this section will be executed, which makes it difficult to detemermine
				   // if there have been any other sort of errors (eg wrong reference number etc) so we have to
				   // check to see if the 'error' property of the returned object exists, and if it does then
				   // force a error by setting a flag
				   // Note: this was forced on us by ie refusing to acknowlege any ajax requests with a return
				   // status of anything but '200' :-(
				   //Log("Got this from the server: "+data);
				   if ( data.error ) {
					   alert("Error while building the print request: " + data.error);
					   data.forcedError = true;
					   return false;
				   }
				   return true;
			   },
			   error: function (XmlHttpRequest, textStatus, errorThrown) {
				   if (XmlHttpRequest && errorThrown ) {
					   alert("Error thrown while building the print request: " + errorThrown);
				   } else {
					   alert("Error while building the print request list; Error (textStatus) " + textStatus);
				   }
				   return false;
			   }
		});
	}

	function reprint(id, count) {
		var url = serverBridge + "reprocessmailmerge";
		url = url + "?print=" + id;
		url = url + "&count=" + count;		// Can be 0

		return $.ajax({
			dataType: 'jsonp',
			   url:  url,
			   success: function (data, textStatus, XmlHttpRequest) {
				   // Nothing to do here - it's all done on the server, nothing retruned
			   },
			   error: function (XmlHttpRequest, textStatus, errorThrown) {
				   if (XmlHttpRequest && errorThrown ) {
					   alert("Error during the reprint creation phase; Error (textStatus): " + textStatus);
					   alert("Error thrown" + errorThrown);
				   }
			   }
		});
	}

	jQuery.fn.center = function() {
		this.css("position", "absolute");
		this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
		this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
		return this;
	}

	var ConfigureAjaxUpdating = function() {
		var Completed = false;
		var text = "Reprocessing records...";
		return function() {
			if (Completed) { return; }
			$('body').append('<div id="updatingDiv"></div>');
			$('#updatingDiv').append('<p id="updatingText">' + text + '</p>')
				.css('background', 'url(/_imgs/AdvFind/progress.gif) no-repeat center')
				.css('background-color', '#CCCCCC')
				.css('height', '50px')
				.css('width', '100px')
				.center()
				.hide();
			$('#updatingText').css('text-align', 'center')
				.css('font', '14px bolder')
				.css('font-family', 'Segoe UI, Tahoma, Arial');

			Completed = true;
		};
	} ();

	ConfigureAjaxUpdating();

}
