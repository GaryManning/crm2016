function init() {
	$('#adhocTitle').css('text-align', 'center')
	$('#adhocTemplateDiv').data('listBuilt', false);
	$('#adhocDoneButton').prop('disabled', true);
}

function adhocPrints (params) {
	var data = decodeURIComponent(params.split(/=/)[1]);
	var ids = data.split(/&/)[0].split(/=/)[1];
	var entity = data.split(/&/)[1].split(/=/)[1];
	var popup = data.split(/&/)[2].split(/=/)[1];
	var winWidth = data.split(/&/)[3].split(/=/)[1];
	var winHeight = data.split(/&/)[4].split(/=/)[1];
	/*
	var grid = data.split(/&/)[3].split(/=/)[1];
	var pdfprint = data.split(/&/)[4].split(/=/)[1];
	*/
	/*
	 * Overview: this function presents a list of appropriate templates to the user
	 */

	var cssNotSelected = {"background": "#ffffff", "color": "#000000"}
	var cssSelected = {"background": "#000000", "color": "#ffffff"}

	$('#adhocTemplateDiv').css('width', winWidth - 10);		// As wide as the window minus the left and right margins of this div
	$('#adhocTemplateDiv').css('height', winHeight - $('#adhocTitle').outerHeight() - $('#adhocButtonDiv').outerHeight() + 28);	// Remove the height of the title and button divs

	function getTemplates (nextURL) {
		var entitySet = "rsl_doctemplate"+"Set";
			
		var URL;
		if ( nextURL ) {
			URL = nextURL;
		} else {
			URL = serverXRMServices + entitySet;
			URL = URL + "?$select=rsl_name,rsl_doctemplateId,rsl_primarysourceofdata&$filter=rsl_primarysourceofdata/Value eq ";

			// The hard-coded option set values for 'Penalty' and 'Prosecution'
			if ( entity == "rsl_penalty" ) {
				URL = URL + "866810000";
			} else {
				URL = URL + "866810001";
			}

			URL = URL + "&$orderby=rsl_name";
		}

		$.ajax({
			type: "GET",
			contentType: "application/json; charaset=utf-8",
			datatype: "json",
			url: URL,
			beforeSend: function (XMLHttpRequest) {
				//Specifiying this header ensures that the results will be returned as JSON
				XMLHttpRequest.setRequestHeader("Accept", "application/json");
			},
			success: function (data, textStatus, XmlHttpRequest) {
				$.when(processRecievedTemplates(data)).then( function(data) {
					if ( data.d.__next ) {
						// Only 50 records can be retrieved at a time, so check
						// if there is a '__next' property and use it to call the getTemplates function again
						// Hopefully this should just loop round and round until all the records have been retrieved
						getTemplates(data.d.__next);
					}
				});
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				if (XmlHttpRequest && XmlHttpRequest.responseText ) {
					alert("Error while retrieving " + entitySet + "; Error: " + XmlHttpRequest.responseText);
				}
			}
		});
	}

	function processRecievedTemplates(data) {
		if ( data.d.results.length != 0 ) {
			$('#adhocDoneButton').off('click'); // Prevent multiple handlers being attached in the cases where several calls were made to get all the templates
			// Build list of documents with the returned data
			//var templateList = $('<ul id="templateListUL"></ul>');
			$(data.d.results).each( function (index, value) {
				$('#templateListUL').append(
					$('<li>'+value.rsl_name+'</li>').bind('click',
						function () {
							$('#templateListUL > li').css(cssNotSelected);
							$('#adhocDoneButton').prop('disabled', false);
							$('#adhocDoneButton').data('selectionName', value.rsl_name);
							$('#adhocDoneButton').data('selectionId', value.rsl_doctemplateId);
							$(this).css(cssSelected);
						})
					);
			});
			//$('#adhocTemplateDiv').append(templateList);
			$('#adhocTemplateDiv').data('listBuilt', true);
			$('#adhocDoneButton').on('click', doneButtonHandler);
			$('#adhocCancelButton').click( function () {
				/*
				$('#templateListUL > li').css(cssNotSelected);
				$('#adhocDoneButton').prop('disabled', true);
				$('#adhocDiv').hide();
				*/
				window.close();
			});
			//$('#adhocDiv').show();
		} else {
			alert("No data found.");
			//window.close();
		}
		return data;
	}

	if ( ! $('#adhocTemplateDiv').data('listBuilt') ) {
		$.when( getTemplates() ).then( function (templateData) {
			Log("Got template data");
		});
	} else {
		$('#adhocDoneButton').off('click'); 
		$('#adhocDoneButton').on('click', doneButtonHandler);
		$('#adhocDiv').show();
	}

	function  doneButtonHandler(e) {
		if ( selectTemplate(ids, $('#adhocDoneButton').data('selectionId'), $('#adhocDoneButton').data('selectionName')) ) {
			$('#templateListUL > li').css(cssNotSelected);
			$('#adhocDoneButton').prop('disabled', true);
		} else {
			$('#adhocDiv').show();
		}
	}

	function selectTemplate (ids, templateId, templateName) {
		Xrm.Utility.confirmDialog("You have selected " + templateName + " as the template to use. Press 'Ok' to continue otherwise press 'Cancel'",
				function() { 
					//popupName = popup;
					//popupWindow(ids, entity, popupName);
					window.opener.Xrm.Page.context.rsl_deferred.resolve({"template": "&template="+templateId});
					setTimeout(function() { window.close(); }, 1000);
				},
				function() {
					return false;}
				);
		}

	function selectTemplate_orig (ids, templateId, templateName) {
		if ( confirm("You have selected " + templateName + " as the template to use. Press 'Ok' to continue otherwise press 'Cancel'") == false ) {
			return false;
		}
		popupName = popup;
		popupWindow(ids, popupName, {"template": "&template="+templateId});
		return true;
	}
}
