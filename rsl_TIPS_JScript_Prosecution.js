// 'Prosecution' Form validation Javascript
//
// Calculate total Prosecution Liability
prosecution_CalcTotalLiability = function(save) {
    //Prosecution Status
    var myprosecutionstatus = Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").getValue();
    //Open/Closed
    var myopenclosed = Xrm.Page.getAttribute("rsl_pros_prosecutionstatusopenclosed").getValue();
    // Get the value of Prosecution Costs Awarded

    var myprosecutioncostsawardedvalue = Xrm.Page.getAttribute("rsl_pros_money_costsawarded").getValue();
    // Get the value of Prosecution Compensation Awarded
    var myprosecutioncompawardedvalue = Xrm.Page.getAttribute("rsl_pros_money_compensationawarded").getValue();
    // Get the value of Prosecution Costs Received
    var myprosecutioncostsreceivedvalue = Xrm.Page.getAttribute("rsl_pros_money_costsreceived").getValue();
    // Get the value of Prosecution Compensation Awarded
    var myprosecutioncompreceivedvalue = Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").getValue();
    // Get the value of OoC Settlement Requested
    var myprosecutionoocsetvalue = Xrm.Page.getAttribute("rsl_pros_money_outofcourtsettlement").getValue();
    // Ensure if the values are null set them to zero
    if (myprosecutioncostsawardedvalue == null) { myprosecutioncostsawardedvalue = 0 };
    if (myprosecutioncompawardedvalue == null) { myprosecutioncompawardedvalue = 0 };
    if (myprosecutioncostsreceivedvalue == null) { myprosecutioncostsreceivedvalue = 0 };
    if (myprosecutioncompreceivedvalue == null) { myprosecutioncompreceivedvalue = 0 };
    if (myprosecutionoocsetvalue == null) { myprosecutionoocsetvalue = 0 };
    // Calc Total Liability
    var balanceRemaining = (myprosecutioncompawardedvalue + myprosecutioncostsawardedvalue + myprosecutionoocsetvalue) - (myprosecutioncostsreceivedvalue + myprosecutioncompreceivedvalue);
    Xrm.Page.getAttribute("rsl_pros_totalawarded").setValue(balanceRemaining);
    if (save == 1) {
        Xrm.Page.getAttribute("rsl_pros_totalawarded").setSubmitMode("always");
    }
    else {
        Xrm.Page.getAttribute("rsl_pros_totalawarded").setSubmitMode("never");
    }

    //
    // Run some checks on Liability remaining and Prosecution Status
    if (balanceRemaining < 0) {
        // Alert the user to the fact the remaining balance is negative
        alert("Total Liability is negative!  Are you sure your payments are correct?");
    }
    else if (balanceRemaining == 0) {
        // Alert the user to the fact the Total Liability is zero and request to Close the
        // Prosecution using the right status if the Prosecution is not already Closed.
        //
        // We have an OoC Settlement Requested to assume this to be the closure type.
        if (myprosecutionoocsetvalue > 0 || myprosecutioncostsawardedvalue > 0 || myprosecutioncompawardedvalue > 0) {
            if (myopenclosed == "Open" && myprosecutionoocsetvalue > 0 && myprosecutionstatus != 866810006) {
                if (confirm("Total Due is zero.  Do you wish to close the Prosecution as 'Closed - Out of Court Settlement'?")) {
                    Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setSubmitMode("always");
                    Xrm.Page.data.entity.attributes.get("rsl_pros_prosecutionstatus").setValue(866810006);
                    Xrm.Page.getAttribute("rsl_pros_dateclosed").setSubmitMode("always");
                    Xrm.Page.data.entity.attributes.get("rsl_pros_dateclosed").setValue(new Date());
                    Xrm.Page.data.entity.save();
                }
            }
            else if (myopenclosed == "Open" && myprosecutionoocsetvalue == 0 && myprosecutionstatus != 866810028) {
                if (confirm("Total Due is zero.  Do you wish to close the Prosecution as 'Closed - Prosecution Paid in Full'?")) {
                    Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setSubmitMode("always");
                    Xrm.Page.data.entity.attributes.get("rsl_pros_prosecutionstatus").setValue(866810028);
                    Xrm.Page.getAttribute("rsl_pros_dateclosed").setSubmitMode("always");
                    Xrm.Page.data.entity.attributes.get("rsl_pros_dateclosed").setValue(new Date());
                    Xrm.Page.data.entity.save();
                }
            }
        }
    }
    // Total Liability >0
    else {
        // Alert the user to the fact the Total Liability is >0 and Pros set to Closed Fully Paid.
        if (myprosecutionstatus == 866810028) {
            alert("WARNING - The Total Liability is greater than zero however the Prosecution has been 'Closed - Full Paid'. Attention required.");
        }
    }
}
//
// Set all controls to be editable if the form is running in CREATE mode
prosecution_SetControlsCreateMode = function() {
    // Get the mode the Form is in
    var formType = Xrm.Page.ui.getFormType();
    // Get the Prosecution type
    var myProsecutionStatusFormValue = Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").getValue();
    //
    // Enable/Disable fields based on Prosecution Status
    //
    // If the Summons has been Printed Disabled all the fields that are normally editable
    // e.g. Lock down the record for further changes
    if (Xrm.Page.getAttribute("rsl_pros_datesummonsprinted").getValue() != null) {
        // Disable Status
        Xrm.Page.ui.controls.get("rsl_pros_prosecutionstatus").setDisabled(true);
        // Disable Law Type
        Xrm.Page.ui.controls.get("rsl_pros_lawtype").setDisabled(true);
        // Disable Date of Offence
        Xrm.Page.ui.controls.get("rsl_pros_dateofoffence").setDisabled(true);
        // Disable Prosecution Type
        Xrm.Page.ui.controls.get("rsl_pros_prosecutiontypelookup").setDisabled(true);
        // Disable Travel Class
        Xrm.Page.ui.controls.get("rsl_pros_travelclass").setDisabled(true);
        // Disable Fare Avoided
        Xrm.Page.ui.controls.get("rsl_pros_money_fareavoided").setDisabled(true);
        // Disable Court booking Notes
        Xrm.Page.ui.controls.get("rsl_pros_courtbookingnotes").setDisabled(true);
        // Disable ASN
        Xrm.Page.ui.controls.get("rsl_pros_asn").setDisabled(true);
        // Disable Inspector
        Xrm.Page.ui.controls.get("rsl_pros_inspector").setDisabled(true);
        // Disable Originator
        Xrm.Page.ui.controls.get("rsl_prose_originator").setDisabled(true);
        // Disable IssuedOn
        Xrm.Page.ui.controls.get("rsl_pros_issuedon").setDisabled(true);
        // Disable Offender
        Xrm.Page.ui.controls.get("rsl_pros_person").setDisabled(true);
        // Disable From/To/At Stations
        Xrm.Page.ui.controls.get("rsl_pros_fromstation").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_ticketissuedat").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_tostation").setDisabled(true);
        // Disable Caution Fields
        Xrm.Page.ui.controls.get("rsl_pros_cautionresponse1").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_cautionresponse2").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_cautionresponse3").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_cautionresponse4").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_purchasefacilitiesconfirmed").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_bylawoffenderscomments").setDisabled(true);
        // Disable Initail Letter Response Field
        Xrm.Page.ui.controls.get("rsl_pros_initial_letter_response_list").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_initial_letter_response_date").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_initial_letter_response_text").setDisabled(true);
        // Disable Witness
        Xrm.Page.ui.controls.get("rsl_pros_witnesses").setDisabled(true);
        // Disable Passengersexcuse (Grounds)
        Xrm.Page.ui.controls.get("rsl_pros_passengersexcuse").setDisabled(true);
        // Disable Passengersexcuse (Grounds)
        Xrm.Page.ui.controls.get("rsl_pros_passengersexcusetype").setDisabled(true);
        // Disable Inspectors View
        Xrm.Page.ui.controls.get("rsl_pros_visualview").setDisabled(true);
        // Disable Weather
        Xrm.Page.ui.controls.get("rsl_pros_weather").setDisabled(true);
        // Disable Brief Events
        Xrm.Page.ui.controls.get("rsl_pros_briefevents").setDisabled(true);
        // Court Paperwork
        Xrm.Page.ui.controls.get("rsl_pros_paperworkwitnessstatement").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_paperworkstatementoffacts").setDisabled(true);
        Xrm.Page.ui.controls.get("rsl_pros_paperworksummons").setDisabled(true);
    }
    // New Record OR we are in a "Partial Raised" (866810027) Status
    else if (formType == 1 || myProsecutionStatusFormValue == 866810027) {
        //
        // SHOW/HIDE, ENABLE/DISABLE Controls.  
        // Mainly used to hide fields that are not entered at the time of 
        // creating the Prosecution.  e.g. ASN number
        //
        // Disable Number
        Xrm.Page.ui.controls.get("rsl_name").setVisible(false);
        // Disable Status
        Xrm.Page.ui.controls.get("rsl_pros_prosecutionstatus").setDisabled(true);
        // Disable Date of Offence
        Xrm.Page.ui.controls.get("rsl_pros_dateofoffence").setDisabled(false);
        // Disable Inspector
        Xrm.Page.ui.controls.get("rsl_pros_inspector").setDisabled(false);
        // Hide How Proved at Offence
        Xrm.Page.ui.controls.get("rsl_pros_howproved").setVisible(false);
        // Hide Date Closed
        Xrm.Page.ui.controls.get("rsl_pros_dateclosed").setVisible(false);
        // Hide Time to issue Ticket
        Xrm.Page.ui.controls.get("rsl_pros_timetoissueticket").setVisible(false);
        // Hide Void Reason
        Xrm.Page.ui.controls.get("rsl_pros_voidreason").setVisible(false);
        // Hide Right-hand part Section of Status Tab
        Xrm.Page.ui.tabs.get("Status").sections.get("Status_section_2").setVisible(false);
        // Hide DOB at Offence
        //Xrm.Page.ui.controls.get("rsl_pros_dob").setVisible(false);
        // Hide Passenger Type at Offence
        //Xrm.Page.ui.controls.get("rsl_pros_passengertype").setVisible(false);
        // Hide Guardian at Offence
        //Xrm.Page.ui.controls.get("rsl_pros_guardian").setVisible(false);
        // Hide Age at Offence
        Xrm.Page.ui.controls.get("rsl_pros_age").setVisible(false);
        // Hide Firstname
        //Xrm.Page.ui.controls.get("rsl_pros_firstname").setVisible(false);
        // Hide Lastname
        //Xrm.Page.ui.controls.get("rsl_pros_lastname").setVisible(false);
        // Hide Fullname
        //Xrm.Page.ui.controls.get("rsl_pros_fullname").setVisible(false);
        // Hide Originating Penalty Fare
        Xrm.Page.ui.controls.get("rsl_pros_originatingpenaltyfare").setVisible(false);
        // Hide Initial Letter Response TAB
        Xrm.Page.ui.tabs.get("Initial_Letter_Response").setVisible(false);
        // Disable "Jurisdiction Court"
        Xrm.Page.ui.controls.get("rsl_pros_jurisdictioncourt").setVisible(false);
        // Hide "Court Paperwork Statements" TAB
        Xrm.Page.ui.tabs.get("Court Paperwork Statements").setVisible(false);
        // Hide "Court Outcome" TAB
        Xrm.Page.ui.tabs.get("Payment").setVisible(false);
        // Hide "Prosecution Photograph" TAB
        Xrm.Page.ui.tabs.get("Prosecution_Photograph").setVisible(false);
        // Hide "Printed Documents" TAB
        Xrm.Page.ui.tabs.get("Printed_Documents").setVisible(false);
        // Hide "Timeline" TAB
        Xrm.Page.ui.tabs.get("Timeline").setVisible(false);
        // Hide "Admin" TAB
        Xrm.Page.ui.tabs.get("Admin").setVisible(false);
        //
        // ---------------------------------------------------------------------------
        // Set Sensible Default Values but only if we are in Create mode.
        //
        if (formType == 1) {
            // Always Submit the Status
            Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setSubmitMode("always");
            // Mark as Back Office Created
            Xrm.Page.getAttribute("rsl_pros_backofficecreated").setSubmitMode("always");
            Xrm.Page.getAttribute("rsl_pros_backofficecreated").setValue(866810001);
            // Default Excuse Type to "no Excuse Provided"
            Xrm.Page.getAttribute("rsl_pros_passengersexcusetype").setValue(866810004);
            // Set Money fields to zero
            Xrm.Page.getAttribute("rsl_pros_money_fineawarded").setValue(0);
            Xrm.Page.getAttribute("rsl_pros_money_outofcourtsettlement").setValue(0);
            Xrm.Page.getAttribute("rsl_pros_money_compensationawarded").setValue(0);
            Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").setSubmitMode("always");
            Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").setValue(0);
            Xrm.Page.getAttribute("rsl_pros_money_costsawarded").setValue(0);
            Xrm.Page.getAttribute("rsl_pros_money_costsreceived").setSubmitMode("always");
            Xrm.Page.getAttribute("rsl_pros_money_costsreceived").setValue(0);
            Xrm.Page.getAttribute("rsl_pros_totalawarded").setSubmitMode("always");
            Xrm.Page.getAttribute("rsl_pros_totalawarded").setValue(0);
        }
    }
    // Form is in EDIT/UPDATE Mode make all controls enabled.
    else if (formType == 2) {
        //
        // SHOW/HIDE, ENABLE/DISABLE Controls.  
        // Mainly used to hide fields that should not be edited after the event of issuing a Prosecution
        // Disable Status
        Xrm.Page.ui.controls.get("rsl_pros_prosecutionstatus").setDisabled(true);
        // Disable Time to Issue Ticket
        Xrm.Page.ui.controls.get("rsl_pros_timetoissueticket").setDisabled(true);
        // Disable Jurisdiction Court if we have one, otherwise set this to be editable.
        // Modified: Simon Hopper  25-Jun-15
        // Changed as Malk required to changed the jurisdiction court manually.
        // Changed the IF to allow you to change the JC up until the Court has been Booked into Court.
        // if (Xrm.Page.getAttribute("rsl_pros_jurisdictioncourt").getValue() != null) {
        if (Xrm.Page.getAttribute("rsl_pros_dateassignedtocourt").getValue() != null) {
            Xrm.Page.ui.controls.get("rsl_pros_jurisdictioncourt").setDisabled(true);
        }
        else {
            Xrm.Page.ui.controls.get("rsl_pros_jurisdictioncourt").setDisabled(false);
        }
        // Disable Initial Letter Response if we are already booking into court.
        if (Xrm.Page.getAttribute("rsl_pros_courtappearance").getValue() != null) {
            Xrm.Page.ui.controls.get("rsl_pros_initial_letter_response_list").setDisabled(true);
        }
        else {
            Xrm.Page.ui.controls.get("rsl_pros_initial_letter_response_list").setDisabled(false);
        }
    }
}
//
// Set all controls to be editable if the form is running in CREATE mode
//prosecution_FillNullStations = function() {
//    var IssuedAtStation = Xrm.Page.getAttribute("rsl_pros_ticketissuedat").getValue()
//    var ToStation = Xrm.Page.getAttribute("rsl_pros_tostation").getValue()
//    //
//    // If Issued set and To Not
//    if (IssuedAtStation != null && ToStation == null) {
//        Xrm.Page.getAttribute("rsl_pros_tostation").setValue(IssuedAtStation)
//    }
//    if (IssuedAtStation == null && ToStation != null) {
//        Xrm.Page.getAttribute("rsl_pros_ticketissuedat").setValue(ToStation)
//    }
//}

// Sets the Age field given the DOB
prosecution_dob = function() {
    if (Xrm.Page.getAttribute("rsl_pros_dob").getValue() != null) {
        var now = new Date(); //get today's date
        var birthday = Xrm.Page.getAttribute("rsl_pros_dob").getValue(); //get the dob value  
        var diff = now.getMonth() - birthday.getMonth();  //have they had their birthday already this year?  
        if (diff > -1) //if they've had a birthday this year  
        {
            var bd1 = now.getFullYear() - birthday.getFullYear();
            Xrm.Page.data.entity.attributes.get("rsl_pros_age").setValue(bd1);
        }
        else //if they have not had a birthday yet this year  
        {
            var bd2 = now.getFullYear() - birthday.getFullYear() - 1;
            Xrm.Page.data.entity.attributes.get("rsl_pros_age").setValue(bd2);
        }
    }
}

// Sets the Adult/Child option set depending on whether the persons age if <18
prosecution_passengertype = function() {
    if (Xrm.Page.getAttribute("rsl_pros_age").getValue() != null) {
        var age = Xrm.Page.getAttribute("rsl_pros_age").getValue(); //get the age value  
        if (age >= 18) //if they are less than 18 set OptionSet to Adult
        {
            Xrm.Page.getAttribute("rsl_pros_passengertype").setValue(0);

        }
        else //They must be an Child
        {
            Xrm.Page.getAttribute("rsl_pros_passengertype").setValue(1);
        }
    }
}

// Enables Guardian based on if PassengerType=Child
prosecution_guardianstate = function() {
    // If Passenger type=Child
    var myOptionSet = Xrm.Page.data.entity.attributes.get("rsl_pros_passengertype");
    var optionSetValue = myOptionSet.getValue();
    var guardiansname = Xrm.Page.data.entity.attributes.get("rsl_pros_guardian");
    var temp_guardiansname = Xrm.Page.data.entity.attributes.get("rsl_pros_temp_guardiansname");

    if (optionSetValue == 0) {
        if (guardiansname.getValue() != null) {
            temp_guardiansname.setValue(guardiansname.getValue());
        }
        guardiansname.setValue(null);
        Xrm.Page.ui.controls.get("rsl_pros_guardian").setDisabled(true);
    }
    else //Enable control
    {
        if (temp_guardiansname.getValue() != null) {
            guardiansname.setValue(temp_guardiansname.getValue());
        }
        temp_guardiansname.setValue(null);

        Xrm.Page.ui.controls.get("rsl_pros_guardian").setDisabled(false);
    }
}

// Out of Court Settlement Money amount changed so offer to Closed the Prosecution as "Closed - Out of Court Settlement"
prosecution_initialletterresponse = function() {
    var myilr_item = Xrm.Page.getAttribute("rsl_pros_initial_letter_response_list").getValue();
    var myilr_date = Xrm.Page.getAttribute("rsl_pros_initial_letter_response_date").getValue();

    if (myilr_item != 866810000 && myilr_date == null) {
        Xrm.Page.getAttribute("rsl_pros_initial_letter_response_date").setValue(new Date());
    }
}



// Called ONSAVE.	Actions
// Have we finished adding the missing details and are happy to 
// set this into "Initial Letter Req. Printing" 866810001
prosecution_saveprecheck = function(executionObj) {
    // Get the Prosecution type
    var myProsecutionStatusFormValue = Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").getValue();
    var alertresponse = null;
    // If Statis is "Partially Raised"
    if (myProsecutionStatusFormValue == 866810027) {
        var alertresponse = confirm("This Prosecution is in a 'Partially Raised' status meaning that not all the necessary details were provided when created from the device.\n\nIf all the details for prosecution have been fully created using the inspectors notes select 'OK' otherwise select 'Cancel'.");
    }
    // If I am in 'Partially Raised' and I have said I am done OR i am not in 'Parially Raised' RUN ALL SAVE VALiDATIONS
    if (alertresponse == true || alertresponse == null) {
        SaveValidation(executionObj, true);
    }
}
// ***********************************************************************************
// ****************************  SAVE VALIDATION  ************************************
// ***********************************************************************************

// Check Passenger Type and Age do not conflict.  Prevent save if they do.
SaveValidation = function(executionObj, movestatus) {

    var myProsecutionStatusFormValue = Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").getValue(); // Capture Penalty Status On form
    //var myOptionSet = Xrm.Page.data.entity.attributes.get("rsl_pros_passengertype");
    //var optionSetValue = myOptionSet.getValue();
    // If the Passenger is Adult AND Age<18 then error and stop save
    //if (optionSetValue == 0 && var_age < 18) {
    //	  alert("Passenger specified as an Adult but DOB states they are under 18.");
    //	  executionObj.getEventArgs().preventDefault();
    //}
    //else if (optionSetValue == 1 && var_age > 17) {
    //	  alert("Passenger specified as an Child but DOB states they are 18 or over.");
    //	  executionObj.getEventArgs().preventDefault();
    //}
    // ***************************************************************************
    // ********************* Individual Field Checks *****************************
    // ***************************************************************************
    //
    // TOC
    var var_toc = Xrm.Page.getAttribute("rsl_toc").getValue();
    if (var_toc == null && myProsecutionStatusFormValue != 866810011) {
        alert("Record cannot be saved.  'TOC' must be specified.");
        Xrm.Page.getControl("rsl_toc").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Prosecution Type
    var var_type = Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup").getValue();
    if (var_type == null && myProsecutionStatusFormValue != 866810011) {
        alert("Record cannot be saved.  'Prosecution Type' must be specified.");
        Xrm.Page.getControl("rsl_pros_prosecutiontypelookup").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Date of Offence
    var var_dateofoffence = Xrm.Page.getAttribute("rsl_pros_dateofoffence").getValue();
    if (var_dateofoffence == null && myProsecutionStatusFormValue != 866810011) {
        alert("Record cannot be saved.  'Date of Offence' must be specified.");
        Xrm.Page.getControl("rsl_pros_dateofoffence").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Originator
    var var_originator = Xrm.Page.getAttribute("rsl_prose_originator").getValue();
    if (var_originator == null && myProsecutionStatusFormValue != 866810011) {
        alert("Record cannot be saved.  'Originator' must be specified.");
        Xrm.Page.getControl("rsl_prose_originator").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // DOB or PassengerType
    //var var_DOB = Xrm.Page.getAttribute("rsl_pros_dob").getValue();
    //var var_passengertype = Xrm.Page.getAttribute("rsl_pros_passengertype").getValue();
    //if (var_DOB == null && var_passengertype == null) {
    //	  alert("Record cannot be saved.  Either 'DOB' or 'Passenger Type' must be specified.");
    //	  Xrm.Page.getControl("rsl_pros_dob").setFocus(true);
    //	  executionObj.getEventArgs().preventDefault();
    //	  return;
    //}
    // Inspector
    var var_inspector = Xrm.Page.getAttribute("rsl_pros_inspector").getValue();
    if (var_inspector == null && myProsecutionStatusFormValue != 866810011) {
        alert("Record cannot be saved.  'Inspector' must be specified.");
        Xrm.Page.getControl("rsl_pros_inspector").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    var var_person = Xrm.Page.getAttribute("rsl_pros_person").getValue();
    if (var_person == null && myProsecutionStatusFormValue != 866810011) {
        alert("Record cannot be saved.  'Offender' must be specified.");
        Xrm.Page.getControl("rsl_pros_person").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Validate Stations based on the Prosecution Type Jurisdication Court which is held
    // in global variable myglobalfromat (created onload).
    // We only need the station for the From, To or At returned in myglobalfromat.
    // If we do not have a value mandate that From/To/At must ALL be specified.
    var var_ticketissuedat = Xrm.Page.getAttribute("rsl_pros_ticketissuedat").getValue();
    var var_fromstation = Xrm.Page.getAttribute("rsl_pros_fromstation").getValue();
    var var_tostation = Xrm.Page.getAttribute("rsl_pros_tostation").getValue();
    //
    // AT Station needed to base Jurisdication Court
    if (myglobalfromat == 866810000) {
        if (var_ticketissuedat == null && myProsecutionStatusFormValue != 866810011) {
            alert("Record cannot be saved.  'Issued At Station' must be specified.");
            Xrm.Page.getControl("rsl_pros_ticketissuedat").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    //
    // FROM Station needed to base Jurisdication Court
    else if (myglobalfromat == 866810001) {
        if (var_fromstation == null && myProsecutionStatusFormValue != 866810011) {
            alert("Record cannot be saved.  'From Station' must be specified.");
            Xrm.Page.getControl("rsl_pros_fromstation").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    //
    // TO Station needed to base Jurisdication Court
    else if (myglobalfromat == 866810002) {
        if (var_tostation == null && myProsecutionStatusFormValue != 866810011) {
            alert("Record cannot be saved.  'To Station' must be specified.");
            Xrm.Page.getControl("rsl_pros_tostation").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    //    else if (myglobalfromat != 866810000 && myglobalfromat != 866810001 && myglobalfromat != 866810002) {
    //        if ((var_ticketissuedat == null || var_fromstation == null || var_tostation == null) && myProsecutionStatusFormValue != 866810011) {
    //            alert("Record cannot be saved.  'From Station', 'To Station'  and 'Issued At Station' must be specified.");
    //            if (var_fromstation == null) {
    //                Xrm.Page.getControl("rsl_pros_fromstation").setFocus(true);
    //            }
    //            else if (var_ticketissuedat == null) {
    //                Xrm.Page.getControl("rsl_pros_ticketissuedat").setFocus(true);
    //            }
    //            else {
    //                Xrm.Page.getControl("rsl_pros_ticketissuedat").setFocus(true);
    //            }
    //            executionObj.getEventArgs().preventDefault();
    //            return;
    //        }
    //    }
    //    // Passengersgroundstype
    //    var var_passengersexcusetype = Xrm.Page.getAttribute("rsl_pros_passengersexcusetype").getValue();
    //    if (var_passengersexcusetype == null && myProsecutionStatusFormValue != 866810011) {
    //        alert("Record cannot be saved.  'Passengers Grounds Picklist' must be specified.");
    //        Xrm.Page.getControl("rsl_pros_passengersexcusetype").setFocus(true);
    //        executionObj.getEventArgs().preventDefault();
    //        return;
    //    }
    //
    // ****************************************************************************
    // ********************* Partially Created Checks *****************************
    // *********** These details will be provided from inspectors notepad. ********
    // ****************************************************************************
    //                                  Partially Created
    if (myProsecutionStatusFormValue == 866810027) {
        // Law Type (Bylaw/MG11)
        var var_lawtype = Xrm.Page.getAttribute("rsl_pros_lawtype").getValue();
        if (var_lawtype == null) {
            alert("Record cannot be saved.  'Law Type' must be specified.");
            Xrm.Page.getControl("rsl_pros_lawtype").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
        // Travel Class
        var var_travelclass = Xrm.Page.getAttribute("rsl_pros_travelclass").getValue();
        if (var_travelclass == null) {
            alert("Record cannot be saved.  'Travel Class' must be specified.");
            Xrm.Page.getControl("rsl_pros_travelclass").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
        // Fare Avoided
        var var_travelclass = Xrm.Page.getAttribute("rsl_pros_money_fareavoided").getValue();
        if (var_travelclass == null) {
            alert("Record cannot be saved.  'Fare Avoided' must be specified.\n\n\If 'Fare Avoided' is not applicable for this Prosecution type set this to zero.");
            Xrm.Page.getControl("rsl_pros_money_fareavoided").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
        // Issed On
        var var_travelclass = Xrm.Page.getAttribute("rsl_pros_issuedon").getValue();
        if (var_travelclass == null) {
            alert("Record cannot be saved.  'Issued On' must be specified.");
            Xrm.Page.getControl("rsl_pros_issuedon").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    // ******************************************************************************************
    // ************************ Initial Letter Response Validations *****************************
    // ******************************************************************************************
    var var_initialletterresponselist = Xrm.Page.getAttribute("rsl_pros_initial_letter_response_list").getValue();
    var var_initialletterresponsedate = Xrm.Page.getAttribute("rsl_pros_initial_letter_response_date").getValue();
    var var_initialletterresponsetext = Xrm.Page.getAttribute("rsl_pros_initial_letter_response_text").getValue();
    var var_prosecutionstatus = Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").getValue();

    //
    // We have chosen and Initial Letter Response but the Prosecution is not in the right status to do this.
    //
    // SAH 3-Jul-14
    // Removed this validation as when the Status passes Initial Letter Response (Court Needs Booking) this validation was incorrectly triggering.
    //    if (myProsecutionStatusFormValue != 866810002 && (var_initialletterresponselist == 866810001 || var_initialletterresponselist == 866810002)) {
    //        alert("Record cannot be saved.  Initial Letter Response value can only be set if the prosecution is in a status of 'Wait...Initial Letter Printed.'\n\n\Setting this value back to 'No Response Received'.");
    //        Xrm.Page.getAttribute("rsl_pros_initial_letter_response_list").setValue(866810000);
    //        Xrm.Page.getAttribute("rsl_pros_initial_letter_response_date").setValue(null);
    //        executionObj.getEventArgs().preventDefault();
    //        return;
    //    }

    // If we have something from the picklist which is not "No Response" we must set a response date
    //																			  No Response
    if (var_initialletterresponselist != null && var_initialletterresponselist != 866810000 && var_initialletterresponsedate == null) {
        alert("Record cannot be saved.  Initial Letter Response Date must be set if an Initial Letter picklist item has been specified.");
        Xrm.Page.getControl("rsl_pros_initial_letter_response_date").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // If Initial Letter Response Date specified then we must select a Reponse from the picklist.
    //																														No Response
    if (var_initialletterresponsedate != null && (var_initialletterresponselist == null || var_initialletterresponselist == 866810000)) {
        alert("Record cannot be saved.  If Initial Letter Response Date specified, an entry other than 'No Response' must be selected from the picklist.");
        Xrm.Page.getControl("rsl_pros_initial_letter_response_list").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // If Initial Response Fields specified and Prosecution Status="Wait...Initial Letter Printed" move Status on to 'Raised'
    //   if ((var_initialletterresponsedate != null || var_initialletterresponselist != null) && var_prosecutionstatus != 866810002) {
    //        Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setValue(866810000);
    //    }
    // **************************************************************************************
    // ************************ Financial Validations ***************************************
    // **************************************************************************************
    var var_fineawarded = Xrm.Page.getAttribute("rsl_pros_money_fineawarded").getValue();
    var var_ocsreceived = Xrm.Page.getAttribute("rsl_pros_money_outofcourtsettlement").getValue();
    var var_costsawarded = Xrm.Page.getAttribute("rsl_pros_money_costsawarded").getValue();
    var var_costsreceived = Xrm.Page.getAttribute("rsl_pros_money_costsreceived").getValue();
    var var_compawarded = Xrm.Page.getAttribute("rsl_pros_money_compensationawarded").getValue();
    var var_compreceived = Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").getValue();
    var var_hearingref = Xrm.Page.getAttribute("rsl_pros_hearingreference").getValue();
    var var_totalliability = Xrm.Page.getAttribute("rsl_pros_totalawarded").getValue();
    // Out of court & Court Outcome specified.
    if (var_ocsreceived > 0 && (var_fineawarded > 0 || var_costsawarded)) {
        alert("Court outcome information and Out of Court information cannot coexist.  e.g.  there can only be one financial outcome for a Prosecution.");
        Xrm.Page.getControl("rsl_pros_money_outofcourtsettlement").setFocus(true);
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // Hearing Reference Specified but no Outcome values
    //if (var_hearingref != null && var_costsawarded == null && var_compawarded == null && var_fineawarded == null) {
    //    var ans = confirm("Hearing Reference Specified but no outcomes entered.  Do you want you continue?");
    //    if (ans == false) {
    //        Xrm.Page.getControl("rsl_pros_hearingreference").setFocus(true);
    //        executionObj.getEventArgs().preventDefault();
    //        return;
    //    }
    //}
    // No Hearing Reference Specified
    //if (var_hearingref == null && (var_totalliability > 0 || var_fineawarded > 0)) {
    //    var ans = confirm("No Hearing Reference specified.	Do you want you continue?");
    //    if (ans == false) {
    //        Xrm.Page.getControl("rsl_pros_hearingreference").setFocus(true);
    //        executionObj.getEventArgs().preventDefault();
    //        return;
    //    }
    //}
    // Negative Total Liability
    if (var_totalliability < 0) {
        var ans = confirm("More moneies have been received than were awarded by the court.	Do you want you continue?");
        if (ans == false) {
            Xrm.Page.getControl("rsl_pros_money_costsawarded").setFocus(true);
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
    // If PFN Flipover Only allow Initial Letter Response of 'No Response' or 'Yes Out of Court Settlement Agreed'.
    // This validation applied as OOC Letter never printed for a Flip Over however you can suspend it into an Waiting OOC Settlement
    // if this has been agreed with prosecutor.
    var var_originatingpenaltyfare = Xrm.Page.getAttribute("rsl_pros_originatingpenaltyfare").getValue();
    //   Orign PFN has a value                                                No Response                                   Yes - Out of court Settlement Agreed
    if (var_originatingpenaltyfare != null && (var_initialletterresponselist != 866810000 && var_initialletterresponselist != 866810003)) {
        alert("A prosecution created from a PFN cannot be manually set to 'Out of Court Settlement - Print OOC Letter.' or 'Court Needs Assigning'.\n\nPlease revise this value.");
        executionObj.getEventArgs().preventDefault();
        return;
    }
    // ******************************************************************************************
    // **************************** All Validations have passed *********************************
    // ************************ Initial Letter Response Validations *****************************
    // ******************************************************************************************
    // If the Status is "Wait...Initial Letter Printed" (866810002) and rsl_pros_initialletterreponsereceived = YES - OoC Settlement (866810000) or Yes - Book Court (866810001)
    // then ask for what status we should go to next
    var myresponse;
    // InitialLetterResponse has been changed AND (Yes - OoC Settlement  OR Yes - Book Court  OR  Yes - OOC Settlement Agreed)
    if (var_initialletterresponselist != InitialLetterResponse && (var_initialletterresponselist == 866810001 || var_initialletterresponselist == 866810002 || var_initialletterresponselist == 866810003)) {
        // Yes - Mark for Out of Court Settlement
        if (var_initialletterresponselist == 866810001) {
            var myresponse = confirm("You have marked this record as having received an Initial Letter response and have chosen to mark this record as requiring an 'Out of Court Settlement'.\n\nPress OK to proceed or Cancel to abort the save and return to the form.")
            if (myresponse == true) {
                Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setValue(866810018);
                Xrm.Page.getAttribute("rsl_datestatusupdate").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_datestatusupdate").setValue(null);
                Xrm.Page.getAttribute("rsl_pros_dateoocsettlementneedsprinting").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_pros_dateoocsettlementneedsprinting").setValue(new Date());
                Xrm.Page.getAttribute("rsl_pros_dateoocourtsettlementprinted").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_pros_dateoocourtsettlementprinted").setValue(null);
                Xrm.Page.getAttribute("rsl_pros_datecourtneedsassigning").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_pros_datecourtneedsassigning").setValue(null);
                //                Xrm.Page.getAttribute("rsl_pros_money_outofcourtsettlement").setValue(myglobaloutofcourtsettlement);
                //                prosecution_CalcTotalLiability(1);
                //                alert("Out of Court Settlements Offered has been set to £" + myglobaloutofcourtsettlement.toString() + ".\n\nPlease modify this value using the 'Payment' tab on the form if necessary.");
            }
            else {
                executionObj.getEventArgs().preventDefault();
                return;
            }
        }
        // Court/Date Req. Booking AND Not Already in court Req Booking.
        else if (var_initialletterresponselist == 866810002 && myProsecutionStatusFormValue != 866810003) {
            var myresponse = confirm("You have marked this record as having received an Initial Letter response and have chosen to mark this record as requiring a 'Court Booking'.\n\nPress OK to proceed or Cancel to abort the save and return to the form.")
            if (myresponse == true) {
                // Set to Raised this will trigger process PROSECUTION_RejectCheck_SetStatus that will evaluate Reject Status 
                // and on move onto 'Cout Need Booking'
                Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setValue(866810000);
                Xrm.Page.getAttribute("rsl_datestatusupdate").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_datestatusupdate").setValue(null);
            }
            else {
                executionObj.getEventArgs().preventDefault();
                return;
            }
        }
        // OOC Settlement Agreed
        else if (var_initialletterresponselist == 866810003) {
            var myresponse = confirm("You have marked this record as having agreed to an Out of Court Settlement.\n\nPress OK to proceed or Cancel to abort the save and return to the form.")
            if (myresponse == true) {
                // Set to Wait...OOC Settlement Printed
                Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setValue(866810019);
                Xrm.Page.getAttribute("rsl_datestatusupdate").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_datestatusupdate").setValue(null);
                Xrm.Page.getAttribute("rsl_pros_dateoocsettlementneedsprinting").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_pros_dateoocsettlementneedsprinting").setValue(new Date());
                Xrm.Page.getAttribute("rsl_pros_dateoocourtsettlementprinted").setSubmitMode("always");
                Xrm.Page.getAttribute("rsl_pros_dateoocourtsettlementprinted").setValue(new Date());
            }
            else {
                executionObj.getEventArgs().preventDefault();
                return;
            }
        }
    }
    // If we are in "Partially Raised" and we confirmed we wanted to move the status on
    // to "Initial Lette Req Print" (passed in as a parameter to this function)
    if (myProsecutionStatusFormValue == 866810027 && movestatus == true) {
        // Move the Status from "Partially Raised" to "Initial  Letter Req Print"
        Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setSubmitMode("always");
        Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setValue(866810001);
        Xrm.Page.getAttribute("rsl_pros_dateinitialletterneedsprinting").setSubmitMode("always");
        Xrm.Page.getAttribute("rsl_pros_dateinitialletterneedsprinting").setValue(new Date());
    }

    // If the Status is "Wait...Awaiting ASN Response" (866810016) and rsl_pros_asn <> NULL
    // then warn the user that the Prosecution will move to Summons Requires Print (866810013)
    var myasn = Xrm.Page.getAttribute("rsl_pros_asn").getValue();
    myresponse == null;
    //      "Wait...Awaiting ASN Response" (866810016) AND ASN has a value
    if (myProsecutionStatusFormValue == 866810016 && myasn != null) {
        var myresponse = confirm("You have entered an ASN against this record which means this prosecution is ready to have the Summons printed.\n\nPress OK to advance the record to Print Summons\nor\nCancel to abort the save and return to the form.")
        if (myresponse == true) {
            Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setSubmitMode("always");
            Xrm.Page.getAttribute("rsl_pros_prosecutionstatus").setValue(866810013);
            Xrm.Page.getAttribute("rsl_pros_datesummonsneedsprinting").setSubmitMode("always");
            Xrm.Page.getAttribute("rsl_pros_datesummonsneedsprinting").setValue(new Date());
        }
        else {
            executionObj.getEventArgs().preventDefault();
            return;
        }
    }
}

function getpersondetails() {
    // Get person details...
    var person = Xrm.Page.getAttribute("rsl_pros_person").getValue();
    //
    // If we have a Person set e.g. It has not been cleared
    //
    if (person != null) {
        var guid = Xrm.Page.getAttribute("rsl_pros_person").getValue()[0].id;
        //var odataSetName = var_person[0].entityType;
        var odataSetName = "LeadSet"; // var_person[0].entityType returns 'lead' rather than 'Lead'

        // Get Server URL
        var serverUrl = Xrm.Page.context.getClientUrl();
        //The OData end-point
        var ODATA_ENDPOINT = "/XRMServices/2011/OrganizationData.svc";
        var select = "?$select=FirstName,LastName,rsl_Age,rsl_DOB,rsl_GuardiansName,FullName,rsl_fulladdress";
        //Asynchronous AJAX function to Retrieve a CRM record using OData
        $.ajax({
            type: "get",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: serverUrl + ODATA_ENDPOINT + "/" + odataSetName + "(guid'" + guid + "')" + select,
            beforeSend: function(XMLHttpRequest) {
                //Specifying this header ensures that the results will be returned as JSON.
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success: function(data, textStatus, XmlHttpRequest) {
                //Xrm.Page.getAttribute("rsl_pros_firstname").setValue(data.d.FirstName);
                //Xrm.Page.getAttribute("rsl_pros_lastname").setValue(data.d.LastName);
                //Xrm.Page.getAttribute("rsl_pros_fullname").setValue(data.d.FullName);
                Xrm.Page.getAttribute("rsl_pros_age").setValue(data.d.rsl_Age);
                //Xrm.Page.getAttribute("rsl_pros_dob").setValue(new Date(parseInt(data.d.rsl_DOB.replace("/Date(", "").replace(")/", ""), 10)));
                //Xrm.Page.getAttribute("rsl_pros_guardian").setValue(data.d.rsl_GuardiansName);
                // SAH this is commented out as this represetns the address at the time of offence and should never be updated from first set.
                //			  Xrm.Page.getAttribute("rsl_pros_addressonoffence").setValue(data.d.rsl_fulladdress);
                //prosecution_passengertype(); // Update the passenger type field
            },
            error: function(XmlHttpRequest, textStatus, errorThrown) {
                alert("Error " + errorThrown)
            }
        });
    }
    // Person has been blanked so null the details.
    else {
        Xrm.Page.getAttribute("rsl_pros_firstname").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_lastname").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_fullname").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_age").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_dob").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_guardian").setValue(null);
        prosecution_passengertype(); // Update the passenger type field
    }
}
// Functional called when the Offender is Changed and we are in NEW record mode.
// Sets the "At time Of Offence (ATOO) details as these need to be saved in the prosecution
// record and never changed as these are used in the Witness Statements and by prosecutor
// at a later date.
function setattimeofoffencedetails() {
    // Are we in NEW record mode ?
    var formType = Xrm.Page.ui.getFormType();
    var person = Xrm.Page.getAttribute("rsl_pros_person").getValue();

    Xrm.Page.getAttribute("rsl_pros_atoo_title").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_firstname").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_middlename").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_lastname").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_addressonoffence").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_gender").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_occupation").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_address1_telephone1").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_dob").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_age").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_passengertype").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_guardiansname").setSubmitMode("always");

    Xrm.Page.getAttribute("rsl_pros_atoo_address1_line1").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_address1_line2").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_address1_line3").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_address1_city").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_address1_county").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_address1_postcode").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_address1_country").setSubmitMode("always");

    Xrm.Page.getAttribute("rsl_pros_atoo_app_ethnicappearance").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_app_build").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_app_eyecolour").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_app_haircolour").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_app_hairtype").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_app_facialhair").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_height").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_otherappearancefeatures").setSubmitMode("always");

    Xrm.Page.getAttribute("rsl_pros_atoo_tips").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_electoralroll").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_drivinglic").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_passport").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_policecheck").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_postoffice").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_studentid").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_utilitybill").setSubmitMode("always");

    Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto1").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto2").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto3").setSubmitMode("always");

    Xrm.Page.getAttribute("rsl_pros_atoo_heshe").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_hesheuppercasefirstletter").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_hisher").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_atoo_hisheruppercasefirstletter").setSubmitMode("always");

    // If we have a Person set e.g. It has not been cleared
    //
    if (person != null) {
        // Get person details...
        var guid = Xrm.Page.getAttribute("rsl_pros_person").getValue()[0].id;
        //var odataSetName = var_person[0].entityType;
        var odataSetName = "LeadSet"; // var_person[0].entityType returns 'lead' rather than 'Lead'

        // Get Server URL
        var serverUrl = Xrm.Page.context.getClientUrl();
        //The OData end-point
        var ODATA_ENDPOINT = "/XRMServices/2011/OrganizationData.svc";
        var select = "?$select=rsl_Title,FirstName,MiddleName,LastName,rsl_fulladdress,rsl_Gender,rsl_Occupation,Address1_Telephone1,rsl_DOB,rsl_Age,rsl_GuardiansName,rsl_PassengerType,Address1_Line1,Address1_Line2,Address1_Line3,Address1_City,Address1_County,Address1_PostalCode,Address1_Country,rsl_App_EthnicAppearance,rsl_App_Build,rsl_App_EyeColour,rsl_App_HairColour,rsl_App_HairType,rsl_App_FacialHair,rsl_Height,rsl_OtherAppearanceFeatures, rsl_tips, rsl_electoralroll, rsl_drivinglic,rsl_passport,rsl_policecheck,rsl_studentid,rsl_utilitybill,rsl_PersonPhoto1,rsl_PersonPhoto2,rsl_PersonPhoto3,rsl_postoffice,rsl_HeShe,rsl_hesheuppercasefirstletter,rsl_hisher,rsl_hisheruppercasefirstletter";
        //Asynchronous AJAX function to Retrieve a CRM record using OData
        $.ajax({
            type: "get",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: serverUrl + ODATA_ENDPOINT + "/" + odataSetName + "(guid'" + guid + "')" + select,
            beforeSend: function(XMLHttpRequest) {
                //Specifying this header ensures that the results will be returned as JSON.
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success: function(data, textStatus, XmlHttpRequest) {
                Xrm.Page.getAttribute("rsl_pros_atoo_title").setValue(data.d.rsl_Title.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_firstname").setValue(data.d.FirstName);
                Xrm.Page.getAttribute("rsl_pros_atoo_middlename").setValue(data.d.MiddleName);
                Xrm.Page.getAttribute("rsl_pros_atoo_lastname").setValue(data.d.LastName);
                Xrm.Page.getAttribute("rsl_pros_addressonoffence").setValue(data.d.rsl_fulladdress);
                Xrm.Page.getAttribute("rsl_pros_atoo_gender").setValue(data.d.rsl_Gender.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_occupation").setValue(data.d.rsl_Occupation);
                Xrm.Page.getAttribute("rsl_pros_atoo_address1_telephone1").setValue(data.d.Address1_Telephone1);
                Xrm.Page.getAttribute("rsl_pros_atoo_dob").setValue(new Date(parseInt(data.d.rsl_DOB.replace("/Date(", "").replace(")/", ""), 10)));
                Xrm.Page.getAttribute("rsl_pros_atoo_age").setValue(data.d.rsl_Age);
                Xrm.Page.getAttribute("rsl_pros_atoo_passengertype").setValue(data.d.rsl_PassengerType);
                Xrm.Page.getAttribute("rsl_pros_atoo_guardiansname").setValue(data.d.rsl_GuardiansName);

                Xrm.Page.getAttribute("rsl_pros_atoo_address1_line1").setValue(data.d.Address1_Line1);
                Xrm.Page.getAttribute("rsl_pros_atoo_address1_line2").setValue(data.d.Address1_Line2);
                Xrm.Page.getAttribute("rsl_pros_atoo_address1_line3").setValue(data.d.Address1_Line3);
                Xrm.Page.getAttribute("rsl_pros_atoo_address1_city").setValue(data.d.Address1_City);
                Xrm.Page.getAttribute("rsl_pros_atoo_address1_county").setValue(data.d.Address1_County);
                Xrm.Page.getAttribute("rsl_pros_atoo_address1_postcode").setValue(data.d.Address1_PostalCode);
                Xrm.Page.getAttribute("rsl_pros_atoo_address1_country").setValue(data.d.Address1_Country);

                Xrm.Page.getAttribute("rsl_pros_atoo_app_ethnicappearance").setValue(data.d.rsl_App_EthnicAppearance.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_app_build").setValue(data.d.rsl_App_Build.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_app_eyecolour").setValue(data.d.rsl_App_EyeColour.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_app_haircolour").setValue(data.d.rsl_App_HairColour.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_app_hairtype").setValue(data.d.rsl_App_HairType.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_app_facialhair").setValue(data.d.rsl_App_FacialHair.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_height").setValue(data.d.rsl_Height);
                Xrm.Page.getAttribute("rsl_pros_atoo_otherappearancefeatures").setValue(data.d.rsl_OtherAppearanceFeatures);

                Xrm.Page.getAttribute("rsl_pros_atoo_tips").setValue(data.d.rsl_tips.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_electoralroll").setValue(data.d.rsl_electoralroll.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_drivinglic").setValue(data.d.rsl_drivinglic.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_passport").setValue(data.d.rsl_passport.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_policecheck").setValue(data.d.rsl_policecheck.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_postoffice").setValue(data.d.rsl_postoffice.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_studentid").setValue(data.d.rsl_studentid.Value);
                Xrm.Page.getAttribute("rsl_pros_atoo_utilitybill").setValue(data.d.rsl_utilitybill.Value);

                Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto1").setValue(data.d.rsl_PersonPhoto1);
                Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto2").setValue(data.d.rsl_PersonPhoto2);
                Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto3").setValue(data.d.rsl_PersonPhoto3);

                Xrm.Page.getAttribute("rsl_pros_atoo_heshe").setValue(data.d.rsl_HeShe);
                Xrm.Page.getAttribute("rsl_pros_atoo_hesheuppercasefirstletter").setValue(data.d.rsl_hesheuppercasefirstletter);
                Xrm.Page.getAttribute("rsl_pros_atoo_hisher").setValue(data.d.rsl_hisher);
                Xrm.Page.getAttribute("rsl_pros_atoo_hisheruppercasefirstletter").setValue(data.d.rsl_hisheruppercasefirstletter);

            },
            error: function(XmlHttpRequest, textStatus, errorThrown) {
                alert("Error " + errorThrown)
            }
        });
    }
    else {
        Xrm.Page.getAttribute("rsl_pros_atoo_title").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_firstname").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_middlename").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_lastname").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_addressonoffence").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_gender").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_occupation").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_address1_telephone1").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_dob").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_age").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_passengertype").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_guardiansname").setValue(null);

        Xrm.Page.getAttribute("rsl_pros_atoo_address1_line1").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_address1_line2").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_address1_line3").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_address1_city").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_address1_county").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_address1_postcode").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_address1_country").setValue(null);

        Xrm.Page.getAttribute("rsl_pros_atoo_app_ethnicappearance").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_app_build").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_app_eyecolour").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_app_haircolour").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_app_hairtype").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_app_facialhair").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_height").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_otherappearancefeatures").setValue(null);

        Xrm.Page.getAttribute("rsl_pros_atoo_tips").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_electoralroll").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_drivinglic").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_passport").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_policecheck").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_postoffice").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_studentid").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_utilitybill").setValue(null);

        Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto1").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto2").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_passengerphoto3").setValue(null);

        Xrm.Page.getAttribute("rsl_pros_atoo_heshe").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_hesheuppercasefirstletter").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_hisher").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_atoo_hisheruppercasefirstletter").setValue(null);
    }
}

// Functional called when the Offender is Changed.
// Sets the Correspondence Name & Address details as these need to be saved in the prosecution
// record from the Person record.
function setcorrespondencedetails() {
    var person = Xrm.Page.getAttribute("rsl_pros_person").getValue();

    Xrm.Page.getAttribute("rsl_pros_corr_title").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_firstname").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_middlename").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_lastname").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_addressonoffence").setSubmitMode("always");

    Xrm.Page.getAttribute("rsl_pros_corr_address1_line1").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_address1_line2").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_address1_line3").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_address1_city").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_address1_county").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_address1_postcode").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_address1_country").setSubmitMode("always");

    Xrm.Page.getAttribute("rsl_pros_corr_postoffice").setSubmitMode("always");
    //
    // If we have a Person set e.g. It has not been cleared
    //
    if (person != null) {
        // Get person details...
        var guid = Xrm.Page.getAttribute("rsl_pros_person").getValue()[0].id;
        //var odataSetName = var_person[0].entityType;
        var odataSetName = "LeadSet"; // var_person[0].entityType returns 'lead' rather than 'Lead'

        // Get Server URL
        var serverUrl = Xrm.Page.context.getClientUrl();
        //The OData end-point
        var ODATA_ENDPOINT = "/XRMServices/2011/OrganizationData.svc";
        var select = "?$select=rsl_Title,FirstName,MiddleName,LastName,rsl_fulladdress,Address1_Line1,Address1_Line2,Address1_Line3,Address1_City,Address1_County,Address1_PostalCode,Address1_Country,rsl_postoffice";
        //Asynchronous AJAX function to Retrieve a CRM record using OData
        $.ajax({
            type: "get",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: serverUrl + ODATA_ENDPOINT + "/" + odataSetName + "(guid'" + guid + "')" + select,
            beforeSend: function(XMLHttpRequest) {
                //Specifying this header ensures that the results will be returned as JSON.
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success: function(data, textStatus, XmlHttpRequest) {
                Xrm.Page.getAttribute("rsl_pros_corr_title").setValue(data.d.rsl_Title.Value);
                Xrm.Page.getAttribute("rsl_pros_corr_firstname").setValue(data.d.FirstName);
                Xrm.Page.getAttribute("rsl_pros_corr_middlename").setValue(data.d.MiddleName);
                Xrm.Page.getAttribute("rsl_pros_corr_lastname").setValue(data.d.LastName);
                Xrm.Page.getAttribute("rsl_pros_corr_addressonoffence").setValue(data.d.rsl_fulladdress);

                Xrm.Page.getAttribute("rsl_pros_corr_address1_line1").setValue(data.d.Address1_Line1);
                Xrm.Page.getAttribute("rsl_pros_corr_address1_line2").setValue(data.d.Address1_Line2);
                Xrm.Page.getAttribute("rsl_pros_corr_address1_line3").setValue(data.d.Address1_Line3);
                Xrm.Page.getAttribute("rsl_pros_corr_address1_city").setValue(data.d.Address1_City);
                Xrm.Page.getAttribute("rsl_pros_corr_address1_county").setValue(data.d.Address1_County);
                Xrm.Page.getAttribute("rsl_pros_corr_address1_postcode").setValue(data.d.Address1_PostalCode);
                Xrm.Page.getAttribute("rsl_pros_corr_address1_country").setValue(data.d.Address1_Country);

                Xrm.Page.getAttribute("rsl_pros_corr_postoffice").setValue(data.d.rsl_postoffice.Value);
            },
            error: function(XmlHttpRequest, textStatus, errorThrown) {
                alert("Error " + errorThrown)
            }
        });
    }
    else {
        Xrm.Page.getAttribute("rsl_pros_corr_title").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_firstname").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_middlename").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_lastname").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_addressonoffence").setValue(null);

        Xrm.Page.getAttribute("rsl_pros_corr_address1_line1").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_address1_line2").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_address1_line3").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_address1_city").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_address1_county").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_address1_postcode").setValue(null);
        Xrm.Page.getAttribute("rsl_pros_corr_address1_country").setValue(null);

        Xrm.Page.getAttribute("rsl_pros_corr_postoffice").setValue(null);
    }
}

//Set the Corr. Full Address Field
set_corr_fulladdress = function() {
    var addr1 = Xrm.Page.getAttribute("rsl_pros_corr_address1_line1").getValue();
    var addr2 = Xrm.Page.getAttribute("rsl_pros_corr_address1_line2").getValue();
    var addr3 = Xrm.Page.getAttribute("rsl_pros_corr_address1_line3").getValue();
    var town = Xrm.Page.getAttribute("rsl_pros_corr_address1_city").getValue();
    var county = Xrm.Page.getAttribute("rsl_pros_corr_address1_county").getValue();
    var postcode = Xrm.Page.getAttribute("rsl_pros_corr_address1_postcode").getValue();
    var country = Xrm.Page.getAttribute("rsl_pros_corr_address1_country").getValue();
    var fulladdress = addr1 + ", " + addr2 + ", " + addr3 + ", " + town + ", " + county + ", " + postcode + ", " + country
    fulladdress = fulladdress.replace(/, null/g, "");
    fulladdress = fulladdress.replace(/null, /g, "");
    Xrm.Page.getAttribute("rsl_pros_corr_addressonoffence").setSubmitMode("always");
    Xrm.Page.getAttribute("rsl_pros_corr_addressonoffence").setValue(fulladdress);
}


/*
// Because Sub Grids load async we need to build in a timeout to allow the SubGrid to build before we Cal the Total Paid
function Prosecution_subGridOnload() {
    //    var grid = Xrm.Page.ui.control.get("ProsecutionPayments");
    var grid = document.getElementById("ProsecutionPayments");

    //    if (grid.readyState != "complete") {
    if (grid == null || ((grid != null) && grid.readyState != "complete")) {
        // delay one second and try again.  
        setTimeout('Prosecution_subGridOnload();', 1000);
        return;
    }
    // Get the mode the Form is in
    // Simon Hopper - 11-Jun-2015
    // With the introduction of secutory roles the Prosecution form may be opened in a no writable
    // mode.  the calling of Prosecution_CalcTotalPaid() calcualtes the monetary totals which caused an
    // error as these values could not be written if the form is in Read Only status.
    //
    // Added the IF to only calculate the totals if the form is NOT READ or Disabled
    var formType = Xrm.Page.ui.getFormType();
    if (formType != 3 && formType != 4) {
        //grid.attachEvent("onrefresh", Prosecution_CalcTotalPaid);
        grid.control.add_onRefresh(Prosecution_CalcTotalPaid);
        Prosecution_CalcTotalPaid();
    }
}
*/

function addProsecutionPaymentsGridHandler () {
	Xrm.Page.getControl("ProsecutionPayments").addOnLoad(Prosecution_CalcTotalPaid);
}

// Calculate total by summing all the records in the payments subgrid
Prosecution_CalcTotalPaid = function() {
    // Get the guid of the current record
    var id = Xrm.Page.data.entity.getId();
    if (!id) {
        // This function will be called as part of the onLoad routine of the form.
        // If it is a new prosecution then it won't have an 'id' yet so we can just silently return
        // as there won't be any payments to add up.
        return;
    }
    var totalCosts = 0;
    var totalCompensation = 0;

    $.ajax({
        type: "GET",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        url: serverXRMServices + "rsl_prosecutionpaymentSet?$select=rsl_prosecutionpaymentcostsreceived,rsl_rsl_prosecutionpaymentcompreceived&$filter=rsl_proseuctionnumberId/Id%20eq%20(guid'" + id + "')",
        beforeSend: function(XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function(data, textStatus, XmlHttpRequest) {
            for (i = 0; i < data.d.results.length; i++) {
                var costs = parseFloat(data.d.results[i].rsl_prosecutionpaymentcostsreceived);
                var compensation = parseFloat(data.d.results[i].rsl_rsl_prosecutionpaymentcompreceived);
                if (isNaN(costs)) {
                    var record = i + 1;
                    alert("Invalid 'costs' value found in payments (record " + record + ")");
                } else {
                    totalCosts += costs;
                }
                if (isNaN(compensation)) {
                    var record = i + 1;
                    alert("Invalid 'compensation' value found in payments (record " + record + ")");
                } else {
                    totalCompensation += compensation;
                }
            }

            Xrm.Page.getAttribute("rsl_pros_money_costsreceived").setValue(totalCosts);
            Xrm.Page.getAttribute("rsl_pros_money_costsreceived").setSubmitMode("never");

            Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").setValue(totalCompensation);
            Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").setSubmitMode("never");
            prosecution_CalcTotalLiability();
            /*
            // The subgrid will generate an 'onrefresh' event whenever it get refreshed, which includes
            // at form loading time. As a result this function will get called even when the form vales have
            // not changed, so at this point just make a quick check to see if the form fields need updating.
            if ( Xrm.Page.getAttribute("rsl_pros_money_costsreceived").getValue() != totalCosts
            || Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").getValue() != totalCompensation ) {

				// Only update the fields on the form if they have changed...
            Xrm.Page.getAttribute("rsl_pros_money_costsreceived").setValue(totalCosts);
            Xrm.Page.getAttribute("rsl_pros_money_compensationreceived").setValue(totalCompensation);

				// ... and then update the database
            var updateObject = new Object();
            updateObject.rsl_pros_money_costsreceived = totalCosts.toFixed(2);
            updateObject.rsl_pros_money_compensationreceived = totalCompensation.toFixed(2);
            updateProsecutionRecord(updateObject);
            }
            */

            // ... and then update the database
            var totalliability = Xrm.Page.getAttribute("rsl_pros_totalawarded").getValue();
            var updateObject = new Object();
            updateObject.rsl_pros_money_costsreceived = totalCosts.toFixed(2);
            updateObject.rsl_pros_money_compensationreceived = totalCompensation.toFixed(2);
            updateObject.rsl_pros_totalawarded = totalliability.toFixed(2);
            updateProsecutionRecord(updateObject);
        },
        error: function(XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && errorThrown) {
                alert("Error while retrieving payment details; Message: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
            }
        }
    });
}

function updateProsecutionRecord(updateObject) {
    // updateProsecutionRecord: see http://rajeevpentyala.wordpress.com/2012/02/05/update-record-using-odata-and-jquery-in-crm-2011/
    var id = Xrm.Page.data.entity.getId();
    var jsonEntity = window.JSON.stringify(updateObject);

    $.ajax({
        type: "POST",
        contentType: "application/json; charaset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: serverXRMServices + "rsl_prosecutionSet(guid'" + id + "')",
        beforeSend: function(XMLHttpRequest) {
            //Specifiying this header ensures that the results will be returned as JSON
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            // Specify the HTTP method MERGE to update just the changes you are submitting
            XMLHttpRequest.setRequestHeader("X-HTTP-METHOD", "MERGE");
        },
        success: function(data, textStatus, XmlHttpRequest) {
            // The update was successful, so now update the 'Notes' entity with the details of what was done
        },
        error: function(XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && errorThrown) {
                alert("Script Error while updating rsl_prosecutionpaymentSet; Message: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value + " - costsrecieved: " + updateObject.rsl_pros_money_costsreceived + ", compreceived: " + updateObject.rsl_pros_money_compensationreceived + ", totalawarded: " + updateObject.rsl_pros_totalawarded);
            }
        }
    });
}

// Simon Hopper 26-Aug-15
// Updated to set the Law Type
//
// If in Wait Initial Letter Printed we may need to OcO Settlement Amount from the
// Prosecutions table so retrieve this and store it in a global variable.
//
// NOTE Tried to retrieve this OnSave but Ajax runs asyncronous so the save had
// happened before the values was retrieved.
var myglobaloutofcourtsettlement = 0;
var myglobalfromat = 0;
function getprostypeFromAt() {
    // Get Prosecution Type details...
    var prostype = Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup").getValue();
    //
    // If we have a Prosecution Type set e.g. It has not been cleared
    //
    if (prostype != null) {
        var guid = Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup").getValue()[0].id;
        var odataSetName = "rsl_prosecutiontypeSet";

        // Get Server URL
        var serverUrl = Xrm.Page.context.getClientUrl();
        //The OData end-point
        var ODATA_ENDPOINT = "/XRMServices/2011/OrganizationData.svc";
        var select = "?$select=rsl_prostype_moneyoocsetoffered,rsl_prostype_fromat_jurisdictioncourt,rsl_prostype_lawtype";
        //Asynchronous AJAX function to Retrieve a CRM record using OData
        $.ajax({
            type: "get",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: serverUrl + ODATA_ENDPOINT + "/" + odataSetName + "(guid'" + guid + "')" + select,
            beforeSend: function(XMLHttpRequest) {
                //Specifying this header ensures that the results will be returned as JSON.
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success: function(data, textStatus, XmlHttpRequest) {
                myglobaloutofcourtsettlement = parseFloat(data.d.rsl_prostype_moneyoocsetoffered);
                myglobalfromat = data.d.rsl_prostype_fromat_jurisdictioncourt.Value;
                Xrm.Page.getAttribute("rsl_pros_lawtype").setValue(data.d.rsl_prostype_lawtype.Value);

            },
            error: function(XmlHttpRequest, textStatus, errorThrown) {
                //alert("Error " + errorThrown)
                alert("Error while retrieving Prosecution Type details; Message: " + $.parseJSON(XmlHttpRequest.responseText).error.message.value);
            }
        });
    }
    //  Out of Court Settlement has been blanked so null the details.
    else {
        myglobaloutofcourtsettlement = 0;
    }
}

function checkProsTypeSet() {
    // If the 'Primary Prosecution Type' field is empty then disable the Court Paperwork fields
    if (Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup").getValue()) {
        // It is set so just return
        return;
    }

    // Disable the fields
    Xrm.Page.ui.controls.get("rsl_pros_paperworksummons").setDisabled(true);
    Xrm.Page.ui.controls.get("rsl_pros_paperworkstatementoffacts").setDisabled(true);
    Xrm.Page.ui.controls.get("rsl_pros_paperworkwitnessstatement").setDisabled(true);

}

function showProsTypeAlert() {

    // Show an alert to the user stating that the expected fields will not be set until a short time after the Prosecution type has been set (via a workflow)
    alert("The Court Paperwork Statements will be amended to match this Prosection Type shortly after the record has been saved.");

}

var InitialLetterResponse = 0
function Get_Onload_InitialLetterResponse() {
    InitialLetterResponse = Xrm.Page.getAttribute("rsl_pros_initial_letter_response_list").getValue();
}

//
// Hide supporting Prosecution types if they are empty
prosecution_HideShowProsecutionTypes = function() {
    if (Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup1").getValue() != null) {
        Xrm.Page.ui.controls.get("rsl_pros_prosecutiontypelookup1").setVisible(true);
    }
    if (Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup2").getValue() != null) {
        Xrm.Page.ui.controls.get("rsl_pros_prosecutiontypelookup2").setVisible(true);
    }
    if (Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup3").getValue() != null) {
        Xrm.Page.ui.controls.get("rsl_pros_prosecutiontypelookup3").setVisible(true);
    }
    if (Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup4").getValue() != null) {
        Xrm.Page.ui.controls.get("rsl_pros_prosecutiontypelookup4").setVisible(true);
    }
    if (Xrm.Page.getAttribute("rsl_pros_prosecutiontypelookup5").getValue() != null) {
        Xrm.Page.ui.controls.get("rsl_pros_prosecutiontypelookup5").setVisible(true);
    }
}
