// getIdentityImages.js
// This script will run when the form loads and will detect whether
// there are any identification images to be displayed in the form.
// 
// If the 'rsl_photo[n]' attribute (which is not displayed on the form)
// is non-null then the SRC attribute of the image element is amended
// to point to the approrpiate file on the image server.
// If any of the attributes/elements do not exist then the update for
// that item will silently fail
//
// 28Nov13 - Updated to also add the 'at time of offence' (atoo) images to the form
// 13Jan14 - Updated element selection and configuration after CRM rollup update

function getIdentityImage (variant) {
	// This is the function called by the onLoad event of the form. The variant parameter will be 'Person', 'Penalty' or 'Prosecution' depending on which form called the function
	var serverPath = serverBridge + "/getimage?id=";							// path to image server

	var photos = 3;
	var dataField;
	var imgElement;
	var atoo_photos = 3;
	var atoo_dataField = "rsl_atoo_passengerphoto";
	var atoo_imgElement = "WebResource_atoo_passengerphoto";

	switch (variant) {
		case "Person":
				//photos = 3;
				dataField = "rsl_personphoto";
				imgElement = "WebResource_PassengerPhoto";
				atoo_photos = 0;
				break;
		case "Penalty":
				//photos = 1;
				dataField = "rsl_penaltyphoto";
				imgElement = "WebResource_PenaltyPhoto";
				break;
		case "Prosecution":
				//photos = 1;
				dataField = "rsl_pros_photo";
				imgElement = "WebResource_ProsecutionPhoto";
				atoo_dataField = "rsl_pros_atoo_passengerphoto";
				break;
	}

	// Get photos if necessary
	for ( var index = 1; index <= photos; index++ ) {
		// Check if there is anything in the respective fields
		var ident = Xrm.Page.getAttribute(dataField+index).getValue();
		if ( ident ) {
			// Get the element that will contain the image
			var identImage = Xrm.Page.ui.controls.get(imgElement+index);
 			if (identImage) {
				// Update the SRC attribute
				identImage.setSrc(serverPath+ident);
				// Add a click handler to the image
				identImage.getObject().onclick = imageClicked;
			}
		}
	}

	// Get 'atoo' photos if necessary
	for ( var index = 1; index <= atoo_photos; index++ ) {
		// Check if there is anything in the respective fields
		var ident = Xrm.Page.getAttribute(atoo_dataField+index).getValue();
		if ( ident ) {
			// Get the element that will contain the image
			var identImage = Xrm.Page.ui.controls.get(atoo_imgElement+index);
 			if (identImage) {
				// Update the SRC attribute
				identImage.setSrc(serverPath+ident);
				// Add a click handler to the image
				identImage.getObject().onclick = imageClicked;
			}
		}
	}

	var parent = Xrm.Page.ui.controls.get(imgElement + '1').getObject().parentElement.parentElement.parentElement.parentElement.parentElement;
	// Call the image configuration function. It needs to be called here because it utilises the jQuery
	// javascript framework, which is not available at load time 
	imagePopupConfigure(parent);
}

function imageClicked () {
	// Click handler to display a larger image
	var pos = $(this).position();
	//var pos = $(this).offset();
	var size = { 'height': $(this).outerHeight(), 'width': $(this).outerWidth() };
	$('#imagePopup', this.parentElement.parentElement.parentElement.parentElement.parentElement).attr('src', this.src);		// 'this' here refers to the calling element (ie the image)
	$('#imagePopupDiv', this.parentElement.parentElement.parentElement.parentElement.parentElement).overlay(pos, size).show().mouseleave( function () { $(this).hide(); });	// 'this' here refers to the imagePopupDiv
}

//function imagePopupConfigure () {
var imagePopupConfigure = function () {
	var completed = false;
	
	return function (parent) {
		if ( completed ) {
			return;
		}

		// A jQuery extension that will overlay the bigger image on the smaller (clicked) one
		jQuery.fn.overlay = function (pos, size) {
			var posTop = pos.top - ((this.outerHeight()-size.height) / 2) + $(window).scrollTop();
			var posLeft = pos.left - ((this.outerWidth()-size.width) / 2) + $(window).scrollLeft();

			if ( posLeft < 0 ) {
				posLeft = 0;
			}

			if ( posLeft > (window.outerWidth - size) ) {
				posLeft = window.outerWidth - size;
			}

			this.css("position", "absolute");
			this.css("top", posTop + "px");
			this.css("left", posLeft + "px");
			this.css("z-index", 100);

			return this;
		}

		// A jQuery extension that will centralise the image in the window
		jQuery.fn.center = function () {
			this.css("position", "absolute");
			this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
			this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
			return this;
		}
	
		//$('body').append('<div id="imagePopupDiv"></div>');
		var imageDiv = $('<div id="imagePopupDiv">');
		var imagePopup = $('<img id="imagePopup">');

		imagePopup.css('background-color', '#CCCCCC');
		imageDiv.append(imagePopup).hide();

		$(parent).append(imageDiv);
		//Log("Popup Configured");
		completed = true;
	}
}();
