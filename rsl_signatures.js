// getSignature.js
// This script will run when the form loads and will detect whether
// there is an inspectors signature image to be downloaded
// 

// Load the Inspectors Signature css file
//rsl_load_css_file("../WebResources/rsl_signaturestyles");
function init() {
	// get the entity type
	var userType;
	$.each(window.location.search.split(/&/), function(i, v) {
		if ( v.split(/=/)[0] === "typename" ) {
			if ( v.split(/=/)[1] === "rsl_staff" ) {
				userType = "Inspector";
			}
			if ( v.split(/=/)[1] === "rsl_prosecutor" ) {
				userType = "Prosecutor";
			}
		}
		});

	getSignature(userType);
	addUploadSignatureButton(userType);
}

function setFields (userType) {
	// Set up an object to be returned - that way if, at a later date, more fields need to be assigned then they just need to
	// be added here rather than having to re-write the whole thing.
	obj = new Object();

	switch ( userType ) {
		case "Inspector":
			obj.sigField = "rsl_signature";
			obj.reference = "ireference";
			break;

		case "Prosecutor":
			obj.sigField = "rsl_prosecutor_signature";
			obj.reference = "preference";
			break;

		default:
			obj.sigField = "rsl_signature";
	}

	return obj;
}


function getSignature(userType) {
	// This is the function called by the onLoad event of the form.
	var serverPath = serverBridge + "getimage?id=";				// path to image server
	var signature;
	var fieldObj = setFields(userType);
	var sigField = fieldObj.sigField;

	// Check if there is anything in the field
	try {
		signature = parent.Xrm.Page.getAttribute(sigField).getValue();
	} catch (e) {
			// Can't find any signature fields
			alert("No signature field found.");
			return;
	}
		
	// Get the element that will contain the image
	var sigImage = $("#rsl_WebResource_signature");
	//var sigImage = document.getElementById("WebResource_signature"); <- Unsupported
	if ( signature ) {
		sigImage.prop('src', serverPath+signature);
	} else {
		sigImage.prop('src', serverWebResources + "rsl_no_signature");
	}
}

function addUploadSignatureButton(userType) {
	// This function is called by the onLoad event of the form.
	var formType = parent.Xrm.Page.ui.getFormType();
	if ( formType == 3 ) {
		// formType 3 is Read Only. The form has been opened by somebody who is not allowed to change it, so we can just exit.
		return;
	}

	var fieldObj = setFields(userType);
	var sigField = fieldObj.sigField;

	//var oldButton = $('#WebResource_btnSignaturePlaceHolder');
	//var newButton = $('<button></button>');

	var newButton = $('#rsl_WebResource_UploadButton');
	// Check if there is anything in the field
	try {
		signature = parent.Xrm.Page.getAttribute(sigField).getValue();
	} catch (e) {
			// Can't find any signature fields
			alert("No signature field found.");
			return;
	}

	if ( signature ) {
		newButton.val("Change");
	}

	//oldButton.parent().append(newButton);
	//oldButton.hide();
	newButton.parent().parent().css('text-align', 'left');
	//newButton.bind('click', function () { clickFunction(e); });
	newButton.bind('click', clickFunction);

	function clickFunction (e) {
		e.preventDefault();
		// Function to actually action the button click
		createPopup(fieldObj.reference);

		$('#addsigDiv').show();

		if ( !$('#addsigGuidHidden').length ) {
			// Only run this section if it has not already been run
			var id = parent.Xrm.Page.data.entity.getId();
			//Log("Adding missing bits");
			//$('#addsigDoneButton').bind("click", submitForm);

			$('#addsigForm')
				// These hidden element need to be added to the form at this point since we did not know
				// their values previously
				.append('<input id="addsigGuidHidden" type="hidden" name="id" value="'+id+'" />');

			$('#addsigCancelButton').click( function () {
				$('#addsigDiv').hide();
			});
		}
	}
}

	function createUpload() {
		//Log("Sending create upload request.");

		var url = serverBridge + "getuploadreference";
			
		return $.ajax({
			dataType: 'jsonp',
			url:  url,
			success: function (data, textStatus, XmlHttpRequest) {
				// Should get something like '{"response": "12345-abcde"}
				if ( !data.response ) {
					// something failed so alert the user
					alert("Error: wrong data format returned during upload creation phase.");
					// Strictly speaking the XmlHttpRequest has succeeded so we need to manually force an error condition that can be picked up in the next section
					// Maybe I could look at 'throw'ing an error - but maybe I'll do that in the future
					data.response = "ERROR";
					return;
				}
				// Add the upload id to the form before it is submitted...
				$('#addsigUploadIdHidden').prop('value', data.response);
			},
			error: function (XmlHttpRequest, textStatus, errorThrown) {
				if (XmlHttpRequest && errorThrown ) {
					alert("Error during the upload creation phase; Error (textStatus): " + textStatus);
					alert("Error thrown" + errorThrown);
				}
				$('#addsigDiv').hide();
			}
		});
	}

	function submitForm(e) {
		// When the 'Done' button is pressed the document is uploaded
		//$('#addsigDoneButton').unbind("click");
		//Log("Submitting the form");
		//
		//if ( $('#filefield").val()
		// Start the upload process by getting a reference from the server
		var getReference = createUpload(serverBridge + "getuploadreference");

		$.when( getReference ).done( function (createUploadResult) {
			var uploadId = createUploadResult.response;
			if ( uploadId == "ERROR" ) {
				//Log("Forced error encountered.");
				return;
			}
			//Log("Upload ref: "+uploadId);

			$('#addsigForm').submit();
			$('#addsigDiv').hide();
			$('#updatingDiv').show();
			window.setInterval(function () {
				// Would rather do this with a bit more finess but a sledgehammer seems the only choice here at the momemt :-(
				parent.window.location.reload(true);
			}, 7000); 	
			/*
			window.setTimeout(function () {
				//getSignature(userType);
				init();
				$('#updatingDiv').hide();
				}, 7000); 	
			*/
		});
	}

var createPopup = function() {
	// Popup for use when uploading signature images
	var Completed = false;
	var addsigPH = 150;
	var addsigPW = 300;

	return function(reference) {
		if ( Completed ) { return; }

		jQuery.fn.center = function () {
			this.css("position", "absolute");
			this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
			this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
			return this;
		}

		$('body').append('<div id="addsigDiv"></div>');

		$('#addsigDiv').append('<p id="addsigTitle">Please select an image...</p>')
			.addClass('addSignature')
			.css('height', addsigPH+'px')
			.css('width', addsigPW+'px')
			.center()
			.hide();

		$('#addsigTitle').css('text-align', 'center');

		$('#addsigDiv').append(
				$('<form id="addsigForm" action="'+serverBridge+'updatesignatureimage" method="post" enctype="multipart/form-data" target="hiddenIframe"></form>')
					.append('<input id="addsigCmdHidden" type="hidden" name="cmd" value="updatesignatureimage" />')
					.append('<input id="addsigUploadIdHidden" type="hidden" name="' + reference + '" />')
					.append('<input id="addsigFileInput" type="file" name="name" />')
					.append($('<div id="formButtonsDiv"></div>')
						.append('<button id="addsigDoneButton">Submit</button>')
						.append('<button id="addsigCancelButton">Cancel</button>')
						)
					.append('<input id="dummyTextInput" type="textarea" name="dummy" style="border-width: 0px; width: 0px; height: 0px;"/>')
					);

		$('#addsigDoneButton').bind("click", function (event) {
			event.preventDefault();
			submitForm();
		});
		$('#addsigCancelButton').click( function (event) {
				event.preventDefault();
				$('#addsigDiv').hide();
			});

		var iframe = $('<iframe name="hiddenIframe" id="hiddenIframe" style="border-width: 0px; width: 0px; height: 0px;"><html><head></head><body><div><p>Waiting...</p></div></body></html></iframe>');
		$('#addsigDiv').append(iframe);

		$('body').append('<div id="updatingDiv"></div>');
		$('#updatingDiv')
			.text("Uploading signature...")
			.addClass('addSignature')
			.center()
			.hide();

		Completed = true;
	}
}();
